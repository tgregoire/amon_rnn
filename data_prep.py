# -*- coding: utf-8 -*-
# pylint: disable=C0103
"""
.

Usage:
  data_prep

Options:
  -h --help     Show this screen.
"""
import os
import math
import warnings
# from itertools import combinations
# from itertools import compress
import numpy as np
import healpy as hp
from scipy import interpolate
# from matplotlib import cm
from matplotlib import pyplot as plt
# import matplotlib as mpl
from astropy import units as u
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, ICRS
from astropy.time import Time
import pandas as pd
from pandas.tseries.offsets import MonthEnd
from itertools import compress
# from sklearn.utils import column_or_1d, check_consistent_length # pylint: disable=E0401
# from sklearn.metrics import roc_auc_score, roc_curve, auc # pylint: disable=E0401
# from pyod.utils.utility import precision_n_scores # pylint: disable=E0401
# from pyod.utils import data as pyod # pylint: disable=E0401
from simple_functions import DATA_PATH, TRAIN_SPLIT, SEED, ang2pix_eq, pix2ang_eq, get_timeline, jd_to_mjd, scaled_pdf, get_random_times, all_combinations, get_split_index
from visualize import print_info_coinc, plot_dur_dec_HAWC_hotspots_spline, SHORTEN_DATASET_NAMES
from io_files import load_obj, save_obj
# from docopt import docopt # pylint: disable=E0401
# docopt(__doc__)
warnings.filterwarnings('ignore') # put default to have them back
# NB I managed to reduce event_density's time from 8 ms to 3 ms!
# I should have used classes, I think, but that is a bit too late

# %%
REF_LOCATION = EarthLocation(lat='0d0.0m', lon='-0d0.0m', height=0 * u.m) # on the equator and prime meridian (south of Togo)
FERMI_MASK_WIDTH = 10 # deg
DENSITY_CAP = 1. # NB Put `None` to not cap # density max of an event in a pixel before scaling factor and sum of events (TODO try several)
SCALING_FACTOR_MIN = 1.
SCALING_FACTOR_MAX = 2.

DATASETS = {'ANTARES':       {'file': 'ANTARES_dataset.pkl',
                              'err68%':1.1, 'err90%': 1.5, # TODO that should maybe be 1 dec for 68%
                              'location': EarthLocation(lat='42d48m', lon='6d10m', height=-2250 * u.m),
                              'begin_test': pd.Timestamp('2018-10-02T02:42:04'), 'end_test': pd.Timestamp('2018-10-08T02:42:04'),
                              'begin_all': pd.Timestamp('2018-05-04T07:59:18'), 'end_all': pd.Timestamp('2021-05-15T11:57:22'),
                              'alt_max': 0, 'alt_min': -90,
                              'df_columns': ['time', 'dec', 'ra', 'pvalue', 'src_err68'],
                              'unnecessary_columns': ['pvalue'],
                              'n_to_inj': 1,
                             },
            'IceCube':       {'file': 'IC_singlets_dataset.pkl',
                              'label': 'src_error', 'containment': '50%', 'scale_68%': 1.287,
                              'min_err68%': 0.3, 'max_err68%_lowalt': 2., 'max_err68%_highalt': 4.,
                              'location': EarthLocation(lat='-89d59.4m', lon='-63d36.29m', height=883 * u.m), # https://wiki.icecube.wisc.edu/index.php/Coordinate_system
                              'begin_test': pd.Timestamp('2019-12-02T02:42:04'), 'end_test': pd.Timestamp('2020-05-08T02:42:04'),
                              'begin_all': pd.Timestamp('2019-12-01T00:01:12'), 'end_all': pd.Timestamp('2021-05-17T21:14:15'),
                              'alt_max': 90, 'alt_min': -90,
                              'df_columns': ['time', 'time_msec', 'dec', 'ra', 'sigmaR', 'energy', 'signalness', 'src_error90', 'src_error', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'energy', 'signalness', 'src_err90', 'src_error'],
                              'n_to_inj': 1,
                             },
            'IceCube_dummy': {'file': 'IC_singlets_dummy_dataset.pkl', # only 5000 events
                              'label': 'src_error', 'containment': '50%', 'scale_68%': 1.287,
                              'min_err68%': 0.3, 'max_err68%_lowalt': 2., 'max_err68%_highalt': 4.,
                              'location': EarthLocation(lat='-89d59.4m', lon='-63d36.29m', height=883 * u.m), # https://wiki.icecube.wisc.edu/index.php/Coordinate_system
                              'begin_test': pd.Timestamp('2019-12-02T02:42:04'), 'end_test': pd.Timestamp('2019-12-08T02:42:04'),
                              'alt_max': 90, 'alt_min': -90,
                              'df_columns': ['time', 'time_msec', 'dec', 'ra', 'sigmaR', 'energy', 'signalness', 'src_error90', 'src_error', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'energy', 'signalness', 'src_err90', 'src_error'],
                              'n_to_inj': 1,
                             },
            'Auger':         {'file': 'Auger_dataset.pkl',
                              'label': 'sigmaR', 'containment': '68%', 'scale_68%': 1., # Jimmy is 'pretty sure' it is 68%
                              'location': EarthLocation(lat='-35d12.4m', lon='-69d18.95m', height=1400 * u.m),
                              'begin_test': pd.Timestamp('2019-10-02T02:42:04'), 'end_test': pd.Timestamp('2019-12-07T02:42:04'),
                              'alt_max': 90, 'alt_min': 30,
                              'df_columns': ['rev', 'time', 'dec', 'ra', 'sigmaR', 'false_pos', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'false_pos'],
                              'n_to_inj': 1,
                             },
            'Auger_dummy':   {'file': 'Auger_dummy_dataset.pkl', # only 5000 events
                              'label': 'sigmaR', 'containment': '68%', 'scale_68%': 1., # Jimmy is 'pretty sure' it is 68%
                              'location': EarthLocation(lat='-35d12.4m', lon='-69d18.95m', height=1400 * u.m),
                              'begin_test': pd.Timestamp('2019-10-02T02:42:04'), 'end_test': pd.Timestamp('2019-10-07T02:42:04'),
                              'alt_max': 90, 'alt_min': 30,
                              'df_columns': ['rev', 'time', 'dec', 'ra', 'sigmaR', 'false_pos', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'false_pos'],
                              'n_to_inj': 1,
                             },
            'HAWC_bursts':   {'file': 'HAWC_bursts_dataset.pkl',
                              'label': 'sigmaR', 'containment': '90%', 'scale_68%': 0.706,
                              'err68%_0': 0.2824, 'err68%_1': 0.4236, 'err68%_2': 0.5648,
                              'location': EarthLocation(lat='18d59.68m', lon='-97d18.51m', height=4100 * u.m),
                              'begin_test': pd.Timestamp('2018-10-02T02:42:04'), 'end_test': pd.Timestamp('2018-10-08T02:42:04'),
                              'begin_all': pd.Timestamp('2018-09-20T01:05:55'), 'end_all': pd.Timestamp('2021-05-17T08:50:38'),
                              'alt_max': 90, 'alt_min': 35,
                              'df_columns': ['time', 'time_msec', 'dec', 'ra', 'sigmaR', 'deltaT', 'false_pos', 'pvalue', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'false_pos', 'pvalue'],
                              'deltaT_choices': [0.2, 1., 10., 100.],
                              'n_to_inj': 1,
                             },
            'HAWC_hotspots': {'file': 'HAWC_hotspots_dataset.pkl',
                              'label': 'sigmaR', 'containment': '68%', 'scale_68%': 1.,
                              'min_err68%': 0.15, 'max_err68%': 1.,
                              'location': EarthLocation(lat='18d59.68m', lon='-97d18.51m', height=4100 * u.m),
                              'begin_test': pd.Timestamp('2018-10-02T02:42:04'), 'end_test': pd.Timestamp('2018-10-08T02:42:04'),
                              'begin_all': pd.Timestamp('2018-09-05T14:36:00'), 'end_all': pd.Timestamp('2021-05-17T19:56:00'),
                              'alt_max': 90, 'alt_min': 45,
                              'df_columns': ['time', 'time_msec', 'dec', 'ra', 'sigmaR', 'signif', 'MJD_i', 'MJD_f', 'MJD_c', 'duration', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'signif'],
                              'dur_dec_HAWC_hotspots_spline': load_obj(os.path.join(DATA_PATH, 'dur_dec_HAWC_hotspots_spline.pkl')), # Got this from interpolate_dur_dec_HAWC_hotspots()
                              'n_to_inj': 1,
                             },
            'Fermi':         {'files': ['Fermi_dataset_2018-03_2018-06.pkl', 'Fermi_dataset_2018-06_2018-09.pkl',
                                        'Fermi_dataset_2018-09_2018-12.pkl', 'Fermi_dataset_2018-12_2019-03.pkl',
                                        'Fermi_dataset_2019-03_2019-06.pkl', 'Fermi_dataset_2019-06_2019-09.pkl',
                                        'Fermi_dataset_2019-09_2019-12.pkl', 'Fermi_dataset_2019-12_2020-03.pkl',
                                        'Fermi_dataset_2020-03_2020-06.pkl', 'Fermi_dataset_2020-06_2020-09.pkl',
                                        'Fermi_dataset_2020-09_2020-12.pkl', 'Fermi_dataset_2020-12_2021-03.pkl',
                                       ],
                              'dummy_file': 'Fermi_dataset_2019-12_2020-03.pkl',
                              'label': 'sigmaR', 'containment': '39%', 'scale_68%': 1.515,
                              'min_err68%': 0.15, 'max_err68%': 6.,
                              'location': None,
                              'begin_test': pd.Timestamp('2019-12-03T02:42:04'), 'end_test': pd.Timestamp('2019-12-09T02:42:04'),
                              'begin_all': pd.Timestamp('2018-03-01T00:00:04'),
                              'end_all': pd.Timestamp('2021-02-28T23:59:59'),
                              'df_columns': ['time', 'time_msec', 'dec', 'ra', 'sigmaR', 'point_RA', 'point_Dec', 'energy', 'inclination', 'conversion', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'point_RA', 'point_Dec', 'energy', 'inclination', 'conversion'],
                              'n_to_inj': 30,
                             },
            'Fermi_dummy':   {'file': 'Fermi_dataset_2019-12_2020-03.pkl',
                              'label': 'sigmaR', 'containment': '39%', 'scale_68%': 1.515,
                              'min_err68%': 0.15, 'max_err68%': 6.,
                              'location': None,
                              'begin_test': pd.Timestamp('2019-12-03T02:42:04'), 'end_test': pd.Timestamp('2019-12-09T02:42:04'),
                              'df_columns': ['time', 'time_msec', 'dec', 'ra', 'sigmaR', 'point_RA', 'point_Dec', 'energy', 'inclination', 'conversion', 'src_err68'],
                              'unnecessary_columns': ['sigmaR', 'point_RA', 'point_Dec', 'energy', 'inclination', 'conversion'],
                              'n_to_inj': 30,
                             },
           }

# %%
#  ____    _  _____  _
# |  _ \  / \|_   _|/ \
# | | | |/ _ \ | | / _ \
# | |_| / ___ \| |/ ___ \
# |____/_/   \_\_/_/   \_\

def assign_src_err68(dataset, dataframe):
    """Get the 68% angular uncertainty.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with eventually 'sigmaR' or 'src_error' columns.
    Returns:
        dataframe: Pandas dataframe with eventually 'src_err68' column.
    """
    dataset_dict = DATASETS[dataset]
    if 'label' in dataset_dict.keys():
        return dataframe.assign(src_err68=dataframe[dataset_dict['label']]*dataset_dict['scale_68%'])
    elif 'err68%' in dataset_dict.keys():
        return dataframe.assign(src_err68=dataset_dict['err68%'])
    else:
        raise ValueError("Missing containment keys in DATASETS dictionary.")

def get_data(dataset, begin=None, end=None):
    """Return dataframe from dataset string.
    As Fermi files are too heavy, begin and end date of the period we want are necessary to load only the files we want.

    Args:
        dataset: String of the experiment name.
        begin: Beginning of the timeline. (pd.Timestamp)
        end: End of the timeline. (pd.Timestamp)
    Returns:
        dataframe: Pandas dataframe of the dataset.
    """
    if dataset != "Fermi":
        filename = DATASETS[dataset]["file"]

    # concatenate fermi files into one dataframe
    if dataset == "Fermi":
        filenames = DATASETS[dataset]["files"]
        df_list = []
        for file_ in filenames:
            if begin is not None and end is not None:
                file_begin = pd.Timestamp(file_[14:21]+"-01")
                file_end = pd.Timestamp(file_[22:29]+"-01")
                if begin < file_end and end > file_begin:
                    df_list += [pd.read_pickle(os.path.join(DATA_PATH, file_))]
            else:
                raise ValueError("Missing begin and end options which are necessary to read Fermi files.")
        dataframe = pd.concat(df_list)

    # Add middle time and duration
    elif dataset == "HAWC_hotspots":
        df_i = pd.read_pickle(os.path.join(DATA_PATH, filename))
        dataframe = df_i.assign(MJD_c=(df_i['MJD_f'] + df_i['MJD_i']) / 2.,
                                duration=(df_i['MJD_f'] - df_i['MJD_i']),)

    else:
        dataframe = pd.read_pickle(os.path.join(DATA_PATH, filename))

    # Look for NaN and drop them
    if dataframe.isna().any().any():
        n_nan = dataframe.isna().sum().sum()
        if n_nan < 8:
            print("Warning: {n} NaN in {dataset} dataframe. Dropping row{plural}...".format(n=n_nan, dataset=dataset, plural=""+"s"*int(n_nan > 1)))
            dataframe.dropna(inplace=True)
        else:
            raise ValueError("Too many NaN ({}) in dataframe {}.".format(n_nan, dataset))

    dataframe = assign_src_err68(dataset, dataframe)

    # dataframe.drop(DATASETS[dataset]['unnecessary_columns'], axis=1, inplace=True) # To get lighter files and go faster
    if begin is not None and end is not None and (end < dataframe['time'].min() or begin > dataframe['time'].max()):
        raise ValueError("Time window [{begin}, {end}] is outside of dataset time period: [{min_time}, {max_time}] {dataset}"
                         .format(begin=begin, end=end, min_time=dataframe['time'].min(), max_time=dataframe['time'].max(), dataset=dataset))

    return dataframe

signif_file = os.path.join(DATA_PATH, 'dic_interp_signif_'+str(int(SCALING_FACTOR_MIN))+'_'+str(int(SCALING_FACTOR_MAX))+'.pkl')
if os.path.isfile(signif_file):
    DIC_INTERP_SIGNIF = load_obj(signif_file)
    # NB file is created after declaration of save_interp_signif function.
def save_interp_signif(dataset_list, begin, end, factor_min=1., factor_max=2.):
    """Save a dictionnary of the functions to get the scaling factor from the energy/p-value/etc for each dataset.

    Args:
        dataset_list: List of the dataset names. (list)
        factor_min: Minimum of the scaling factor. (float)
        factor_max: Maximum of the scaling factor. (float)
    Returns:
        dic_interp_signif:
    """
    dic_interp_signif = {'cdf': {}, 'bin_midpoints': {}, 'to_scaling_factor': {}}
    for dataset in dataset_list:
        dataframe = get_data(dataset, begin, end)
        if dataset == 'ANTARES':
            signifs = -np.log10(np.array(dataframe.pvalue.dropna(), dtype=float))
        elif dataset == 'HAWC_bursts':
            signifs = -np.log10(np.array(dataframe.pvalue.dropna(), dtype=float))
        elif dataset == 'IceCube':
            signifs = np.array(dataframe.signalness.dropna(), dtype=float)
            # signifs = np.log10(np.array(dataframe.energy.dropna(), dtype=float))
        elif dataset == 'Fermi':
            signifs = np.log10(np.array(dataframe.energy.dropna(), dtype=float))
        elif dataset == 'HAWC_hotspots':
            signifs = np.array(dataframe.signif.dropna(), dtype=float)

        plt.figure(figsize=(6 * 1.618, 6))
        y_pdf, bins, _ = plt.hist(signifs, density=True, bins=500)
        cdf = np.cumsum(y_pdf)
        cdf = cdf/cdf[-1]
        dic_interp_signif['cdf'][dataset] = cdf
        bin_midpoints = bins[:-1] + np.diff(bins)/2
        dic_interp_signif['bin_midpoints'][dataset] = bin_midpoints

        y, bins, _ = plt.hist(signifs, cumulative=-1, density=True, bins=500)
        bin_midpoints = bins[:-1] + np.diff(bins)/2
        # plt.plot(bin_midpoints, factor_max + (factor_min-factor_max)*y, lw=2)
        interp = interpolate.interp1d(bin_midpoints, factor_max + (factor_min-factor_max)*y, bounds_error=False, fill_value=(factor_min, factor_max))
        dic_interp_signif['to_scaling_factor'][dataset] = interp
        x = np.linspace(bin_midpoints[0]-0.1, bin_midpoints[-1]+0.1, 1000)
        plt.plot(x, interp(x))

    save_obj(dic_interp_signif, os.path.join(DATA_PATH, 'dic_interp_signif_'+str(int(factor_min))+'_'+str(int(factor_max))+'.pkl')) # TODO get min max in filename

if not os.path.isfile(signif_file): # Create file if it does not exists.
    print('Warning: {signif_file} does not exist. Creating it.'.format(signif_file=signif_file))
    dataset_list = ['ANTARES', 'Fermi', 'HAWC_bursts', 'HAWC_hotspots', 'IceCube']
    save_interp_signif(dataset_list, pd.Timestamp('2019-12-01 00:00:00'), pd.Timestamp('2021-02-28 00:00:00'), SCALING_FACTOR_MIN, SCALING_FACTOR_MAX)
    DIC_INTERP_SIGNIF = load_obj(signif_file)

#  ____   ____ ____      _    __  __ ____  _     _____
# / ___| / ___|  _ \    / \  |  \/  | __ )| |   | ____|
# \___ \| |   | |_) |  / _ \ | |\/| |  _ \| |   |  _|
#  ___) | |___|  _ <  / ___ \| |  | | |_) | |___| |___
# |____/ \____|_| \_\/_/   \_\_|  |_|____/|_____|_____|

def equatorial_to_local(dataset, dataframe, dataset_name=False):
    """Convert equatorial to local detector coordinates.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with at least columns 'ra', 'dec' in degrees and 'time' in format isot frame utc.
        dataset_name: Set to True to name the local coord 'az_<dataset>' et 'alt_<dataset>'
    Returns:
        dataframe: Pandas dataframe including 'az' and 'alt' columns in degrees. NB for icecube, for example: alt_astropy = 90-zen_IC and
        az_astropy ~ 153.6 - az_icecube.
    """
    if dataframe['ra'].max() <= 2*math.pi and len(dataframe['ra']) > 10:
        print("Warning: right ascension should be in degrees but seems to be in radians.")
    if dataframe['dec'].abs().max() <= math.pi and len(dataframe['dec']) > 10:
        print("Warning: declination should be in degrees but seems to be in radians.")
    det_location = DATASETS[dataset]["location"]
    if det_location is None:
        raise ValueError("No location in DATASETS for {dataset}".format(dataset=dataset))
    icrs_coord = SkyCoord(ra=np.array(dataframe['ra']) * u.deg, dec=np.array(dataframe['dec']) * u.deg, frame=ICRS)
    if dataset == 'HAWC_hotspots':
        if 'MJD_c' in dataframe.columns:
            observing_time = Time(dataframe['MJD_c'], format='mjd')
        elif dataset_name: # If dataset_name then time correspond to the source time which correspond to MJD_c
            observing_time = Time(dataframe['time'])
    else:
        observing_time = Time(dataframe['time'])
    local_frame = AltAz(location=det_location, obstime=observing_time)
    local_coord = icrs_coord.transform_to(local_frame)

    if dataset_name:
        kwargs = {dataset+'-alt' : local_coord.alt, dataset+'-az' : local_coord.az}
        dataframe = dataframe.assign(**kwargs)
    else:
        dataframe = dataframe.assign(alt=local_coord.alt, az=local_coord.az)
    return dataframe

def local_to_equatorial(dataset, dataframe):
    """Convert local detector coordinates to equatorial.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with at least columns 'az', 'alt' in degrees and 'time' in format isot and frame utc. NB for icecube, for example: alt_astropy = 90-zen_IC and
        az_astropy ~ 153.6 - az_icecube.
    Returns:
        dataframe: Pandas dataframe including 'ra' and 'dec' columns in degrees.
    """
    if dataframe['az'].max() <= 2*math.pi and len(dataframe['az']) > 10:
        print("Warning: azimuth should be in degrees but seems to be in radians.")
    if dataframe['alt'].abs().max() <= math.pi and len(dataframe['alt']) > 10:
        print("Warning: altitude should be in degrees but seems to be in radians.")
    det_location = DATASETS[dataset]["location"]
    if det_location is None:
        raise ValueError("No location in DATASETS for {dataset}".format(dataset=dataset))
    observing_time = Time(dataframe['time'])
    local_frame = AltAz(location=det_location, obstime=observing_time)
    local_coord = SkyCoord(az=dataframe['az'] * u.deg, alt=dataframe['alt'] * u.deg, frame=local_frame)
    icrs_coord = local_coord.transform_to(ICRS)
    return dataframe.assign(dec=icrs_coord.dec, ra=icrs_coord.ra)

def permute(dataframe, label):
    """Permute rows for the columns in label.

    Args:
        dataframe: Pandas dataframe with at least columns in label.
        label: list of column names, or string of one column name.
    Returns:
        df_permute: Pandas dataframe after permutation.
    """
    if isinstance(label, (list, np.ndarray)) and len(label) > 1:
        for lab in label:
            if not isinstance(lab, str):
                raise TypeError("Label should be a string or a list of strings.")
            if lab not in dataframe.columns:
                raise ValueError("Label {label} does not correspond to a column of the dataframe.".format(label=lab))

        if 'id' in dataframe.columns:
            df_permute = dataframe.reset_index(drop=True).copy()
        else:
            df_permute = dataframe.reset_index().copy()
        df_permute2 = df_permute.reindex(np.random.permutation(df_permute.index))
        df_permute2.reset_index(drop=True, inplace=True)
        df_permute2[label] = df_permute[label]
        if 'time_msec' in df_permute.columns:
            df_permute = df_permute2.sort_values(by=['time', 'time_msec'])
        else:
            df_permute = df_permute2.sort_values(by='time')
        df_permute.reset_index(drop=True, inplace=True)
    elif isinstance(label, str) or (isinstance(label, (list, np.ndarray)) and len(label) == 1):
        if label not in dataframe.columns:
            raise ValueError("Label {label} does not correspond to a column of the dataframe.".format(label=label))
        df_permute = dataframe.copy()
        df_permute[label] = np.random.permutation(df_permute[label].values)
    else:
        raise TypeError("Label should be a string or a list of strings.")
    return df_permute

def scramble_data(dataset, dataframe):
    """Scramble data.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe of the dataset.
    Returns:
        dataframe: Pandas dataframe of the scrambled dataset.
    """
    if any([dset in dataset for dset in ["ANTARES", "IceCube", "Auger"]]):
        # Split dataframe because SkyCoord cannot treat more than 10000 events at once
        max_evts = 10000
        df_list = [dataframe[j*max_evts:(j+1)*max_evts] for j in range(int(math.ceil(len(dataframe)/max_evts)))]
        df_scram_list = []
        for df in df_list:
            df = equatorial_to_local(dataset, df)
            if "time_msec" in df.columns:
                df = permute(df, ['az', 'time', 'time_msec'])
            else:
                df = permute(df, ['az', 'time'])
            df_scram_list += [local_to_equatorial(dataset, df)]
        df_scram = pd.concat(df_scram_list)
        df_scram.reset_index(inplace=True, drop=True)

    elif dataset == "HAWC_hotspots":
        # Permute declination+duration because transit duration depends on declination
        df_scram = permute(dataframe, ['dec', 'duration'])
        df_scram['MJD_i'] = df_scram['MJD_c'] - df_scram['duration'] / 2.
        df_scram['MJD_f'] = df_scram['MJD_c'] + df_scram['duration'] / 2.

    elif dataset == "HAWC_bursts":
        df_scram = permute(dataframe, "dec")

    elif "Fermi" in dataset:
        # Fermi LAT photons are not scrambled as the LAT data contains known sources and extensive (complex) structure.
        df_scram = dataframe

    return df_scram



#  _____                 _         _                _ _
# | ____|_   _____ _ __ | |_    __| | ___ _ __  ___(_) |_ _   _
# |  _| \ \ / / _ \ '_ \| __|  / _` |/ _ \ '_ \/ __| | __| | | |
# | |___ \ V /  __/ | | | |_  | (_| |  __/ | | \__ \ | |_| |_| |
# |_____| \_/ \___|_| |_|\__|  \__,_|\___|_| |_|___/_|\__|\__, |
#                                                         |___/
def time_selection(dataset, dataframe, time_window, no_evt_time_check=False, verbose=True):
    """Select events in the dataframe that are inside the time window defined by [time_i, time_f].

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with at least 'time' column.
        time_window: Dictionary of the time window for which we want the event density. It should contain 'start' and 'stop' in pandas timestamp.
        no_evt_time_check: Set to true when getting a small slice of a dataframe with time selection already applied, in this case it is normal to have first event
                           after start and/or last before stop.
        verbose: If True print Warning.
    Returns:
        dataframe: Pandas dataframe with only events overlaping with the time window.
    """
    if not isinstance(time_window['start'], pd.Timestamp) or not isinstance(time_window['stop'], pd.Timestamp):
        raise TypeError("time_window['start'] and time_window['stop'] should be pandas Timestamp.")
    if time_window['start'] > time_window['stop']:
        raise ValueError("Time window starts after it stops.")
    if not no_evt_time_check and (time_window['stop'] < dataframe['time'].min() or time_window['start'] > dataframe['time'].max()):
        print("Warning: Time window [{t_win_start}, {t_win_stop}] is outside of dataset time period: [{min_time}, {max_time}] {dataset}"
              .format(t_win_start=time_window['start'], t_win_stop=time_window['stop'], min_time=dataframe['time'].min(), max_time=dataframe['time'].max(), dataset=dataset))
    event_time = dataframe['time']
    if 'time_msec' in dataframe.columns:
        if dataframe.head()['time_msec'].max() > 1000:
            unit = "microseconds"
        else:
            unit = "milliseconds"
        event_time = dataframe['time']+pd.to_timedelta(dataframe['time_msec'], unit=unit)

    if dataset == 'HAWC_hotspots':
        start_mjd = jd_to_mjd(time_window['start'].to_julian_date())
        stop_mjd = jd_to_mjd(time_window['stop'].to_julian_date())
        df_sel = dataframe[(dataframe['MJD_i'] < stop_mjd) & (dataframe['MJD_f'] > start_mjd)]
    elif dataset == 'HAWC_bursts':
        start_time = time_window['start']-pd.to_timedelta(dataframe['deltaT'], unit='seconds')
        stop_time = time_window['stop']+pd.to_timedelta(dataframe['deltaT'], unit='seconds')
        df_sel = dataframe[(event_time > start_time) & (event_time < stop_time)]
    else:
        df_sel = dataframe[(event_time > time_window['start']) & (event_time < time_window['stop'])]

    if df_sel.empty and verbose:
        print("Warning: not a single event in the time window anywhere in the sky.")

    return df_sel

def assign_ra_dec_rad(dataframe):
    """Add columns 'ra_rad' and 'dec_rad' in radians to the dataframe.

    Args:
        dataframe: Pandas dataframe with at least 'ra' and 'dec' columns in degrees.
    Returns:
        dataframe: Pandas dataframe with 'ra_rad' and 'dec_rad' columns in radians.
    """
    if dataframe['ra'].max() <= 2*math.pi and len(dataframe['ra']) > 10:
        print("Warning: right ascension should be in degrees but seems to be in radians.")
    if dataframe['dec'].abs().max() <= math.pi and len(dataframe['dec']) > 10:
        print("Warning: declination should be in degrees but seems to be in radians.")
    return dataframe.assign(ra_rad=np.radians(dataframe.ra), dec_rad=np.radians(dataframe.dec))

def great_circle_distance(dataframe, position):
    """Compute the great circle distance between (ra, dec) and the data in dataframe. This is about three times faster than great_circle_distance_slow.

    Args:
        dataframe: Pandas dataframe with at least 'ra_rad' and 'dec_rad' columns in radians.
        position: Dictionary of the position in (ra, dec) for which we want the event density. It should contain 'ra_rad' and 'dec_rad' in radians.
    Returns:
        distances: pd.Series of the distances between position and the data.
    """
    if not 'ra_rad' in position.keys() or not 'dec_rad' in position.keys():
        raise ValueError("ra_rad and/or dec_rad key missing in position dic.")
    if not isinstance(position['ra_rad'], (float, int)) or not isinstance(position['dec_rad'], (float, int)):
        raise TypeError("ra_rad and dec_rad should be floats.")
    if dataframe.empty:
        raise ValueError("Dataframe is empty.")

    phi_evts = dataframe.ra_rad
    theta_evts = dataframe.dec_rad
    phi_pos = position['ra_rad']
    theta_pos = position['dec_rad']
    return np.degrees(np.arccos(np.cos(theta_evts) * np.cos(theta_pos) * np.cos(phi_evts - phi_pos) + np.sin(theta_evts) * np.sin(theta_pos)))

def pdf_test(distance, sig68): # TODO docstring is not correct
    """Scaled probability density function to be 1 for distance=sig68. This function is more than 3 times faster than scaled_pdf_slow with the same result.

    Args:
        distance: distance from the event.
        sig68: Angular error of the event, corresponding to 68% containment.
    Returns:
        Scaled probability density function to be 1 for distance=sig68."""
    return 1/(sig68/1.515)*np.exp(-np.power(distance, 2)/(2*np.power(sig68/1.515, 2)))


def f_event_density(distance, src_err68, scaling_factor=1.):
    """Compute the density of events from the distances (already computed) and 68% angular error of the events which are in dataframe.

    Args:
        distance: Pandas series, array or float of the distance to the event.
        src_err68: Pandas series, array or float of the 68% containment contour of the events.
    Returns:
        Density of events.
    """
    if (isinstance(src_err68, (float, int)) and src_err68 <= 0) or (isinstance(src_err68, pd.Series) and (src_err68 <= 0).any()) or (distance < 0).any():
        raise ValueError("Angular error should be strictly positive.")
    # NB having something = 1 when close allows the RNN to count the number of events (if in 68% contour...) but makes no difference between large and small angular errors
    # therefore TODO test with pdf when close too
    # Eventually test with 1 for 50% contour and 0.5 for 90% contour and less then (not sure continuity is important)
    # Test without continuity between far and close
    # Info que je voudrais qu'on puisse extraire de l'evt density: nb d'evts (two events overlaping should be different from the center of one event),
    # ...how close to best fit, angular error of the event (large error should give lower density)
    # ...eventually use pvalue or else.
    densities = np.clip(pdf_test(distance, src_err68), 0., DENSITY_CAP)
    return np.sum(densities*scaling_factor)


def event_density(dataset, dataframe, position, inpix=False):
    """Compute the event density at position X in the time window [time_i, time_f].

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with at least 'time', 'ra_rad', 'dec_rad' and angular error columns.
        position: Dictionary of the position in ('ra_rad', 'dec_rad') for which we want the event density. It should contain 'ra_rad' and 'dec_rad' in radians.
    Returns:
        event_density: Float corresponding to the event density at position X in the time window.
    """
    # if not 'ra_rad' in position.keys() or not 'dec_rad' in position.keys():
    #     raise ValueError("position must contain keys 'ra_rad' and 'dec_rad'")
    if not 'ra_rad' in dataframe.columns or not 'dec_rad' in dataframe.columns:
        raise ValueError("dataframe must contain columns 'ra_rad' and 'dec_rad'")
    if "HAWC" in dataset and (position['dec_rad'] > np.radians(75) or position['dec_rad'] < np.radians(-35)): # HAWC events are always within [-35, 75]
        return 0.
    if inpix:
        distance = pd.Series(np.zeros(len(dataframe.index)))
    else:
        distance = great_circle_distance(dataframe, position)
    src_error68 = dataframe.src_err68.to_numpy() # Could be removed from the loop, but takes 150 times less time than great_circle_distance.
    scaling_factor = dataframe.scaling_factor.to_numpy()
    if any([dset in dataset for dset in ["Fermi", "HAWC_hotspots"]]): # This should make it faster for these
        selection = (distance < src_error68*3)
        distance = distance[selection]
        src_error68 = src_error68[selection]
        scaling_factor = scaling_factor[selection]
        if distance.empty:
            return 0
    return f_event_density(distance, src_error68, scaling_factor)

#  ____  _  ____   ____  __    _    ____  ____
# / ___|| |/ /\ \ / /  \/  |  / \  |  _ \/ ___|
# \___ \| ' /  \ V /| |\/| | / _ \ | |_) \___ \
#  ___) | . \   | | | |  | |/ ___ \|  __/ ___) |
# |____/|_|\_\  |_| |_|  |_/_/   \_\_|   |____/

def get_grid(nside):
    """Get a grid of the sky using healpy.

    Args:
        nside: nside of the healpy skymap.
    Returns:
        grid_dict: Dictionnary containing 'ra_rad' and 'dec_rad' keys with for each the corresponding np.array of the grid points.
    """
    npix = hp.nside2npix(nside)
    dec, ra = pix2ang_eq(nside, range(npix))
    return {'ra_rad': ra, 'dec_rad': dec}

def mask_Fermi(nside, b_dist_deg):
    """Create a mask covering the galactic plane region with b<b_dist_deg.

    Args:
        nside: Nside of the skymap. (int)
        b_dist_deg: All pixels with a galactic latitude b tel que -b_dist_deg < b < b_dist_deg
            will be set to 1 to be masked. (float)
    Returns:
        mask: Healpy skymap of 0 and 1, 1 corresponding to the pixels to mask. (np.array)
    """
    mask = np.arange(hp.nside2npix(nside))
    dec, ra = np.degrees(pix2ang_eq(nside, mask)) # pylint: disable=E1111
    icrs_coords = SkyCoord(ra=np.array(ra) * u.deg, dec=np.array(dec) * u.deg, frame=ICRS)
    gal_coords = icrs_coords.galactic
    resolution = np.degrees(hp.nside2resol(nside)) # pylint: disable=E1111
    mask = [np.abs(gal_coord.b.deg) < b_dist_deg+resolution/2. for gal_coord in gal_coords]
    return mask

def get_skymaps_of_densities(opt, dataset, dataframe, verbose=True): # pylint: disable=R0913
    """Get the event densities of each pixels and each timesteps between begin and end.
    Event density is sum of 1 for each event within the pixel + event_density of the others.

    Args:
        opt: Option arguments of the script. (dict)
        dataset: String of the experiment name.
        dataframe: Pandas dataframe.
        verbose: If True print Warning.
    Returns:
        skymaps: pd.DataFrame containing 'ra_rad', 'dec_rad' and each time steps in columns and each pixel as rows.
    """
    timeline = get_timeline(opt)
    skymaps = pd.DataFrame(get_grid(opt['nside']))
    no_evt_time_check = opt['job_id'] == opt['n_jobs']-1 # if last job, do not check event time
    dataframe = time_selection(dataset, dataframe, {'start': opt['begin']-opt['timestep'], 'stop': opt['end']+opt['timestep']}, no_evt_time_check, verbose)
    if dataset == 'Fermi':
        mask = mask_Fermi(opt['nside'], FERMI_MASK_WIDTH)

    for i, time in enumerate(timeline): # in realtime analysis time will be the current time here not
        time_slice = {'start': time, 'stop': time+opt['timestep']}
        data_sel = time_selection(dataset, dataframe, time_slice, no_evt_time_check=True, verbose=verbose)

        skymaps[i] = 0.
        if not data_sel.empty:
            evts_pixels = ang2pix_eq(opt['nside'], data_sel.dec_rad, data_sel.ra_rad)
            # prev_pix_dec = 0.
            # max_src_err68 = data_sel.src_err68.max()
            # data_sel_close_dec = data_sel
            for pix in range(hp.nside2npix(opt['nside'])):
                if dataset == 'Fermi' and mask[pix]:
                    skymaps[i][pix] = 0
                    continue
                pix_position = {"ra_rad": skymaps.ra_rad[pix], "dec_rad": skymaps.dec_rad[pix]}
                # if np.abs(prev_pix_dec-pix_position['dec_rad']) > hp.nside2resol(opt['nside'])/2.: # To make it faster preselect event with dec close (especially for Fermi)
                #     data_sel_close_dec = data_sel[np.abs(pix_position['dec_rad']-data_sel.dec_rad) < np.radians(max_src_err68*3.) + hp.nside2resol(opt['nside'])]
                #     prev_pix_dec = pix_position['dec_rad']
                # data_sel_outpix = data_sel_close_dec.drop(index=evts_pixels.index[evts_pixels == pix]) # drop events that fall in this pixel (because we add 1 and not event_density for them)
                data_sel_outpix = data_sel.drop(index=evts_pixels.index[evts_pixels == pix]) # drop events that fall in this pixel (because we add 1 and not event_density for them)
                data_sel_inpix = data_sel.drop(index=evts_pixels.index[evts_pixels != pix]) # drop events that fall in this pixel (because we add 1 and not event_density for them)
                if data_sel_outpix.empty:
                    skymaps[i][pix] = event_density(dataset, data_sel_inpix, pix_position, inpix=True)
                elif data_sel_inpix.empty:
                    skymaps[i][pix] = event_density(dataset, data_sel_outpix, pix_position, inpix=False)
                else:
                    skymaps[i][pix] = event_density(dataset, data_sel_outpix, pix_position) + event_density(dataset, data_sel_inpix, pix_position, inpix=True)
    return skymaps


 #  ____         ___  ____
 # |  _ \ _   _ / _ \|  _ \
 # | |_) | | | | | | | | | |
 # |  __/| |_| | |_| | |_| |
 # |_|    \__, |\___/|____/
 #        |___/

# def generate_data_train_inlier(n_train, n_test, n_features, contamination_threshold, test_contamination):
#     """Generate train and test data with no outliers in trainning.
#
#     Args:
#         n_train: Number of trainning samples.
#         n_test: Number of test samples.
#         n_features: Number of features/dimensions of each data point.
#         contamination_threshold: Proportion of false positive we want in the training (only inliers) sample.
#         test_contamination: Proportion of outliers injected in the test sample.
#     Returns:
#         X_train: Training sample. (array of dim n_train*n_features)
#         X_test: Testing sample. (array of dim n_test*n_features)
#         y_train: Outlier-ness of train events, 0 for inliers, 1 for outliers. (array of dim n_train)
#         y_test: Outlier-ness of test events, 0 for inliers, 1 for outliers. (array of dim n_test)
#     """
#     X_train, _, y_train, _ = \
#         pyod.generate_data(n_train=n_train,
#                            n_test=1,
#                            n_features=n_features,
#                            contamination=contamination_threshold,
#                            random_state=43,
#                            behaviour="new")
#
#     X_train = X_train[y_train == 0]
#     y_train = y_train[y_train == 0]
#
#     _, X_test, _, y_test = \
#         pyod.generate_data(n_train=1,
#                            n_test=n_test,
#                            n_features=n_features,
#                            contamination=test_contamination,
#                            random_state=43,
#                            behaviour="new")
#
#     return X_train, X_test, y_train, y_test
def skymaps_to_data(dataset_list, dic_skymaps, timeline, min_vals=None, max_vals=None, rescale=True):
    """Convert dic_skymaps to data with the desired format.

    Args:
        dataset_list: List of dataset names. (list of strings)
        dic_skymaps: Dictionary containing the skymaps dataframes for each dataset. (dict of pd.DataFrame)
        timeline: List of pd.Timestamp.
    Returns:
        data: np.array of data.
    """
    for dataset in dataset_list:
        n_col = len(dic_skymaps[dataset].columns[2:])
        if len(timeline) != n_col:
            raise ValueError("Number of columns in dic_skymaps {dataset} {n_col} different from number of timesteps in timeline {n_tsteps}. "\
                             "begin and end must correspond to custom_filename."
                             .format(dataset=dataset, n_col=n_col, n_tsteps=len(timeline)))
    if rescale and np.any(['Fermi' in dataset for dataset in dataset_list]):
        density_col = dic_skymaps['Fermi'][dic_skymaps['Fermi'].columns[2:]] # all columns except ra_rad, dec_rad
        dic_skymaps['Fermi'][dic_skymaps['Fermi'].columns[2:]] = density_col.divide(density_col.mean(axis=1), axis='rows')
        dic_skymaps['Fermi'].fillna(0, inplace=True)
    pix_eq = SkyCoord(ra=np.array(dic_skymaps[dataset_list[0]]["ra_rad"]) * u.rad, dec=np.array(dic_skymaps[dataset_list[0]]["dec_rad"]) * u.rad, frame=ICRS)
    # data = np.empty((0, len(dataset_list)+2))
    arrays = []
    for index, time in enumerate(timeline):
        ref_frame = AltAz(location=REF_LOCATION, obstime=time)
        pix_ref = pix_eq.transform_to(ref_frame) # Direction of pixels in respect to earth position
        data_thistime = [dic_skymaps[dataset][index].tolist() for dataset in dataset_list]# + [pix_ref.az.deg] + [np.sin(pix_ref.alt.rad)] I removed alt az, it is better without
        arrays += [np.array(data_thistime).T]
        # data = np.concatenate((data, np.array(data_thistime).T))
    data = np.concatenate(arrays)

    if rescale:
        for i in range(data.shape[1]):
            if not min_vals or not max_vals:
                min_val = min(data[:, i]) # used to rescale sin(alt) to [0, 1]
                if min_val >= 0 and min_val < 0.2:
                    min_val = 0
                max_val = max(data[:, i])
                if max_val >= 0 and max_val < 1e-2: # If event density is always 0, no need to rescale
                    max_val = 1.
            else:
                min_val = min_vals[i]
                max_val = max_vals[i]
            data[:, i] = (data[:, i] - min_val)/(max_val - min_val)

    return data, min_vals, max_vals

def split_to_train_test(opt, data):
    """Split data into train and test samples. data can be X or y.
    The data are split in two and we pick up random data points in the first part for the training sample
    so that the proportion of training data points corresponds to train_prop.
    The test sample is the second part.

    Args:
        opt: Option arguments of the script. (dict)
        data: Data to split. (np.array)
    Returns:
        data_train: Training input data.
        data_test: Testing input data.
    """
    if opt['train_prop'] > TRAIN_SPLIT:
        raise ValueError("train_prop should be <= TRAIN_SPLIT (i.e. {train_split})".format(train_split=TRAIN_SPLIT))
    n_steps = len(get_timeline(opt))
    npix = hp.nside2npix(opt['nside'])
    if len(data) != n_steps * npix:
        raise ValueError("data size is {size} does not correspond to n_steps * npix = {expected}.".format(size=len(data), expected=n_steps * npix))
    split_index = get_split_index(opt)  # In order to not split the data of a time step in two we use the ceil of n_steps*train_split

    n_train_points = int(opt['train_prop']*n_steps*npix)
    full_data_train = data[:split_index]
    rng = np.random.default_rng(SEED) # NB it is necessary to have a constant seed to have X corresponding to y
    # TODO we could choose the seed in opt to be able to have different mo-gaal but that would make necessary to have a different folder for each seed. Or we would need to save y_all and X_all to redo just the split for each seed
    data_train = rng.choice(full_data_train, n_train_points, replace=False)

    data_test = data[split_index:]
    return data_train, data_test

# def split_to_train_test(data, train_prop=0.5):
#     """Split data into train and test samples with a proportion of train_prop train. data can be X or y.
#
#     Args:
#         data: Data to split. (np.array)
#         train_prop: Proportion of train samples.
#     Returns:
#         data_train: Training input data.
#         data_test: Testing input data.
#     """
#     split_index = int(len(data)*train_prop)
#     data_train = data[:split_index]
#     data_test = data[split_index:]
#     return data_train, data_test


# Simulate signal
def get_random_source_df(n_src, begin, end, dataset_list=[]):
    """Draw random times, ra and dec to simulate sources.

    Args:
        n_src: Number of sources to simulate.
        begin: Beginning of the timeline. (pd.Timestamp)
        end: End of the timeline. (pd.Timestamp)
        dataset_list: List of dataset names. (list of strings)
    Returns:
        rdm_src_df: pd.Dataframe with columns time, ra, dec and n_src rows.
    """
    rdm_src_array = [get_random_times(begin, end, n_src), # rdm time
                     np.random.random(n_src)*360., # rdm ra
                     np.degrees(np.arcsin(np.random.random(n_src)*2.-1.)),] # rdm dec
    rdm_src_df = pd.DataFrame(rdm_src_array).T
    rdm_src_df.columns = ['time', 'ra', 'dec']
    rdm_src_df = add_all_local_coords(dataset_list, rdm_src_df)
    return rdm_src_df

def get_random_src_err68(dataset, event):
    """Get random 68% src error.

    Args:
        dataset: String of the experiment name.
        event: Event corresponding to a row of the dataframe, it should contain '<dataset>-alt' column.
    Returns:
        src_err68: Random event reconstruction uncertainty from which we will draw a distance to the source using a Rayleigh distribution. (float)
    """
    if 'ANTARES' in dataset:
        src_err68 = DATASETS[dataset]['err68%']
    elif any([dset in dataset for dset in ['Fermi', 'HAWC_hotspots']]):
        src_err68 = DATASETS[dataset]['min_err68%'] + np.random.random(1) * (DATASETS[dataset]['max_err68%'] - DATASETS[dataset]['min_err68%'])
    elif 'HAWC_bursts' in dataset:
        choices = [DATASETS[dataset]['err68%_0'], DATASETS[dataset]['err68%_1'], DATASETS[dataset]['err68%_2']]
        src_err68 = np.random.choice(choices, 1)
    elif 'IceCube' in dataset:
        if event[dataset+'-alt'] > 0:
            src_err68 = DATASETS[dataset]['min_err68%'] + np.random.random(1)*(DATASETS[dataset]['max_err68%_lowalt'] - DATASETS[dataset]['min_err68%'])
        else:
            src_err68 = DATASETS[dataset]['min_err68%'] + np.random.random(1)*(DATASETS[dataset]['max_err68%_highalt'] - DATASETS[dataset]['min_err68%'])
    return float(src_err68)

def remove_useless_columns(dataset, dataframe):
    """Remove useless columns of the dataframe.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe.
    Returns:
        dataframe: Pandas dataframe.
    """
    if dataset == 'IceCube':
        dataframe.drop(['sigmaR', 'energy', 'src_error90', 'src_error'], axis=1, inplace=True)
    elif dataset == 'Fermi':
        dataframe.drop(['sigmaR', 'point_RA', 'point_Dec', 'inclination', 'conversion'], axis=1, inplace=True)
    elif dataset == 'HAWC_hotspots':
        dataframe.drop(['sigmaR'], axis=1, inplace=True)
    elif dataset == 'HAWC_bursts':
        dataframe.drop(['sigmaR', 'false_pos'], axis=1, inplace=True)
    elif dataset == 'ANTARES':
        pass
    if 'id' in dataframe.columns:
        dataframe.drop(['id'], axis=1, inplace=True)
    if 'alt' in dataframe.columns and 'az' in dataframe.columns:
        dataframe.drop(['alt', 'az'], axis=1, inplace=True)
    return dataframe

def get_empty_sig_dfs(dataset_list):
    """Get a dic of empty dataframes with the columns corresponding to each dataset.

    Args:
        dataset_list: List of dataset names. (list of strings)
    Returns:
        sig_dfs: Dictionnary of empty pd.DataFrame containing the corresponding columns of each dataset. (dic of pd.DataFrame)
    """
    sig_dfs = {'comb_datasets':[]}
    for dataset in dataset_list:
        columns_list = DATASETS[dataset]['df_columns']
        # for col in DATASETS[dataset]['unnecessary_columns']:
        #     columns_list.remove(col)
        sig_dfs[dataset] = remove_useless_columns(dataset, pd.DataFrame(columns=columns_list))
    return sig_dfs


def add_all_local_coords(dataset_list, src_df):
    """Add local coordinates of each detector of the dataset_list (except Fermi) to the dataframe.

    Args:
        dataset_list: List of dataset names. (list of strings)
    Returns:
        src_df: pd.DataFrame with local coords written as <dataset>-az and <dataset>-alt. (pd.DataFrame)
    """
    for dataset in dataset_list:
        if not 'Fermi' in dataset:
            src_df = equatorial_to_local(dataset, src_df, dataset_name=True)
    return src_df


def interpolate_dur_dec_HAWC_hotspots():
    """Interpolate duration versus declination plot of HAWC_hotspots using a spline.
    """
    dataset = 'HAWC_hotspots'
    dataframe = get_data(dataset)
    df_scram = scramble_data(dataset, dataframe)

    n = 100000
    x = sorted(df_scram[:n].dec)
    y = [dur for _, dur in sorted(zip(df_scram[:n].dec, df_scram[:n].duration))]

    spl = interpolate.splrep(x, y, s=0.1)

    plot_dur_dec_HAWC_hotspots_spline(spl, x, y)
    return spl


def get_y(opt, src_injected):
    """Get the y array telling for each data point if there is a source leading to coincidence at this time and position (1) or not (0).

    Args:
        opt: Option arguments of the script. (dict)
        src_injected: Time and position information of the sources for which we have injected signal.
                      Set it to None if no sources injected. (pd.DataFrame or None)
    Returns:
        y: Array of 0 and 1, 1 if signal injected at this time and position, 0 else. Warning: pixels close to injection are set to 0. (1D np.array)
    """
    timeline = get_timeline(opt)
    npix = hp.nside2npix(opt['nside'])

    y = np.zeros(npix*len(timeline), dtype=int)
    if src_injected is None:
        return y

    src_pixels = ang2pix_eq(opt['nside'], np.radians(src_injected.dec), np.radians(src_injected.ra))
    time_indices = np.where([(time > np.array(timeline)) * (time-opt['timestep'] < np.array(timeline)) for time in src_injected.time])[1]

    for index in list(time_indices*npix+src_pixels):
        y[index] += 1
    if any(y > 1):
        print("Warning: two coincidences are on the same pixel at the same datetime.\n"\
              "Be careful, when two coincidences are in adjacent pixels this warning won't show.")
        print_info_coinc(y, (y > 1), timeline, opt['nside'], show_num_coinc=True)
        y = np.where(y > 0, 1, 0)
    return y


def get_comb_list(opt, begin, end):
    """Get list of dataset combinations corresponding to number of sources we want and n_comb_inj.

    Args:
        opt: Option arguments of the script. (dict)
        begin: Beginning of the timeline. (pd.Timestamp)
        end: End of the timeline. (pd.Timestamp)
    Returns:
        comb_list: List of list of indices of datasets corresponding to the combination of events we want for each source. (list)
    """
    if isinstance(opt['n_comb_inj'], int) and opt['n_comb_inj'] > len(opt['dataset_list']):
        raise ValueError("n_comb_inj is larger than the number of datasets in dataset_list")
    n_src = opt['n_src_per_month']*((end-begin).days/30)
    combs = all_combinations(range(len(opt['dataset_list'])), opt['n_comb_inj'], opt['dataset_list'])
    comb_list = combs*int(n_src/len(combs))
    if not comb_list:
        comb_list = combs
        print("Warning: Injecting minimum number of sources: {n_src}".format(n_src=len(comb_list)))
    return comb_list

def get_injection_contamination(opt, n_src, begin, end, verbose=True):
    """Get proportion of injected sources in the number of datapoints.

    Args:
        opt: Option arguments of the script. (dict)
        n_src: Number of sources to simulate.
        begin: Beginning of the injection timeline. (pd.Timestamp)
        end: End of the injection timeline. (pd.Timestamp)
    Returns:
        contam_inj: Proportion of sources over total number of datapoints. (float)
    """
    contam_inj = n_src/(hp.nside2npix(opt['nside']) * len(get_timeline({'begin': begin, 'end': end, 'timestep': opt['timestep']})))
    if contam_inj > 0.5 or contam_inj <= 0:
        raise ValueError("Contamination should be in [0, 0.5], got {}".format(contam_inj))
    if verbose:
        print("{n_src} sources to inject. Contamination: {cont}%".format(n_src=n_src, cont='{:.3}'.format(contam_inj*100)))
    return contam_inj

def get_trigg_dets(dataset_list, src, nside, fermi_mask):
    """Get list of indices of triggered detectors by source src.

    Args:
        dataset_list: List of dataset names. (list of strings)
        src: pd.DataFrame with columns time, ra, dec and <dataset>-alt, <dataset>-az.
        nside: nside of skymaps. Same used for the Fermi mask. (int)
        fermi_mask: Mask of the galactic plane obtained with mask_Fermi().
    Returns:
        trigg_dets: List of indices of detectors triggered by source src.
    """
    src_pix = ang2pix_eq(nside, np.radians(src.dec), np.radians(src.ra))
    is_det_trigg = [(dataset == 'Fermi' and not fermi_mask[src_pix]) or (dataset != 'Fermi' and src[dataset+'-alt'] < DATASETS[dataset]['alt_max'] and src[dataset+'-alt'] > DATASETS[dataset]['alt_min']) for dataset in dataset_list]
    return [i for i in range(len(dataset_list)) if is_det_trigg[i]]

def get_n_to_inj(dataset):
    """Get number of events to inject depending on dataset.

    Args:
        dataset: String of the experiment name. (str)
    Returns:
        n_to_inj: Number of events to inject. (int)
    """
    n_to_inj = DATASETS[dataset]['n_to_inj']
    if n_to_inj != 1:
        n_to_inj = max(1, np.random.poisson(n_to_inj))
    return n_to_inj

def get_random_signif(dataset, min_quantile=0.5):
    """Get a random value for the pval/signalness/etc of the dataset from the distribution of the 1-min_quantile (50%) of the most sigificant
    background events in order to approximate signal like events.

    Args:
        dataset: String of the experiment name. (str)
        min_quantile: Quantile of the minimum value, ie the generated value is at least higher than min_quantile (50%) of the events. (float)
    Returns:
        random_signif: Randomly generated value of the pvalue/signalness/etc use to estimate the significance/signalness of an event. (float)
        signif_name: Name of the quantity used to estimate the significance, eg: pvalue, signalness, signif. (str)
    """
    bin_midpoints = DIC_INTERP_SIGNIF['bin_midpoints'][dataset]
    cdf = DIC_INTERP_SIGNIF['cdf'][dataset]

    random_quantiles = min_quantile+np.random.rand(1)*(1-min_quantile)
    random_bins = np.searchsorted(cdf, random_quantiles)
    random_values = bin_midpoints[random_bins]

    if dataset == 'ANTARES':
        random_signif = np.power(10., -random_values)[0]
        signif_name = 'pvalue'
    elif dataset == 'HAWC_bursts':
        random_signif = np.power(10., -random_values)[0]
        signif_name = 'pvalue'
    elif dataset == 'IceCube':
        random_signif = random_values[0]
        signif_name = 'signalness'
        # random_signif = np.power(10., random_values)[0]
        # signif_name = 'energy'
    elif dataset == 'Fermi':
        random_signif = np.power(10., random_values)[0]
        signif_name = 'energy'
    elif dataset == 'HAWC_hotspots':
        random_signif = random_values[0]
        signif_name = 'signif'

    return random_signif, signif_name

def sim_evt(dataset, src, src_eq, signal):
    """Simulate an event from src dataframe and dataset

    Args:
        dataset: String of the experiment name. (str)
        src: pd.DataFrame with columns time, ra, dec and <dataset>-alt, <dataset>-az.
        src_eq: Astropy Skycoord of the source in equatorial coordinates.
    Returns:
        evt_df: pd.DataFrame of event to add to main dataframe of the dataset.
    """
    src_err68 = get_random_src_err68(dataset, src)
    min_quantile = 0.5 if signal else 0.
    signif, signif_name = get_random_signif(dataset, min_quantile)
    rdm_dist = float(np.random.rayleigh(src_err68/1.515)) * u.deg # get rdm distance from source using rayleigh distrib
    rdm_phi = float(np.random.random(1)*360.) * u.deg
    src_eq = SkyCoord(src.ra*u.deg, src.dec*u.deg, frame='icrs')
    evt_eq = src_eq.directional_offset_by(rdm_phi, rdm_dist)

    evt_df = pd.DataFrame({'time': [src.time], 'dec': [evt_eq.dec.deg], 'ra': [evt_eq.ra.deg], 'src_err68': [src_err68], signif_name: [signif]})

    if 'HAWC_bursts' in dataset:
        deltaT = np.random.choice(DATASETS[dataset]['deltaT_choices'], 1)
        evt_df['deltaT'] = deltaT
    elif 'HAWC_hotspots' in dataset:
        duration = interpolate.splev(evt_eq.dec.deg, DATASETS[dataset]['dur_dec_HAWC_hotspots_spline'], der=0)
        MJD_c = Time(src.time).mjd
        MJD_i = MJD_c - duration/2.
        MJD_f = MJD_c + duration/2.
        time = Time(MJD_f, format='mjd').datetime

        evt_df['MJD_c'] = MJD_c
        evt_df['MJD_i'] = MJD_i
        evt_df['MJD_f'] = MJD_f
        evt_df['time'] = time
        evt_df['duration'] = duration

    if 'time_msec' in DATASETS[dataset]['df_columns']:
        evt_df['time_msec'] = 0
    return evt_df

def injecting_events(opt, rdm_pos_df, comb_list, dataset_list, fermi_mask, signal):
    """Simulate signal sources and coincident events or inject background.

    Args:
        opt: Option arguments of the script. (dict)
        rdm_pos_df: pd.Dataframe of random position and time with columns time, ra, dec.
        comb_list: List of list of indices of datasets corresponding to the combination of events we want for each source.
                   Only one dataset each if we want to inject background. (list)
        dataset_list: List of dataset names. (list of strings)
        fermi_mask: Healpy skymap of 0 and 1, 1 corresponding to the galactic plane pixels to mask for fermi data. (np.array)
        signal: True if we inject signal, False either. (bool)

    Returns:
        evts_dfs: Dictionnary of pd.DataFrame of each dataset containing all the events with also the 'comb_datasets' key containing
                  a list of strings of the combined datasets for signal.
        pos_injected: pd.Dataframe of injected positions, i.e. position of the source if signal, useless if background.
    """
    evts_dfs = get_empty_sig_dfs(dataset_list)
    evts_dfs['comb_datasets'] = []
    sort_comb = []
    pos_injected = pd.DataFrame()

    for _, pos in rdm_pos_df.iterrows():
        comb_datasets = ''
        trigg_det = get_trigg_dets(dataset_list, pos, opt['nside'], fermi_mask)
        all_trigg_comb = all_combinations(trigg_det)
        if all([len(comb) == 1 for comb in comb_list]): # Used to inject background events when not enough in one dataset
            all_trigg_comb = [[det] for det in range(len(dataset_list))]
        for trigg_comb in all_trigg_comb:
            if trigg_comb in comb_list:
                pos_eq = SkyCoord(pos.ra*u.deg, pos.dec*u.deg, frame='icrs')
                comb_list.remove(trigg_comb)
                pos_injected = pos_injected.append(pos, ignore_index=True)
                for det_id in trigg_comb:
                    dataset = dataset_list[det_id]
                    n_to_inj = get_n_to_inj(dataset)
                    for _ in range(n_to_inj):
                        # Add a row to the dataset with the event # NB unspecified keys/columns are filled with NaN
                        evts_dfs[dataset] = evts_dfs[dataset].append(sim_evt(dataset, pos, pos_eq, signal), ignore_index=True)
                    if dataset in SHORTEN_DATASET_NAMES.keys():
                        dataset = SHORTEN_DATASET_NAMES[dataset]
                    comb_datasets += dataset+'+'
                timestep = int((pos.time - opt['begin'])/opt['timestep'])
                pixel = ang2pix_eq(opt['nside'], np.radians(pos.dec), np.radians(pos.ra))
                sort_comb += [[timestep, pixel]]
                evts_dfs['comb_datasets'] += [comb_datasets[:-1]]
                break

        if not comb_list: # if empty
            break

    if comb_list: # if not empty
        raise ValueError("{n} missing combinations {missing_comb}".format(n=len(comb_list), missing_comb=comb_list)) # eventually increase n_sim_pos if this error comes too often

    evts_dfs['comb_datasets'] = [comb_sets for _, comb_sets in sorted(zip(sort_comb, evts_dfs['comb_datasets']))] # reorder comb_datasets by injection time
    return evts_dfs, pos_injected


def get_sig_inj_begin_time(opt):
    """Get begin time after which we inject signal.

    Args:
        opt: Option arguments of the script. (dict)
    Returns:
        begin_sig_inj: Beginning of the injection time. (pd.Timestamp)
    """
    begin_sig_inj = opt['begin']
    if not opt['sig_in_train']:
        n_training_steps = math.ceil(len(get_timeline(opt))*TRAIN_SPLIT)
        begin_sig_inj = opt['begin'] + n_training_steps*opt['timestep'] # we inject signal in test only
    return begin_sig_inj

def combine_skymap_jobs(opt, range_jobs, is_nosig=False):
    """Get all the skymaps obtained from the multiple jobs and combine them in two files, one for sig and one for nosig.

    Args:
        opt: Option arguments of the script. (dict)
        range_jobs: list of the jobs ids.
        is_nosig: True to combine nosig files, ie files of the test sample without signal injected (used to compute the FAR).
    Returns:
        dic_skymaps: Dictionnary of the dataframes of the skymaps of each dataset. (dict)
    """
    dic_skymaps = {}
    nosig = '_nosig' if is_nosig else ''
    for job in range_jobs:
        job_filename = 'dic_skymaps{nosig}_{job}.pkl'.format(nosig=nosig, job='{0:02d}'.format(job))
        skymaps_path_job = os.path.join(opt['out_path'], job_filename)
        if not os.path.exists(skymaps_path_job):
            raise FileNotFoundError("{filename} does not exists. Cannot combine dic_skymaps{nosig}_<job>.pkl files.".format(filename=job_filename, nosig=nosig)) # pylint: disable=E0602
        dic_skymaps_job = load_obj(skymaps_path_job)
        if job == range_jobs[0]:
            dic_skymaps['begin'] = dic_skymaps_job['begin']
        if job == range_jobs[-1]:
            dic_skymaps['end'] = dic_skymaps_job['end']

        for dataset in opt['dataset_list']:
            if job == range_jobs[0]:
                dic_skymaps[dataset] = dic_skymaps_job[dataset]
            else:
                timeline = get_timeline(opt)
                # n_jobs_sig = math.ceil(opt['n_jobs']/(1+(1-TRAIN_SPLIT)))
                n_jobs = len(range_jobs)
                if is_nosig:
                    split_index = math.ceil(len(timeline)*TRAIN_SPLIT)
                    timeline = timeline[split_index:] # here timeline is only the test part
                    # n_jobs = max(opt['n_jobs'] - n_jobs_sig, 1)
                job_first_timestep = int((job-min(range_jobs))*len(timeline)/n_jobs)
                job_skymaps_df = dic_skymaps_job[dataset].drop(columns=['ra_rad', 'dec_rad'])
                timesteps_corr = {step: step + job_first_timestep for step in job_skymaps_df.columns} # timesteps jobs are all starting at 0, we correct for this
                job_skymaps_df.rename(timesteps_corr, axis='columns', inplace=True)
                dic_skymaps[dataset] = pd.concat([dic_skymaps[dataset], job_skymaps_df], axis=1)
    return dic_skymaps

def get_contamination(opt, period=pd.Timedelta('365 days')):
    """Set the contamination to be one *pixel* per period. Contamination is used for the threshold fit.

    Args:
        opt: Option arguments of the script. (dict)
        period: Period for which we want one pixel above threshol in average. (pd.Timedelta)
    Returns:
        contamination: Proportion of pixels containing injected signal.
    """
    begin_sig_inj = get_sig_inj_begin_time(opt)
    n_timesteps = len(get_timeline({'begin': begin_sig_inj, 'end': opt['end'], 'timestep': opt['timestep']}))
    contamination = (opt['end'] - begin_sig_inj)/period/(hp.nside2npix(opt['nside']) * n_timesteps)
    return contamination

def get_detections(opt, y_test_scores, y_test, threshold, verbose=False):
    """Get a dictionnary containing the detections that are spatially distinct.
    (In order to distinguish nearby pixels of a same signal from distinct sources.)

    Args:
        opt: Option arguments of the script. (dict)
        y_test_scores: y scores of the test sample. (list)
        threshold: Threshold above which an event is considered an outlier. (float)
        verbose: If True, print detail of detections. (bool)
    Returns:
        detections: Dictionnary of 'pix', 'ra', 'dec', 'score' of each detection for each time id.
            Also contains 'all_scores' which is the list of the scores of all the detections of all time steps.
    """
    npix = hp.nside2npix(opt['nside'])
    resolution = np.degrees(hp.nside2resol(opt['nside'])) # pylint: disable=E1111
    first_test_tid = int(get_split_index(opt)/npix)
    time_ids = range(len(get_timeline(opt)))
    detections = {'pix':{tid:[] for tid in time_ids if tid >= first_test_tid},
                  'ra':{tid:[] for tid in time_ids if tid >= first_test_tid},
                  'dec':{tid:[] for tid in time_ids if tid >= first_test_tid},
                  'score':{tid:[] for tid in time_ids if tid >= first_test_tid},
                  'y':{tid:[] for tid in time_ids if tid >= first_test_tid},
                 }

    for tid in time_ids:
        if tid < first_test_tid:
            continue
        tid_first_pix = (tid-first_test_tid)*npix
        tid_last_pix = (tid-first_test_tid+1)*npix
        skymap_score = np.array(y_test_scores[tid_first_pix:tid_last_pix])
        y = np.array(y_test[tid_first_pix:tid_last_pix])

        pix_max = np.argmax(skymap_score)
        while skymap_score[pix_max] > threshold:
            detections['pix'][tid] += [pix_max]
            detections['ra'][tid] += [np.degrees(pix2ang_eq(opt['nside'], pix_max)[1])]
            detections['dec'][tid] += [np.degrees(pix2ang_eq(opt['nside'], pix_max)[0])]
            detections['score'][tid] += [skymap_score[pix_max]]
            detections['y'][tid] += [y[pix_max]]
            around_max = pix_max
            radius = resolution/2 # deg
            while np.any(skymap_score[around_max] > threshold):
                skymap_score[around_max] = 0
                if detections['y'][tid][-1] == 0:
                    detections['y'][tid][-1] = max(np.array(y[around_max], ndmin=1))
                around_max = hp.query_disc(opt['nside'], hp.pix2vec(opt['nside'], pix_max), np.radians(radius), inclusive=True)
                radius += resolution
            pix_max = np.argmax(skymap_score)
    detections['all_scores'] = [det for tid in time_ids if tid >= first_test_tid for det in detections['score'][tid]]

    if verbose:
        n_detections = len(detections['all_scores'])
        print(n_detections, 'detections')
        for tid in time_ids:
            if tid < first_test_tid:
                continue
            print('Time id', tid)
            print('Score\t\t', detections['score'][tid])
            print('Pixels\t\t', detections['pix'][tid])
            print('Ra (deg)\t', detections['ra'][tid])
            print('Dec (deg)\t', detections['dec'][tid])
            print('y\t', detections['y'][tid])
            print()

    return detections

def get_detections_dataset(opt, all_detections, sel_dataset_evts):
    """Get the detections corresponding to pixels with an event in <dataset>.

    Args:
        opt: Option arguments of the script. (dict)
        all_detections: Dictionnary containing all the detections. (dict)
        sel_dataset_evts: Mask, list of booleans, with True for pixels containing an event in dataset.
    Returns:
        detections: Dictionnary of 'pix', 'ra', 'dec', 'score' of each detection of dataset events for each time id.
            Also contains 'all_scores' which is the list of the scores of all the detections of all time steps.
    """
    det_datapoints = list(compress(range(len(sel_dataset_evts)), sel_dataset_evts))
    npix = hp.nside2npix(opt['nside'])
    first_test_tid = min(all_detections['pix'].keys())
    detections = {'pix': {}, 'ra': {}, 'dec': {}, 'score': {}, 'y': {}}
    for time_id, pixels in all_detections['pix'].items():
        for key in detections:
            detections[key][time_id] = []
        for evt, pixel in enumerate(pixels):
            datapoint = (time_id-first_test_tid)*npix+pixel
            if datapoint in det_datapoints:
                for key in detections:
                    detections[key][time_id] += [all_detections[key][time_id][evt]]

    detections['all_scores'] = [det for tid in detections['score'] for det in detections['score'][tid]]
    return detections

def far_distrib(opt, det_nosig_scores, det_scores, y_truth, threshold, filename_ext=''):
    """Get function of false alarm rate from detection score of nosig and plot it as well as false alarm rate distribution of sig.

    Args:
        opt: Option arguments of the script. (dict)
        det_nosig_scores: List of all scores of detections of the nosig test sample from dic_Xy['detections_nosig']['all_scores']. (list)
        det_scores: Dictionnary of the scores of each timestep from dic_Xy['detections']['score']. (dict)
        threshold: Threshold of the score above which we use detections.
        filename_ext: Extension to the file name. (str)
    Return:
        f_far: False alarm rate in function of score. (interp1d function)
    """
    npix = hp.nside2npix(opt['nside'])
    test_begin_tid = int(get_split_index(opt)/npix)
    timeline = get_timeline(opt)
    one_year = 365.25
    norm2year = np.ones(len(det_nosig_scores))*one_year/((opt['end']-timeline[test_begin_tid])/pd.Timedelta('1 day'))

    plt.rcParams.update({'font.size': 18})
    plt.figure(figsize=(6 * 1.618, 6))
    xmin = 0#threshold/2.
    xmax = max(det_nosig_scores)*1.1
    y, bins, _ = plt.hist(det_nosig_scores, cumulative=-1, weights=norm2year, bins=np.linspace(xmin, xmax, 1001))
    # plt.axvline(x=threshold, color='k', lw=2, linestyle='--')
    # FAR_threshold = y[np.searchsorted(bins, threshold)]
    # plt.plot([bins[0], threshold], [FAR_threshold, FAR_threshold], color='k', lw=2, linestyle='--')
    plt.xlim([xmin, xmax])
    # plt.ylim([0, 100])
    # plt.yscale('log')
    plt.xlabel('Detection score')
    plt.ylabel('False Alarm Rate [yr-1]')

    if opt['write_files'] != 0:
        if filename_ext != '':
            filename_ext = '_'+filename_ext

        if '_nosig' in filename_ext:
            match_index = filename_ext.index('_nosig')
            filename_ext_score = filename_ext[:match_index] + filename_ext[match_index+6:]
        else:
            filename_ext_score = filename_ext
        plt.savefig(os.path.join(opt['out_path'], opt['clf_name']+'_FAR_score'+filename_ext_score+'.png'), bbox_inches='tight', facecolor='white')
        print('False Alarm Rate in function of score plot saved.')

    x = bins[:-1]
    f_far = interpolate.interp1d(x, y, bounds_error=False, fill_value=(y[0], y[-1]))

    all_far = []
    for tid in det_scores:
        # if ((det_scores[tid] > max(x)).any() and ) or (det_scores[tid] < min(x)).any():
        #     print("Warning: A value in det_scores is above (resp. below) the interpolation range. Using the max (resp. min) far. \n"
        #           "det_score = {det_score}, x range = {x_min}, {x_max}"
        #           .format(det_score=det_scores[tid], x_min=min(x), x_max=max(x)))
        all_far += list(f_far(det_scores[tid]))
    all_far = np.array(all_far)

    all_y = np.array(np.concatenate([y_tid for y_tid in y_truth.values()]), dtype=bool)

    for x_max in [100, max(all_far)]:
        # mpl_tex_rc(sans=True) TODO fix that
        plt.figure(figsize=(6 * 1.618, 6))
        plt.xlabel('False Alarm Rate [yr-1]')
        binning = np.linspace(0, x_max, 200)

        all_far_stacked = [all_far[(all_y == 1)],
                           all_far[(all_y == 0)]]
        labels = ['Signal', 'Background']
        colors = ['tab:blue', 'tab:red']
        plt.hist(all_far_stacked, stacked=True, histtype='bar', lw=2, label=labels, color=colors, bins=binning)# bins=binning)#
        plt.legend()

        if opt['write_files'] != 0:
            plt.savefig(os.path.join(opt['out_path'], opt['clf_name']+'_FAR_distrib_'+str(x_max)+filename_ext+'.png'), bbox_inches='tight', facecolor='white')
            print('False Alarm Rate distribution plot saved.')

    return f_far

def dataset_preds_Xys(opt, test_evt_sel, train_evt_sel, dic_pred, dic_Xy):
    """Get dic_pred and dic_Xy for pixels passing the evt_sel, usually pixels for which there is data in a dataset with rare events.
    Args:
        opt: Option arguments of the script. (dict)
        test_evt_sel: Mask of the pixels we want to select for the test set. (list)
        train_evt_sel: Mask of the pixels we want to select for the train set. (list)
        dic_pred: Dictionnary of predictions and scores. (dict)
        dic_Xy: Dictionnary X and y. (dict)
    Returns:
        dic_pred_dset: dic_pred for only events passing the evt_sel.
        dic_Xy_dset: dic_Xy for only events passing the evt_sel.
    """
    dic_pred_dset = {}
    dic_pred_dset['y_test_scores'] = dic_pred['y_test_scores'][test_evt_sel]
    dic_pred_dset['y_test_nosig_scores'] = dic_pred['y_test_nosig_scores'][test_evt_sel]
    dic_pred_dset['y_train_scores'] = dic_pred['y_train_scores'][train_evt_sel]

    if dic_pred_dset['y_train_scores'].size == 0:
        print("Warning: dic_pred_dset['y_train_scores'] is empty. Using 0.5 as threshold")
        threshold = 0.5
    else:
        threshold = np.quantile(dic_pred_dset['y_train_scores'], 1-get_contamination(opt))
    print("Threshold is", threshold)
    dic_pred_dset['y_test_pred'] = dic_pred_dset['y_test_scores'] > threshold
    dic_pred_dset['y_test_nosig_pred'] = dic_pred_dset['y_test_nosig_scores'] > threshold
    dic_pred_dset['y_train_pred'] = dic_pred_dset['y_train_scores'] > threshold

    dic_Xy_dset = {}
    dic_Xy_dset['y_test'] = dic_Xy['y_test'][test_evt_sel]

    return dic_pred_dset, dic_Xy_dset

def sel_dataset_evts(dataset, dataset_list, evt_thres, X_data):
    """Get mask to apply to data to select pixels in which there is an event of the given dataset.

    Args:
        dataset: Dataset for which we want to select pixels containing an event. (str)
        dataset_list: List of all datasets in the data. (list)
        evt_thres: Dictionnary containing at least the threshold we use to consider an event density as an event for the dataset we are interested in. (dict)
        X_data: The content of dic_Xy['X_train'] or dic_Xy['Y_train'] depending on which we are interested in. (array)
    Returns:
        selection: Mask, list of booleans, with True for pixels corresponding to event density higher than threshold.
    """
    data = X_data[:, dataset_list.index(dataset)]
    selection = data > max(data)*evt_thres[dataset]
    return selection

def get_signif_var(dataset, dataframe):
    """Get the variable we use to estimate significance of the event for all events in the dataframe.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with eventually 'sigmaR' or 'src_error' columns.
    Returns:
        signif_var: List of the variable we use to estimate significance of the event. (list)
    """
    if dataset == 'ANTARES':
        signif_var = -np.log10(np.array(dataframe.pvalue.dropna(), dtype=float))
    elif dataset == 'HAWC_bursts':
        signif_var = -np.log10(np.array(dataframe.pvalue.dropna(), dtype=float))
    elif dataset == 'IceCube':
        signif_var = np.array(dataframe.signalness.dropna(), dtype=float)
        # signif_var = np.log10(np.array(dataframe.energy.dropna(), dtype=float))
    elif dataset == 'Fermi':
        signif_var = np.log10(np.array(dataframe.energy.dropna(), dtype=float))
    elif dataset == 'HAWC_hotspots':
        signif_var = np.array(dataframe.signif.dropna(), dtype=float)
    return signif_var

def get_scaling_factor(dataset, dataframe):
    """Add the scaling factor as a new column of the dataframe.

    Args:
        dataset: String of the experiment name.
        dataframe: Pandas dataframe with at least the pvalue, or significance etc depending on the dataset.
    Returns:
        dataframe: Pandas dataframe with with the scaling_factor column.
    """
    scaling_factor = DIC_INTERP_SIGNIF['to_scaling_factor'][dataset](get_signif_var(dataset, dataframe))
    dataframe.insert(4, 'scaling_factor', scaling_factor)
    return dataframe

def masks_for_FAR(opt, evt_thres, X_data):
    """Get the masks to select datapoints with one of the rare events datasets, or others or both.

    Args:
        opt: Option arguments of the script. (dict)
        evt_thres: Dictonnary of threshold to define if pixel contains event of rare datasets or not. (dict)
        data: List of datapoints containing the event densities of each dataset, eg: dic_Xy['X_test']. (list)
    Returns:
        masks: Dictionnary containing the masks to select datapoints with one of the rare events datasets, or others or both.
    """
    masks = {}
    dset_rare_evts = [dataset for dataset in evt_thres if dataset in opt['dataset_list']]
    for dataset in dset_rare_evts:
        masks[dataset] = sel_dataset_evts(dataset, opt['dataset_list'], evt_thres, X_data)

    for count, dset in enumerate(dset_rare_evts):
        if count == 0:
            masks['others'] = ~np.array(masks[dset])
            masks['both'] = np.array(masks[dset])
        else:
            masks['others'] *= ~masks[dset]
            masks['both'] *= masks[dset]

    for dataset in dset_rare_evts:
        masks[dataset] *= ~masks['both']
    return masks

def get_preds_Xys(opt, evt_thres, dic_pred, dic_Xy):
    """Get the preds and Xys of the rare events datasets and both and others.

    Args:
        opt: Option arguments of the script. (dict)
        evt_thres: Dictonnary of threshold to define if pixel contains event of rare datasets or not. (dict)
        dic_pred: Dictionnary of predictions and scores. (dict)
        dic_Xy: Dictionnary X and y. (dict)
    Returns:
        dic_pred: Dictionnary of predictions and scores for each of the rare events datasets and both and others. (dict)
        dic_Xy: Dictionnary X and y for each of the rare events datasets and both and others. (dict)
    """
    dset_rare_evts = [dataset for dataset in evt_thres if dataset in opt['dataset_list']]
    train_evt_sel = {}
    test_evt_sel = {}
    dic_preds = dic_pred
    dic_Xys = {}

    for count, dataset in enumerate(dset_rare_evts):
        train_evt_sel[dataset] = sel_dataset_evts(dataset, opt['dataset_list'], evt_thres, dic_Xy['X_train'])
        test_evt_sel[dataset] = sel_dataset_evts(dataset, opt['dataset_list'], evt_thres, dic_Xy['X_test'])

        if count == 0:
            train_evt_sel['others'] = ~np.array(train_evt_sel[dataset])
            test_evt_sel['others'] = ~np.array(test_evt_sel[dataset])

            train_evt_sel['both'] = np.array(train_evt_sel[dataset])
            test_evt_sel['both'] = np.array(test_evt_sel[dataset])
        else:
            train_evt_sel['others'] *= ~train_evt_sel[dataset]
            test_evt_sel['others'] *= ~test_evt_sel[dataset]

            train_evt_sel['both'] *= train_evt_sel[dataset]
            test_evt_sel['both'] *= test_evt_sel[dataset]

    for dataset in dset_rare_evts:
        print(np.any(test_evt_sel[dataset]*~test_evt_sel['both']))
        dic_preds[dataset], dic_Xys[dataset] = dataset_preds_Xys(opt, test_evt_sel[dataset]*~test_evt_sel['both'], train_evt_sel[dataset]*~train_evt_sel['both'], dic_pred, dic_Xy)

    for dataset in ['others', 'both']:
        dic_preds[dataset], dic_Xys[dataset] = dataset_preds_Xys(opt, test_evt_sel[dataset], train_evt_sel[dataset], dic_pred, dic_Xy)

    if not dset_rare_evts: # if empty
        dic_preds['others'] = dic_pred
        dic_Xys['others'] = dic_Xy

    return dic_preds, dic_Xys
