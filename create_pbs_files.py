# -*- coding: utf-8 -*-
# pylint: disable=C0103,E0401
"""
Write qsub skymaps files launch the job and erase the created file.

Usage:
  create_pbs_files [--help]
  create_pbs_files [--write_files=<num> --file_suffix=<suffix> --n_Hb_bkg_inj=<n> --n_ANT_bkg_inj=<n>
                  --n_src_per_month=<n> --n_comb_inj=<n_inj> --sig_in_train
                  --train_prop=<prop> --begin=<date> --end=<date>
                  --timestep=<step> --nside=<nside> --clf_name=<clf>
                  --job_events_df --job_skymaps --n_jobs=<n> --job_X
                  --job_clf --job_predictions --visualize --all_datasets <datasets>...]

Options:
  -h --help                 Show this screen.
  --write_files=[0|1|2]     If '0' don't write don't load, '1' write if files don't exist else load, '2' always write,
                                even if files already exist. [default: 1]
  --file_suffix=<suffix>    Suffix to add to the filenames when saving or loading. [default: ]
  --n_Hb_bkg_inj=<n>        Number of HAWC_bursts background events injected in the training set. [default: 0]
  --n_ANT_bkg_inj=<n>       Number of ANTARES background events injected in the training set. [default: 0]
  --n_src_per_month=<n>     Number of signal sources to inject per month. [default: 30]
  --n_comb_inj=<n_inj>      Number of datasets to combine for signal injection, ie we inject signal in n_comb_inj
                                datasets for each source. If 'all' signal will be injected for all possible combinations
                                of datasets. [default: 3]
  --sig_in_train            Inject signal in the trainning set.
  --train_prop=<prop>       Proportion of the dataset used for trainning. [default: 0.5]
  --begin=<date>            Beginning of the dataset to use. [default: 2019-12-01T00:00:00]
  --end=<date>              End of the dataset to use. [default: 2021-02-28T00:00:00]
  --timestep=<step>         Timestep (in days) to use to produce input data. [default: 1]
  --nside=<nside>           Healpy skymap nside parameter, determines the resolution of the skymap. [default: 4]
  --clf_name=<clf>          Name of the pyod algorithm to use. [default: KNN]
  --job_events_df           Get df_event and y.
  --job_skymaps             Get skymaps. Also set n_jobs and job options to use more than one job.
  --n_jobs=<n>              Total number of jobs run for skymaps. Max number of jobs I can run at once is 100. [default: 1]
  --job_X                   Combine skymaps_job and add X to dic_Xy. Should set n_jobs too.
  --job_clf                 Fit clf.
  --job_predictions         Get predictions.
  --visualize               Visualize results.
  --all_datasets            Use all datasets: Fermi, ANTARES, HAWC_hotspots, IceCube, HAWC_bursts
  <datasets>                List of datasets to combine [default: ANTARES HAWC_hotspots HAWC_bursts]
"""

import os
import sys
import math
from docopt import docopt
from io_files import output_dir, get_opt_from_args, write_pbs_file, N_PRED_JOBS
from simple_functions import JOB_PATH

args = docopt(__doc__)
opt = get_opt_from_args(args, verbose=True)

if '--clf_name' in sys.argv:
    classifiers = [opt['clf_name']]
else:
    # classifiers = ['ABOD', #'ABOD_20neig', # 'CBLOF', 'FeatureBagging', 'HBOS',
    #                'HBOS_30bins', 'IForest', 'IForest_500est',#'IForest_40est',
    #                'KNN_100neig', 'KNN_20neig', #'Average_KNN', #'Average_KNN_20neig', # 'Median_KNN',
    #                'LOF', 'LOF_100neig', # 'LOCI', 'MCD',
    #                'OCSVM', 'PCA', #'PCA_mle', # 'SOS',
    #                'AutoEncoder', 'AutoEncoder_64_4', 'AutoEncoder_128_7', 'AutoEncoder_sig_32_7',# 'LSCP',
    #                # 'COF', 'SOD',
    #                # 'SO-GAAL', # 'MO-GAAL',
    #                #'MO-GAAL_k2', 'MO-GAAL_k3',
    #                'MO-GAAL_k3_ep10',]
    classifiers = ['KNN_20neig',
                   'PCA',
                   'MO-GAAL_k10_ep1', 'MO-GAAL_k3_ep1', #'MO-GAAL_k5_ep1',
                   # 'SO-GAAL', 'SO-GAAL_ep5', 'SO-GAAL_ep10',
                   'AutoEncoder',]

job_dir = os.path.join(JOB_PATH, output_dir(opt))
if not os.path.exists(job_dir):
    print("mkdir", job_dir)
    os.mkdir(job_dir)
os.chdir(job_dir)

write_pbs_file('events_df', opt, 2)

qsub_list = ""
jobs_per_qsub = 10
for qsub_job in range(math.ceil(opt['n_jobs']/jobs_per_qsub)):
    job_ids_list = [str(qsub_job*jobs_per_qsub+i) for i in range(jobs_per_qsub) if qsub_job*jobs_per_qsub+i < opt['n_jobs']]
    job_ids = ' '.join(job_ids_list)
    write_pbs_file('skymaps', opt, 48, ppn=10, mem=200, filename_ext='{0:02d}'.format(qsub_job), extra_opt="--job_id={}", parallel_args=job_ids)
    qsub_list += "qsub job_{0:02d}_skymaps.pbs\n".format(qsub_job)
f = open('skymaps_jobs_launcher.sh', 'w')
f.write(qsub_list)
f.close()

write_pbs_file('X', opt, 4, mem=80)

# qsub_list = ""
# for classifier in classifiers:
#     gpu = 'GAAL' in classifier or 'AutoEncoder' in classifier
#     write_pbs_file('clf', opt, 48, mem=40, filename_ext=classifier+'_pred', extra_opt="--job_predictions --visualize --clf_name={clf_name}".format(clf_name=classifier), gpu=gpu)
#     qsub_list += "qsub job_{clf_name}_pred_clf.pbs\n".format(clf_name=classifier)
# f = open('clf_pred_jobs_launcher.sh', 'w')
# f.write(qsub_list)
# f.close()

qsub_list = ""
for classifier in classifiers:
    gpu = 'GAAL' in classifier or 'AutoEncoder' in classifier
    write_pbs_file('clf', opt, 48, mem=50, filename_ext=classifier, extra_opt="--clf_name={clf_name}".format(clf_name=classifier), gpu=gpu)
    qsub_list += "qsub job_{clf_name}_clf.pbs\n".format(clf_name=classifier)
f = open('clf_jobs_launcher.sh', 'w')
f.write(qsub_list)
f.close()

qsub_list = ""
for classifier in classifiers:
    gpu = 'GAAL' in classifier or 'AutoEncoder' in classifier
    for i in range(N_PRED_JOBS):
        write_pbs_file('predictions', opt, 36, ppn=1, mem=20, filename_ext='{job_id}_{classifier}'.format(job_id=i, classifier=classifier),
                       extra_opt="--job_id={job_id} --clf_name={clf_name}".format(job_id=i, clf_name=classifier), gpu=gpu, parallel_args=job_ids)
        qsub_list += "qsub job_{job_id}_{clf_name}_predictions.pbs\n".format(job_id=i, clf_name=classifier)
f = open('predictions_jobs_launcher.sh', 'w')
f.write(qsub_list)
f.close()

qsub_list = ""
for classifier in classifiers:
    write_pbs_file('visualize', opt, 24, mem=100, filename_ext=classifier, extra_opt="--clf_name={clf_name}".format(clf_name=classifier))
    qsub_list += "qsub job_{clf_name}_visualize.pbs\n".format(clf_name=classifier)
f = open('visualize_jobs_launcher.sh', 'w')
f.write(qsub_list)
f.close()

print("""\n\nCreated pbs files for:
job_events_df.pbs
job_skymaps_<job_id>.pbs for {n_jobs} jobs
job_X.pbs
job_clf_<classif>.pbs for {classifiers}
job_<job_id>_<classif>_predictions.pbs
""".format(n_jobs=opt['n_jobs'], classifiers=classifiers))

job_launcher1 = """#!/bin/bash
events_df=$(qsub job_events_df.pbs)
echo $events_df"""

skymap_ids = ""
for qsub_job in range(math.ceil(opt['n_jobs']/jobs_per_qsub)):
    job_launcher1 += """
skymaps{qsub_job}=$(qsub -W depend=afterok:$events_df job_{qsub_job}_skymaps.pbs)
echo $skymaps{qsub_job}""".format(qsub_job='{0:02d}'.format(qsub_job))
    skymap_ids += '$skymaps{0:02d}:'.format(qsub_job)
skymap_ids = skymap_ids[:-1]

# TODO test with more than 10 skymaps parallel jobs so that max job time is shorter (regression vers la moyenne en melangeant)
    # TODO ou plutot tester bien plus de jobs (double ou quadruple) puisque je peux en soumettre bien plus à la fois
job_launcher2 = """
jobX=$(qsub -W depend=afterok:{skymap_ids} job_X.pbs)
echo $jobX
model='KNN_20neig'
clf=$(qsub -W depend=afterok:$jobX job_${{model}}_clf.pbs)
echo $clf
pred0=$(qsub -W depend=afterok:$clf job_0_${{model}}_predictions.pbs)
echo $pred0
pred1=$(qsub -W depend=afterok:$clf job_1_${{model}}_predictions.pbs)
echo $pred1
pred2=$(qsub -W depend=afterok:$clf job_2_${{model}}_predictions.pbs)
echo $pred2
pred3=$(qsub -W depend=afterok:$clf job_3_${{model}}_predictions.pbs)
echo $pred3
pred4=$(qsub -W depend=afterok:$clf job_4_${{model}}_predictions.pbs)
echo $pred4
pred5=$(qsub -W depend=afterok:$clf job_5_${{model}}_predictions.pbs)
echo $pred5
pred6=$(qsub -W depend=afterok:$clf job_6_${{model}}_predictions.pbs)
echo $pred6
pred7=$(qsub -W depend=afterok:$clf job_7_${{model}}_predictions.pbs)
echo $pred7
pred8=$(qsub -W depend=afterok:$clf job_8_${{model}}_predictions.pbs)
echo $pred8
pred9=$(qsub -W depend=afterok:$clf job_9_${{model}}_predictions.pbs)
echo $pred9
visualize=$(qsub -W depend=afterok:$pred0:$pred1:$pred2:$pred3:$pred4:$pred5:$pred6:$pred7:$pred8:$pred9 job_${{model}}_visualize.pbs)
echo $visualize
""".format(skymap_ids=skymap_ids)

job_launcher = job_launcher1 + job_launcher2

f = open('job_launcher.sh', 'w')
f.write(job_launcher)
f.close()
