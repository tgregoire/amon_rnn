# -*- coding: utf-8 -*-
# pylint: disable=C0103,E0401
"""
Write qsub skymaps files launch the job and erase the created file.

Usage:
  submit_skymaps_jobs [--help]
  submit_skymaps_jobs [--write_files=<num> --file_suffix=<suffix>
                  --n_src_per_month=<n> --n_comb_inj=<n_inj> --sig_in_train
                  --train_prop=<prop> --begin=<date> --end=<date>
                  --timestep=<step> --nside=<nside> --clf_name=<clf>
                  --job_events_df --job_skymaps --n_jobs=<n> --job_id=<id>
                  --job_X --job_clf --job_predictions --all_datasets <datasets>...]

Options:
  -h --help                 Show this screen.
  --write_files=[0|1|2]     If '0' don't write don't load, '1' write if files don't exist else load, '2' always write,
                                even if files already exist. [default: 1]
  --file_suffix=<suffix>    Suffix to add to the filenames when saving or loading. [default: ]
  --n_src_per_month=<n>     Number of signal sources to inject per month. [default: 30]
  --n_comb_inj=<n_inj>      Number of datasets to combine for signal injection, ie we inject signal in n_comb_inj
                                datasets for each source. If 'all' signal will be injected for all possible combinations
                                of datasets. [default: 3]
  --sig_in_train            Inject signal in the trainning set.
  --train_prop=<prop>       Proportion of the dataset used for trainning. [default: 0.5]
  --begin=<date>            Beginning of the dataset to use. [default: 2019-12-01T00:00:00]
  --end=<date>              End of the dataset to use. [default: 2020-02-29T00:00:00]
  --timestep=<step>         Timestep (in days) to use to produce input data. [default: 1]
  --nside=<nside>           Healpy skymap nside parameter, determines the resolution of the skymap. [default: 4]
  --clf_name=<clf>          Name of the pyod algorithm to use. [default: KNN]
  --job_events_df           Get df_event and y.
  --job_skymaps             Get skymaps. Also set n_jobs and job options to use more than one job.
  --n_jobs=<n>              Total number of jobs run for skymaps. [default: 1]
  --job_id=<id>             Job number. [default: 0]
  --job_X                   Combine skymaps_job and add X to dic_Xy. Should set n_jobs too.
  --job_clf                 Fit clf.
  --job_predictions         Get predictions.
  --all_datasets            Use all datasets: Fermi, ANTARES, HAWC_hotspots, IceCube, HAWC_bursts
  <datasets>                List of datasets to combine [default: ANTARES HAWC_hotspots HAWC_bursts]
"""

import os
from docopt import docopt
import pandas as pd
from io_files import output_dir
from pprint import pprint
OUTPUT_PATH = '/storage/home/tmg5746/Desktop/OutlierDetection/data'

def get_opt_jobs_from_args(args, verbose=False): # TODO modify this and use it
    """Make a dictionnary of options to run the script from the dictionnary of arguments, ie get the correct types and key names.

    Args:
        args: Dictionnary of arguments given to the script (docopt output). (dict)
    Returns:
        opt: Dictionnary of options. (dict)
    """
    opt = dict((key.replace('--', ''), value) for (key, value) in args.items()) # Remove '--' from arguments keys
    del opt['help']
    opt['dataset_list'] = opt.pop('<datasets>')
    if opt['all_datasets']:
        opt['dataset_list'] = ['ANTARES', 'Fermi', 'HAWC_bursts', 'HAWC_hotspots', 'IceCube']
    if not opt['dataset_list']:
        opt['dataset_list'] = ['ANTARES', 'HAWC_hotspots', 'HAWC_bursts']
    if opt['write_files'] not in ['0', '1', '2']:
        raise ValueError("write_files option should be in [0, 1, 2]; got {}".format(opt['write_files']))
    opt['write_files'] = int(opt['write_files'])
    opt['n_src_per_month'] = int(opt['n_src_per_month'])
    if opt['n_comb_inj'] != 'all':
        opt['n_comb_inj'] = int(opt['n_comb_inj'])
    opt['train_prop'] = float(opt['train_prop'])
    opt['begin'] = pd.Timestamp(opt['begin'])
    opt['end'] = pd.Timestamp(opt['end'])
    if float(opt['timestep']) <= 0:
        raise ValueError("Timestep should be a float > 0; got {}".format(float(opt['timestep'])))
    opt['timestep'] = pd.Timedelta(float(opt['timestep']), unit='d')
    opt['nside'] = int(opt['nside'])
    opt['out_path'] = os.path.join(OUTPUT_PATH, output_dir(opt))
    opt['n_jobs'] = int(opt['n_jobs'])
    opt['job_id'] = int(opt['job_id'])

    job_types = ['job_events_df', 'job_skymaps', 'job_X', 'job_clf', 'job_predictions']
    if all([not opt[job_type] for job_type in job_types]):
        for job_type in job_types:
            opt[job_type] = True

    if verbose:
        pprint(opt)
    return opt

args = docopt(__doc__)
opt = get_opt_jobs_from_args(args, verbose=True)
os.chdir('/storage/home/tmg5746/Desktop/jobs')

print(args)
print(opt)

n_jobs = 20
nside = 128
timestep_in_days = 1 # 1
for step in range(n_jobs):
    filename = 'job_skymaps_{step}.pbs'.format(step=step)
    f = open(filename, 'w')

    file_content = """
#!/bin/bash

#PBS -l nodes=1:ppn=1

#PBS -l walltime=12:00:00

#PBS -A open

# Get started

echo "Job started on $(hostname) at $(date)"

# Go to the correct place

cd /storage/home/tmg5746/Desktop/OutlierDetection/

# Run the job itself

/storage/home/tmg5746/miniconda3/bin/python /storage/home/tmg5746/Desktop/OutlierDetection/PyOD_playground.py --write_files=2 --nside={nside} --timestep={timestep} --all_datasets --job_skymaps --n_jobs={n_jobs} --job_id={step}

# Finish up

echo "Job Ended at $(date)"
""".format(nside=nside, timestep=timestep_in_days, n_jobs=n_jobs, step=step)

    print(file_content)

    f.write(file_content)
    f.close()

    command = 'qsub '+filename
    print(command)
    os.system(command)
    print('rm '+filename)
    os.system('rm '+filename)
