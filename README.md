Note: do not install docopt with pip (it does not work), just copy docopt.py from https://github.com/docopt/docopt/blob/master/docopt.py

`ssh tmg5746@submit.aci.ics.psu.edu`

## Run
<!-- `python create_pbs_files.py --n_jobs=130 --nside=128 --timestep=0.25 --train_prop=0.005 --n_comb_inj=3 --end=2020-07-29T00:00:00 --clf_name=KNN_20neig --file_suffix= Fermi HAWC_hotspots IceCube` -->
`python create_pbs_files.py --n_jobs=95 --nside=128 --timestep=0.25 --train_prop=0.005 --n_comb_inj=3 --n_Hb_bkg_inj=1000 --all_datasets`        Create the corresponding directory in `/storage/home/tmg5746/Desktop/jobs/` with all the pbs files
Go there to submit jobs, one after the other finished:\
`qsub job_events_df.pbs`\
`bash skymaps_jobs_launcher.sh`\
`qsub job_X.pbs`\
`qsub job_<clf>_clf.pbs`          or `bash clf_jobs_launcher.sh` to run all classifier models\
`qsub job_<clf>_predictions.pbs`  or `bash predictions_jobs_launcher.sh` for the same reason\
Prediction will save the plots as png but if you need to re-plot without computing the predictions again, you can use:\
`qsub job_<clf>_visualize.pbs`    or `bash visualize_jobs_launcher.sh` for the same reason

## Submiting and other commands
`qsub file.pbs`\
`qsub -A open -l walltime=1:00:00 -l nodes=1:ppn=2 -l feature=rhel7 -I`  Interactiv job\
`qstat -u tmg5746`\
`qselect -u tmg5746 | xargs qdel`       Delete all my jobs

## Change clf models
Add, modify models in PyOD_playground.py in the `classifiers` dictionnary then add the new key in the `classifiers` list in create_pbs_files.py.

## Get the plots locally
Locally:\
`cd /home/timothee/code/OutlierDetection/data`\
Create locally the directory corresponding to the data you want to visualize, e.g. `mkdir 90d_per…; cd 90d_per…`\
`sftp tmg5746@aci-b.aci.ics.psu.edu`\
`cd Desktop/OutlierDetection/data/90d_per…`\
`get *.png`

Eventually from `OutlierDetection`\
`cd Desktop/OutlierDetection/`\
`get data/90d_per…/*.png data/90d_per…/`

## Unittesting
(Locally after `conda activate`)\
`python -m unittest test_data_prep.py`     To test data_prep.py using test_data_prep.py script\
`python -m unittest discover`              To test whatever can be tested in this folder\
`python -m unittest test_data_prep.TestEventDensity`   To test only TestEventDensity if test_data_prep\
`coverage run -m unittest test_data_prep.py; coverage report -m`    To evaluate the coverage of your testing

## Input data
The input data are stored in pkl files containing pandas tables.
I used get_dataset.py on the AMON dev AWS server to produce these files.
