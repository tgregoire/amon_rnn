# -*- coding: utf-8 -*-
# pylint: disable=C0103,E0401
"""
Search for coincident signal from several datasets using outlier detection methods.

Usage:
  PyOD_playground [--help]
  PyOD_playground [--write_files=<num> --file_suffix=<suffix> --n_Hb_bkg_inj=<n> --n_ANT_bkg_inj=<n>
                  --n_src_per_month=<n> --n_comb_inj=<n_inj> --sig_in_train
                  --train_prop=<prop> --begin=<date> --end=<date>
                  --timestep=<step> --nside=<nside> --clf_name=<clf>
                  --job_events_df --job_skymaps --n_jobs=<n> --job_id=<id>
                  --job_X --job_clf --job_predictions --visualize --all_datasets <datasets>...]

Options:
  -h --help                 Show this screen.
  --write_files=[0|1|2]     If '0' don't write don't load, '1' write if files don't exist else load, '2' always write,
                                even if files already exist. [default: 1]
  --file_suffix=<suffix>    Suffix to add to the filenames when saving or loading. [default: ]
  --n_Hb_bkg_inj=<n>        Number of HAWC_bursts background events injected in the training set. [default: 0]
  --n_ANT_bkg_inj=<n>       Number of ANTARES background events injected in the training set. [default: 0]
  --n_src_per_month=<n>     Number of signal sources to inject per month. [default: 30]
  --n_comb_inj=<n_inj>      Number of datasets to combine for signal injection, ie we inject signal in n_comb_inj
                                datasets for each source. If 'all' signal will be injected for all possible combinations
                                of datasets. It can also be a list of datasets separated by ','
                                for example: --n_comb_inj=Fermi,HAWC_hotspots,IceCube [default: 3]
  --sig_in_train            Inject signal in the trainning set.
  --train_prop=<prop>       Proportion of the dataset used for trainning. [default: 0.5]
  --begin=<date>            Beginning of the dataset to use. [default: 2019-12-01T00:00:00]
  --end=<date>              End of the dataset to use. [default: 2021-02-28T00:00:00]
  --timestep=<step>         Timestep (in days) to use to produce input data. [default: 1]
  --nside=<nside>           Healpy skymap nside parameter, determines the resolution of the skymap. [default: 4]
  --clf_name=<clf>          Name of the pyod algorithm to use. [default: KNN]
  --job_events_df           Get df_event and y.
  --job_skymaps             Get skymaps. Also set n_jobs and job options to use more than one job.
  --n_jobs=<n>              Total number of jobs run for skymaps. [default: 1]
  --job_id=<id>             Job number. [default: 0]
  --job_X                   Combine skymaps_job and add X to dic_Xy. Should set n_jobs too.
  --job_clf                 Fit clf.
  --job_predictions         Get predictions.
  --visualize               Visualize results.
  --all_datasets            Use all datasets: Fermi, ANTARES, HAWC_hotspots, IceCube, HAWC_bursts
  <datasets>                List of datasets to combine [default: ANTARES HAWC_hotspots HAWC_bursts]
"""
from __future__ import division
from __future__ import print_function
from math import ceil
# import pdb
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
# from numba import jit
from pyod.models.abod import ABOD
from pyod.models.cblof import CBLOF
from pyod.models.feature_bagging import FeatureBagging
from pyod.models.hbos import HBOS
from pyod.models.iforest import IForest
from pyod.models.knn import KNN
from pyod.models.lof import LOF
# from pyod.models.loci import LOCI
from pyod.models.mcd import MCD
from pyod.models.ocsvm import OCSVM
from pyod.models.pca import PCA
from pyod.models.sos import SOS
# from pyod.models.lscp import LSCP
from pyod.models.cof import COF
from pyod.models.sod import SOD
from pyod.models.auto_encoder import AutoEncoder
from pyod.models.so_gaal import SO_GAAL
from pyod.models.mo_gaal import MO_GAAL
from data_prep import * # pylint: disable=W0614,W0401
from simple_functions import * # pylint: disable=W0614,W0401
from visualize import * # pylint: disable=W0614,W0401
from io_files import * # pylint: disable=W0614,W0401
from docopt import docopt
# from pyod.models.combination import aom, moa, average, maximization
# from pyod.utils.utility import standardizer # it does z = (x-u)/s with u the mean and s the standard deviation

# %%
# TODO the time selection of Hawc events can be problematic for the transition between time steps, as they are not independent
# TODO eventuellement si je pouvais prendre en compte energie des IC evts...
# TODO I inject signal in one time window, but signal injection could be more subtle (not easy)
# TODO j'l'ai ptêt d'jà écrit quequpart: je vais devoir run l'analyse plusieurs fois en faisant glisser time windows
# TODO at some point test if inj sig events then scrambling we still see signal (which would mean injection is not correcly done)

# %%
if type_of_script() != 'jupyter':
    args = docopt(__doc__)
    opt = get_opt_from_args(args, verbose=True)
else:
    existing_output_dirs(verbose=False)
    opt = {'write_files': 2, # 0 don't write don't load, 1 write if they don't exist else load, 2 always write, even if already exist
           'file_suffix': '',
           'n_Hb_bkg_inj': 1000,
           'n_ANT_bkg_inj': 0,
           'n_src_per_month': 30*10,
           'n_comb_inj': 3,#['Fermi', 'HAWC_hotspots', 'IceCube'], #3, # int or 'all' or list of datasets ['HAWC_bursts', 'ANTARES', 'IceCube']
           'sig_in_train': False,
           'train_prop': 0.3,
           'begin': pd.Timestamp('2019-12-01 00:00:00'),
           # 'end': pd.Timestamp('2020-01-29 00:00:00'),
           # 'end': pd.Timestamp('2021-02-28 00:00:00'),
           'end': pd.Timestamp('2019-12-07 00:00:00'),
           'timestep': pd.Timedelta(1, unit='d'),
           'nside': 8,
           'clf_name': 'KNN',
           'job_events_df': True,
           'job_skymaps': True,
           'job_X': True,
           'job_clf': True,
           'job_predictions': True,
           'visualize': True,
           'n_jobs': 1,
           'job_id': 0,
           'dataset_list': ['ANTARES', 'Fermi', 'HAWC_bursts', 'HAWC_hotspots', 'IceCube'],
           # 'dataset_list': ['IceCube', 'HAWC_hotspots'],
           # 'dataset_list': ['Fermi', 'HAWC_hotspots', 'IceCube'],
           # 'dataset_list': ['ANTARES', 'HAWC_hotspots', 'IceCube'],
          }
    opt['end'] = get_timeline(opt)[-1]+opt['timestep'] # In case end is not the end of a timestep, we change it
    opt['out_path'] = os.path.join(OUTPUT_PATH, output_dir(opt))
    N_PRED_JOBS = 1 # TODO eventually do that to correct the problem of multiple jobs in interactiv mode but that is not a real correction, but ok for testing

resolution = np.degrees(hp.nside2resol(opt['nside'])) # pylint: disable=E1111
print("Resolution: {0:.3f} deg".format(resolution))
never_save = opt['write_files'] == 0
if never_save:
    opt['n_jobs'] = 1
    opt['job_id'] = 0
    opt['job_events_df'] = True
    opt['job_skymaps'] = True
    opt['job_X'] = True
    opt['job_clf'] = True
    opt['job_predictions'] = True
create_dir(opt, verbose=True)

Xy_path = os.path.join(opt['out_path'], 'dic_Xy.pkl')
df_events_path = os.path.join(opt['out_path'], 'dic_df_events.pkl')
df_events_nosig_path = os.path.join(opt['out_path'], 'dic_df_events_nosig.pkl')
skymaps_path = os.path.join(opt['out_path'], 'dic_skymaps.pkl')
skymaps_nosig_path = os.path.join(opt['out_path'], 'dic_skymaps_nosig.pkl')
clf_path = os.path.join(opt['out_path'], opt['clf_name']+'.pkl')
pred_path = os.path.join(opt['out_path'], 'predictions_'+opt['clf_name']+'.pkl')


# %% Signal injection
if opt['job_events_df']:
# TODO plot l'evolution temporelle d'un bin Fermi pour get le nb d'events à inj (quand on aura des gros fichiers avec ptit timestep)
    if not use_written_file(opt['write_files'], df_events_path) or not use_written_file(opt['write_files'], Xy_path): # Get y_train, y_test
        fermi_mask = mask_Fermi(opt['nside'], FERMI_MASK_WIDTH)

        if opt['n_src_per_month'] == 0:
            sig_dfs = get_empty_sig_dfs(opt['dataset_list'])
            print('0 source injected. Contamination: 0%')
            y_all = get_y(opt, src_injected=None)
            y_train, y_test = split_to_train_test(opt, y_all)

        else:
            begin_sig_inj = get_sig_inj_begin_time(opt)
            comb_list = get_comb_list(opt, begin_sig_inj, opt['end'])
            n_src = len(comb_list)
            n_sim_src = n_src * 50 # sim more src because some of them won't fall in the field of view of the several detectors
            rdm_src_df = get_random_source_df(n_sim_src, begin_sig_inj, opt['end'], opt['dataset_list'])
            sig_dfs, src_injected = injecting_events(opt, rdm_src_df, comb_list, opt['dataset_list'], fermi_mask, signal=True)
            for dataset in opt['dataset_list']:
                sig_dfs[dataset] = get_scaling_factor(dataset, sig_dfs[dataset])
            y_all = get_y(opt, src_injected)
            y_train, y_test = split_to_train_test(opt, y_all)

        bg_inj = {}
        if 'HAWC_bursts' in opt['dataset_list']:
            bg_inj['HAWC_bursts'] = opt['n_Hb_bkg_inj']
        if 'ANTARES' in opt['dataset_list']:
            bg_inj['ANTARES'] = opt['n_ANT_bkg_inj']
        bg_dfs = {}
        for key in bg_inj:
            if bg_inj[key] != 0:
                n_sim_bg = bg_inj[key] * 20 # sim more bg because some of them won't fall in the field of view of the detector
                n_training_steps = ceil(len(get_timeline(opt))*TRAIN_SPLIT)
                end_training = opt['begin'] + n_training_steps*opt['timestep']
                rdm_bg_df = get_random_source_df(n_sim_bg, opt['begin'], end_training, opt['dataset_list'])
                bg_list = [[opt['dataset_list'].index(key)]]*bg_inj[key]
                if not bg_dfs:
                    bg_dfs = injecting_events(opt, rdm_bg_df, bg_list, opt['dataset_list'], fermi_mask, signal=False)[0]
                else:
                    bg_dfs[key] = injecting_events(opt, rdm_bg_df, bg_list, opt['dataset_list'], fermi_mask, signal=False)[0][key]

        # Get df_events
        dic_df_events = {}
        dic_df_events_nosig = {}
        for dataset in opt['dataset_list']:
            dataframe = get_data(dataset, opt['begin'], opt['end'])
            dataframe = get_scaling_factor(dataset, dataframe)
            df_events = scramble_data(dataset, dataframe)
            dataframe = remove_useless_columns(dataset, df_events)
            if dataset in bg_dfs:
                df_events = df_events.append(bg_dfs[dataset], ignore_index=True)
            df_events_nosig = assign_ra_dec_rad(df_events)
            timeline = get_timeline(opt)
            split_index = math.ceil(len(timeline)*TRAIN_SPLIT)
            df_events_nosig = time_selection(dataset, df_events_nosig, {'start': timeline[split_index], 'stop': opt['end']})

            df_events = df_events.append(sig_dfs[dataset], ignore_index=True)
            df_events = assign_ra_dec_rad(df_events)
            print(dataset, "loaded and scrambled.")
            print(len(time_selection(dataset, df_events, {'start': opt['begin'], 'stop': opt['end']})), "events in dataset.")
            dic_df_events[dataset] = df_events
            dic_df_events_nosig[dataset] = df_events_nosig

        dic_Xy = {'y_train': y_train, 'y_test': y_test, 'comb_datasets': sig_dfs['comb_datasets']}
        # print(src_injected.sort_values('time'))
        print(len(dic_Xy['y_train']), 'data points in train')
        print(len(dic_Xy['y_test']), 'data points in test')
        if not never_save: # Save
            save_obj(dic_Xy, Xy_path)
            print("dic_Xy file written with y only.")
            save_obj(dic_df_events, df_events_path, verbose=True)
            save_obj(dic_df_events_nosig, df_events_nosig_path, verbose=True)


# %%
if opt['job_skymaps']:
    timeline = get_timeline(opt)
    n_steps = len(timeline)
    n_jobs_sig = math.ceil(opt['n_jobs']/(1+(1-TRAIN_SPLIT)))
    n_jobs = n_jobs_sig
    if opt['job_id'] < n_jobs:
        if 'dic_df_events' not in globals():
            dic_df_events = load_obj(df_events_path)
        opt_job = dict(opt)
        opt_job['begin'] = timeline[int(opt['job_id']*n_steps/n_jobs)]
        opt_job['end'] = timeline[int((opt['job_id']+1)*n_steps/n_jobs)-1]+opt['timestep']
        dic_skymaps_job = {'begin': opt_job['begin'], 'end': opt_job['end']}
        print("Skymaps between", opt_job['begin'], "and", opt_job['end'])
        for dataset in opt['dataset_list']:
            skymaps = get_skymaps_of_densities(opt_job, dataset, dic_df_events[dataset], verbose=False)
            print("Created skymaps of densities for "+dataset)
            dic_skymaps_job[dataset] = skymaps
        if not never_save:
            skymaps_path_job = os.path.join(opt['out_path'], 'dic_skymaps_{job}.pkl'.format(job='{0:02d}'.format(opt['job_id'])))
            save_obj(dic_skymaps_job, skymaps_path_job, verbose=True)

    if opt['job_id'] >= n_jobs_sig or (opt['n_jobs'] - n_jobs_sig <= 0 and opt['job_id'] == opt['n_jobs']-1):
        timeline = get_timeline(opt)
        split_index = math.ceil(len(timeline)*TRAIN_SPLIT)
        timeline = timeline[split_index:] # here timeline is only the test part
        n_steps = len(timeline)
        n_jobs = max(opt['n_jobs'] - n_jobs_sig, 1)
        if 'dic_df_events_nosig' not in globals():
            dic_df_events_nosig = load_obj(df_events_nosig_path)
        opt_job = dict(opt)
        opt_job['begin'] = timeline[int((opt['job_id']-n_jobs_sig)*n_steps/n_jobs)]
        opt_job['end'] = timeline[int((opt['job_id']-n_jobs_sig+1)*n_steps/n_jobs)-1]+opt['timestep']
        dic_skymaps_nosig_job = {'begin': opt_job['begin'], 'end': opt_job['end']}
        print("Skymaps with no injected signal between", opt_job['begin'], "and", opt_job['end'])
        for dataset in opt['dataset_list']:
            skymaps = get_skymaps_of_densities(opt_job, dataset, dic_df_events_nosig[dataset], verbose=False)
            print("Created skymaps of densities for "+dataset)
            dic_skymaps_nosig_job[dataset] = skymaps
        if not never_save:
            skymaps_nosig_path_job = os.path.join(opt['out_path'], 'dic_skymaps_nosig_{job}.pkl'.format(job='{0:02d}'.format(opt['job_id'])))
            save_obj(dic_skymaps_nosig_job, skymaps_nosig_path_job, verbose=True)

# %%
if opt['job_X']: # Combine skymaps_job files
    n_jobs_sig = math.ceil(opt['n_jobs']/(1+(1-TRAIN_SPLIT)))
    range_jobs_sig = range(n_jobs_sig)
    skymaps_exist = all([os.path.exists(os.path.join(opt['out_path'], 'dic_skymaps_{0:02d}.pkl'.format(job_id))) for job_id in range_jobs_sig])
    min_job_nosig = n_jobs_sig if n_jobs_sig < opt['n_jobs'] else n_jobs_sig-1
    range_jobs_nosig = range(min_job_nosig, opt['n_jobs'])
    skymaps_nosig_exist = all([os.path.exists(os.path.join(opt['out_path'], 'dic_skymaps_nosig_{0:02d}.pkl'.format(job_id))) for job_id in range_jobs_nosig])

    if 'dic_skymaps_job' in globals() and 'dic_skymaps_nosig_job' in globals() and opt['n_jobs'] == 1 and never_save:
        dic_skymaps = dic_skymaps_job
        dic_skymaps_nosig = dic_skymaps_nosig_job

    elif skymaps_exist and skymaps_nosig_exist:
        dic_skymaps = combine_skymap_jobs(opt, range_jobs_sig, is_nosig=False)
        save_obj(dic_skymaps, skymaps_path, verbose=True)
        for job in range_jobs_sig:
            os.remove(os.path.join(opt['out_path'], 'dic_skymaps_{0:02d}.pkl'.format(job)))

        dic_skymaps_nosig = combine_skymap_jobs(opt, range_jobs_nosig, is_nosig=True)
        save_obj(dic_skymaps_nosig, skymaps_nosig_path, verbose=True)
        for job in range_jobs_nosig:
            os.remove(os.path.join(opt['out_path'], 'dic_skymaps_nosig_{0:02d}.pkl'.format(job)))
    else:
        if not os.path.exists(skymaps_path):
            print("Problem skymap files missing?")
            print([os.path.join(opt['out_path'], 'dic_skymaps_{0:02d}.pkl'.format(job_id)) for job_id in range_jobs_sig])
            print([os.path.join(opt['out_path'], 'dic_skymaps_nosig_{0:02d}.pkl'.format(job_id)) for job_id in range_jobs_nosig])
            print([os.path.exists(os.path.join(opt['out_path'], 'dic_skymaps_{0:02d}.pkl'.format(job_id))) for job_id in range_jobs_sig])
            print([os.path.exists(os.path.join(opt['out_path'], 'dic_skymaps_nosig_{0:02d}.pkl'.format(job_id))) for job_id in range_jobs_nosig])
            raise FileNotFoundError("No such file or directory: {}".format(skymaps_path))
        dic_skymaps = load_obj(skymaps_path)
        dic_skymaps_nosig = load_obj(skymaps_nosig_path)

#%%
if opt['job_X']:
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)
        if opt['write_files'] == 2 and 'X_train' in dic_Xy.keys():
            raise ValueError("dic_Xy already contains X arrays while it should only contain y array because write_files is 2.")

    if 'X_train' not in dic_Xy.keys():
        timeline = get_timeline(opt)
        data, min_vals, max_vals = skymaps_to_data(opt['dataset_list'], dic_skymaps, timeline, rescale=True)
        X_train, X_test = split_to_train_test(opt, data)
        dic_Xy['X_train'] = X_train
        dic_Xy['X_test'] = X_test

        split_index = math.ceil(len(timeline)*TRAIN_SPLIT)
        timeline = timeline[split_index:] # here timeline is only the test part
        X_test_nosig, _, _ = skymaps_to_data(opt['dataset_list'], dic_skymaps_nosig, timeline, min_vals, max_vals, rescale=True)
        dic_Xy['X_test_nosig'] = X_test_nosig

        if not never_save:
            save_obj(dic_Xy, Xy_path)
            print("dic_Xy file written with X added.")

# %%
if opt['job_clf']:
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)
    contamination = get_contamination(opt)

    classifiers = {
        'ABOD': ABOD(contamination=contamination),
        'ABOD_20neig': ABOD(contamination=contamination, n_neighbors=20),
        'CBLOF': CBLOF(contamination=contamination, check_estimator=False, n_jobs=-1),
        'FeatureBagging': FeatureBagging(LOF(n_neighbors=35), contamination=contamination, n_jobs=-1),
        'HBOS': HBOS(contamination=contamination),
        'HBOS_30bins': HBOS(n_bins=30, contamination=contamination),
        'HBOS_50bins': HBOS(n_bins=50, contamination=contamination),
        'IForest': IForest(contamination=contamination, verbose=True, n_jobs=-1),
        'IForest_40est': IForest(n_estimators=40, contamination=contamination, verbose=True, n_jobs=-1),
        'IForest_500est': IForest(n_estimators=500, contamination=contamination, verbose=True, n_jobs=-1),
        'KNN': KNN(contamination=contamination, n_jobs=-1),
        'KNN_100neig': KNN(n_neighbors=100, contamination=contamination, n_jobs=-1),
        'KNN_20neig': KNN(n_neighbors=20, contamination=contamination, n_jobs=-1),
        'Average_KNN': KNN(method='mean', contamination=contamination, n_jobs=-1),
        'Average_KNN_20neig': KNN(n_neighbors=20, method='mean', contamination=contamination, n_jobs=-1),
        # 'Median_KNN': KNN(method='median', contamination=contamination),
        'LOF': LOF(n_neighbors=35, contamination=contamination, n_jobs=-1),
        'LOF_100neig': LOF(n_neighbors=100, contamination=contamination, n_jobs=-1),
        # 'LOCI': LOCI(contamination=contamination), # Implemented here: https://github.com/Cloudy10/loci "performance is very limited". It needs tens of TiB of memory when using nside=128
        'MCD': MCD(contamination=contamination),
        'OCSVM': OCSVM(contamination=contamination),
        'PCA': PCA(contamination=contamination),
        'PCA_mle': PCA(n_components='mle', contamination=contamination, svd_solver='full'),
        'SOS': SOS(contamination=contamination),
        'AutoEncoder': AutoEncoder(contamination=contamination, hidden_neurons=[64, len(dic_Xy['X_train'][0, :]), len(dic_Xy['X_train'][0, :]), 64], verbose=0),
        'AutoEncoder_64_4': AutoEncoder(contamination=contamination, hidden_neurons=[64, 4, 4, 64], verbose=0),
        'AutoEncoder_128_7': AutoEncoder(contamination=contamination, hidden_neurons=[128, len(dic_Xy['X_train'][0, :]), len(dic_Xy['X_train'][0, :]), 128], verbose=0),
        'AutoEncoder_sig_32_7': AutoEncoder(contamination=contamination, hidden_activation='sigmoid', hidden_neurons=[32, len(dic_Xy['X_train'][0, :]), len(dic_Xy['X_train'][0, :]), 32], verbose=0),
        # 'LSCP': LSCP(detector_list, contamination=contamination),
        'COF': COF(n_neighbors=35, contamination=contamination),
        'SOD': SOD(contamination=contamination),
        'SO-GAAL': SO_GAAL(contamination=contamination),
        'SO-GAAL_ep5': SO_GAAL(contamination=contamination, stop_epochs=5),
        'SO-GAAL_ep10': SO_GAAL(contamination=contamination, stop_epochs=10),
        'MO-GAAL': MO_GAAL(contamination=contamination),
        # 'MO-GAAL_k5': MO_GAAL(k=5, stop_epochs=10, contamination=contamination),
        # 'MO-GAAL_k3_ep10': MO_GAAL(k=3, stop_epochs=10, contamination=contamination),
        # 'MO-GAAL_k1_ep10': MO_GAAL(k=1, stop_epochs=10, contamination=contamination),
        # 'MO-GAAL_k5_ep2': MO_GAAL(k=5, stop_epochs=2, contamination=contamination),
        # 'MO-GAAL_k10_ep2': MO_GAAL(k=10, stop_epochs=2, contamination=contamination),
        'MO-GAAL_k10_ep1': MO_GAAL(k=10, stop_epochs=1, contamination=contamination),
        'MO-GAAL_k5_ep1': MO_GAAL(k=5, stop_epochs=1, contamination=contamination),
        'MO-GAAL_k3_ep1': MO_GAAL(k=3, stop_epochs=1, contamination=contamination),
    }
    if opt['clf_name'] not in classifiers.keys():
        raise ValueError('clf_name {clf_name} not recognised'.format(clf_name=opt['clf_name']))

    if not use_written_file(opt['write_files'], clf_path):
        clf = classifiers[opt['clf_name']]
        print(clf)
        print(dic_Xy['X_train'])
        clf.fit(dic_Xy['X_train'])
        if not never_save:
            save_clf(clf, opt['out_path'], opt['clf_name'], verbose=True)

# %%
if opt['job_predictions']:
    if 'clf' not in globals():
        clf = load_clf(opt['out_path'], opt['clf_name'])
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)
    if not use_written_file(opt['write_files'], pred_path):
        first_index, last_index = job_list_indices(opt['job_id'], N_PRED_JOBS, len(dic_Xy['X_test']))
        X_test_job = dic_Xy['X_test'][first_index:last_index]

        first_index, last_index = job_list_indices(opt['job_id'], N_PRED_JOBS, len(dic_Xy['X_test_nosig']))
        X_test_nosig_job = dic_Xy['X_test_nosig'][first_index:last_index]

        if 'GAAL' in opt['clf_name']: # NB Correct for a bug in pyod for MO-GAAL, SO-GAAL where 0 is outlier and 1 is inlier. https://github.com/yzhao062/pyod/issues/237
            dic_pred_job = {'y_test_scores': -np.log10(clf.decision_function(X_test_job).flatten()), # outlier scores
                            'y_test_nosig_scores': -np.log10(clf.decision_function(X_test_nosig_job).flatten()), # outlier scores
                           }
        else:
            dic_pred_job = {'y_test_pred': clf.predict(X_test_job), # outlier labels (0 or 1)
                            'y_test_scores': clf.decision_function(X_test_job).flatten(), # outlier scores
                            'y_test_nosig_pred': clf.predict(X_test_nosig_job), # outlier labels (0 or 1)
                            'y_test_nosig_scores': clf.decision_function(X_test_nosig_job).flatten(), # outlier scores
                           }
        if not never_save:
            pred_path_job = os.path.join(opt['out_path'], 'predictions_'+opt['clf_name']+'_{0:02d}.pkl'.format(opt['job_id']))
            save_obj(dic_pred_job, pred_path_job, verbose=True)


# %%
if opt['visualize']:
    if 'clf' not in globals():
        clf = load_clf(opt['out_path'], opt['clf_name'])
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)

    pred_job_paths = [os.path.join(opt['out_path'], 'predictions_'+opt['clf_name']+'_{0:02d}.pkl'.format(job_id)) for job_id in range(N_PRED_JOBS)]
    predictions_exist = all([os.path.exists(pred_job_path) for pred_job_path in pred_job_paths])

    if 'dic_pred_job' in globals() and opt['n_jobs'] == 1 and never_save:
        dic_pred = dic_pred_job
    elif predictions_exist:
        dic_pred_jobs = [load_obj(pred_job_path) for pred_job_path in pred_job_paths]
        dic_pred = concatenate_dic(dic_pred_jobs)

        if 'GAAL' in opt['clf_name']: # NB Correct for a bug in pyod for MO-GAAL, SO-GAAL where 0 is outlier and 1 is inlier. https://github.com/yzhao062/pyod/issues/237
            dic_pred['y_train_scores'] = -np.log10(clf.decision_scores_.flatten()) # raw outlier scores

            threshold = np.quantile(dic_pred['y_train_scores'], 1-get_contamination(opt))
            clf.threshold_ = threshold
            print("Threshold is", threshold)
            dic_pred['y_train_pred'] = dic_pred['y_train_scores'] > threshold
            dic_pred['y_test_pred'] = dic_pred['y_test_scores'] > threshold
            dic_pred['y_test_nosig_pred'] = dic_pred['y_test_nosig_scores'] > threshold

        else:
            dic_pred['y_train_pred'] = clf.labels_ # binary labels (0: inliers, 1: outliers)
            dic_pred['y_train_scores'] = clf.decision_scores_.flatten() # raw outlier scores

        save_obj(dic_pred, pred_path, verbose=True)

        for pred_job_path in pred_job_paths:
            os.remove(pred_job_path)

    else:
        if not os.path.exists(pred_path):
            raise OSError("Neither the pred_job files nor the final prediction one exist.\n"
                          "List of pred_job files:\n{pred_job_paths}\n"
                          "pred_job files existing:\n{pred_job_exist}\n"
                          "Final file do not exist either:\n{pred_path}"
                          .format(pred_job_paths=pred_job_paths,
                                  pred_job_exist=[os.path.exists(pred_job_path) for pred_job_path in pred_job_paths],
                                  pred_path=pred_path))
        dic_pred = load_obj(pred_path)

    # evaluate and print the results
    # print("On Training Data:")
    # evaluate_print(opt, dic_Xy, dic_pred, 'train')
    # print("On Test Data:")
    # evaluate_print(opt, dic_Xy, dic_pred, 'test') # TODO put back after testing, it is just a bit long

# %%
if opt['visualize']:
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)
    if 'dic_pred' not in globals():
        dic_pred = load_obj(pred_path)

    evt_thres = {'ANTARES':1/100, 'HAWC_bursts':1/100} # TODO optimize max(test_data)/1000: c'est une question difficile de trouver le bon threshold pour passer d'une distribution a une autre. A partir du moment ou les données d'antares commencent a avoir un impact sur la distribution du score alors il faut que ce soit dans la distrib avec antares event.
    dic_pred, dic_Xys = get_preds_Xys(opt, evt_thres, dic_pred, dic_Xy)

# %%
if opt['visualize']:
    distrib_outlier_score(opt, dic_Xy['y_test'], dic_pred['y_test_scores'], dic_pred['y_test_pred'], opt['clf_name']+'_distrib_score.png')
    distrib_outlier_score(opt, dic_Xy['y_test'], dic_pred['y_test_nosig_scores'], dic_pred['y_test_nosig_pred'], opt['clf_name']+'_distrib_nosig_score.png', data_type='nosig')
    distrib_outlier_score(opt, dic_Xy['y_train'], dic_pred['y_train_scores'], dic_pred['y_train_pred'], opt['clf_name']+'_distrib_score_training.png', data_type='train')

    dset_rare_evts = [dataset for dataset in evt_thres if dataset in opt['dataset_list']]
    for dataset in dset_rare_evts + ['others', 'both']:
        print(dataset)
        distrib_outlier_score(opt, dic_Xys[dataset]['y_test'], dic_pred[dataset]['y_test_scores'], dic_pred[dataset]['y_test_pred'], opt['clf_name']+'_distrib_score_'+dataset+'.png')
        distrib_outlier_score(opt, dic_Xys[dataset]['y_test'], dic_pred[dataset]['y_test_nosig_scores'], dic_pred[dataset]['y_test_nosig_pred'], opt['clf_name']+'_distrib_nosig_score_'+dataset+'.png', data_type='nosig')


# %%
if opt['visualize']:
    if 'clf' not in globals():
        clf = load_clf(opt['out_path'], opt['clf_name'])
    inj_ids = np.where(dic_Xy['y_test'])[0] + get_split_index(opt)#len(dic_Xy['y_train'])
    inj_ids = list(set(np.array(inj_ids/hp.nside2npix(opt['nside']), dtype='int')))
    time_ids = ids_to_plot(inj_ids, all=False)
    outlier_skymaps(opt, dic_Xy, dic_pred, key='y_test_scores', title='Test score', time_ids=time_ids, threshold=clf.threshold_)
    print('Test score skymaps plotted.')


# %% Get detections and plot FAR
if opt['visualize']:
    dic_pred['detections'] = get_detections(opt, dic_pred['y_test_scores'], dic_Xy['y_test'], clf.threshold_/4, verbose=False)
    dic_pred['detections_nosig'] = get_detections(opt, dic_pred['y_test_nosig_scores'], dic_Xy['y_test'], clf.threshold_/4, verbose=False)
    if not never_save: # Save
        save_obj(dic_pred, pred_path)
    print(dic_pred['detections']['y'])
    f_far = far_distrib(opt, dic_pred['detections_nosig']['all_scores'], dic_pred['detections']['score'], dic_pred['detections']['y'], clf.threshold_)
    # f_far = far_distrib(opt, dic_pred['detections_nosig']['all_scores'], dic_pred['detections_nosig']['score'], dic_pred['detections_nosig']['y'], clf.threshold_, filename_ext='nosig')


# %% Get detections and plot FAR
if opt['visualize']:
    masks = masks_for_FAR(opt, evt_thres, dic_Xy['X_test'])
    for dataset in dset_rare_evts + ['others', 'both']:
        print(dataset)
        dic_pred[dataset]['detections'] = get_detections_dataset(opt, dic_pred['detections'], masks[dataset])
        dic_pred[dataset]['detections_nosig'] = get_detections_dataset(opt, dic_pred['detections_nosig'], masks[dataset])

        if dic_pred[dataset]['y_train_scores'].size == 0:
            print("Warning: dic_pred['"+dataset+"']['y_train_scores'] is empty. Using 0.5 as threshold")
            threshold = 0.5
        else:
            threshold = np.quantile(dic_pred[dataset]['y_train_scores'], 1-get_contamination(opt))
        if not dic_pred[dataset]['detections_nosig']['all_scores']:
            print("Warning: dic_pred['"+dataset+"']['detections_nosig']['all_scores'] is empty. Continue.")
            continue
        # f_far = far_distrib(opt, dic_pred[dataset]['detections_nosig']['all_scores'], dic_pred[dataset]['detections_nosig']['score'], dic_pred[dataset]['detections_nosig']['y'], threshold, dataset+'_nosig')
        f_far = far_distrib(opt, dic_pred[dataset]['detections_nosig']['all_scores'], dic_pred[dataset]['detections']['score'], dic_pred[dataset]['detections']['y'], threshold, dataset)

    if not never_save: # Save
        save_obj(dic_pred, pred_path)


# %% Visualize training score maps
if opt['visualize']:
    n_steps = len(get_timeline(opt))
    time_ids = list(range(min(n_steps, 15)))
    # outlier_skymaps(opt, dic_Xy, dic_pred, key='y_train_scores', title='Train score', time_ids=time_ids, threshold=clf.threshold_)
    # outlier_skymaps(opt, dic_Xy, dic_pred, key='y_train_scores', title='Train score', time_ids=time_ids)  # Color scale starts at 0 to see unused bins
    # print('Training score skymaps plotted.')

    # Get map of 0 and 1 for pixels used in training
    y_training = np.zeros(get_split_index(opt))
    rng = np.random.default_rng(SEED) # pylint: disable=E1101
    n_train_points = len(dic_pred['y_train_scores'])
    indices_y = rng.choice(range(get_split_index(opt)), n_train_points, replace=False)
    y_training[indices_y] = 1
    dic_used_pix = {'y_train': y_training, 'comb_datasets': []}
    outlier_skymaps(opt, dic_used_pix, dic_used_pix, key='y_train', title='Pixels used for training', time_ids=time_ids)
    print('Training pixels skymaps plotted.')

# %% Plot skymaps
if opt['visualize'] or opt['job_X']:
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)
    if 'dic_skymaps' not in globals():
        dic_skymaps = load_obj(skymaps_path)
    npix = hp.nside2npix(opt['nside'])
    inj_ids = np.where(dic_Xy['y_test'])[0] + get_split_index(opt)
    inj_ids = list(set(np.array(inj_ids/npix, dtype='int')))
    time_ids = ids_to_plot(inj_ids, all=False)
    if not time_ids:
        time_ids = range(min(15, len(get_timeline(opt)))) # To get all time steps
    dataset_list = opt['dataset_list']

    first_time_id = int(get_split_index(opt)/npix)
    for dataset in dataset_list:
        max_dataset = dic_skymaps[dataset].max()[2:].max() # Used as max of color scale
        min_dataset = dic_skymaps[dataset].min()[2:].min() # Used as max of color scale
        for time_id in time_ids:
            tid_start = (time_id-first_time_id)*npix
            tid_end = (time_id-first_time_id+1)*npix
            dic_inj_comb = get_dic_inj_comb(dic_Xy, True, tid_start, tid_end)
            # print(dic_skymaps[dataset][time_id])
            if dataset == dataset_list[0]:
                print(dic_inj_comb)
            draw_skymap(opt, dic_skymaps[dataset][time_id], min_dataset, max_dataset, dataset, time_id, dic_inj_comb)
    print('Input data skymaps plotted.')

# %% Plot skymaps
if opt['visualize'] or opt['job_X']:
    if 'dic_Xy' not in globals():
        dic_Xy = load_obj(Xy_path)
    if 'dic_skymaps' not in globals():
        dic_skymaps = load_obj(skymaps_path)
    npix = hp.nside2npix(opt['nside'])
    time_ids = range(min(15, int(get_split_index(opt)/npix)))
    dataset_list = opt['dataset_list']

    first_time_id = int(get_split_index(opt)/npix)
    for dataset in dataset_list:
        max_dataset = dic_skymaps[dataset].max()[2:].max() # Used as max of color scale
        min_dataset = dic_skymaps[dataset].min()[2:].min() # Used as max of color scale
        for time_id in time_ids:
            tid_start = (time_id-first_time_id)*npix
            tid_end = (time_id-first_time_id+1)*npix
            dic_inj_comb = get_dic_inj_comb(dic_Xy, True, tid_start, tid_end)
            # print(dic_skymaps[dataset][time_id])
            if dataset == dataset_list[0]:
                print(dic_inj_comb)
            draw_skymap(opt, dic_skymaps[dataset][time_id], min_dataset, max_dataset, dataset+'_training', time_id)
    print('Input data skymaps plotted.')

#
# # %% Plot skymaps
# if opt['visualize'] or opt['job_X']:
#     if 'dic_Xy' not in globals():
#         dic_Xy = load_obj(Xy_path)
#     if 'dic_skymaps' not in globals():
#         dic_skymaps = load_obj(skymaps_path)
#     npix = hp.nside2npix(opt['nside'])
#     inj_ids = np.where(dic_Xy['y_test'])[0]# + get_split_index(opt)
#     inj_ids = list(set(np.array(inj_ids/npix, dtype='int')))
#     time_ids = ids_to_plot(inj_ids, all=False)
#     if not time_ids:
#         time_ids = range(min(15, len(get_timeline(opt)))) # To get all time steps
#     dataset_list = opt['dataset_list']
#
#     first_time_id = int(get_split_index(opt)/npix)
#     for dataset in dataset_list:
#         max_dataset = dic_skymaps[dataset].max()[2:].max() # Used as max of color scale
#         min_dataset = dic_skymaps[dataset].min()[2:].min() # Used as max of color scale
#         for time_id in time_ids:
#             tid_start = (time_id-first_time_id)*npix
#             tid_end = (time_id-first_time_id+1)*npix
#             dic_inj_comb = get_dic_inj_comb(dic_Xy, True, tid_start, tid_end)
#             # print(dic_skymaps[dataset][time_id])
#             if dataset == dataset_list[0]:
#                 print(dic_inj_comb)
#             draw_skymap(opt, dic_skymaps[dataset][time_id], 0, max_dataset, 'Simulated background', time_id, dic_inj_comb)
#     print('Input data skymaps plotted.')

# %%
if opt['visualize']:
    time_ids = ids_to_plot(inj_ids, all=False)
    for time_id in time_ids:
        images_names = [opt['clf_name']+'_skymapTest_score'+str(time_id)+'.png'] + ['skymap'+dataset+str(time_id)+'.png' for dataset in opt['dataset_list']]
        images = [Image.open(os.path.join(opt['out_path'], x)) for x in images_names]
        widths, heights = zip(*(i.size for i in images))

        total_width = sum(widths) if len(images_names) < 4 else max(widths)*3
        total_height = max(heights)*math.ceil(len(images_names)/3)

        new_im = Image.new('RGB', (total_width, total_height))

        x_offset = 0
        y_offset = 0
        for count, im in enumerate(images):
            new_im.paste(im, (x_offset, y_offset))
            x_offset += im.size[0]
            if count == 2 and len(images_names) > 3:
                y_offset = max(heights)
                x_offset = 0

        new_im.save(os.path.join(opt['out_path'], opt['clf_name']+'_combined_skymaps_{}.png'.format(time_id)))


# # %% Takes a lot of time
# if opt['visualize']:
#     # visualize the results
#     visualize_all_results(opt, clf, dic_Xy['X_train'], dic_Xy['y_train'], dic_pred['y_train_pred'],
#                           dic_Xy['X_test'], dic_Xy['y_test'], dic_pred['y_test_pred'])
#     print('2D visualization done.')

# %%
# %%
if type_of_script() != 'jupyter':
    exit()

# %% Plot skymaps
if opt['visualize']:
    # inj_ids = np.where(dic_Xy['y_test'])[0] + get_split_index(opt)
    # inj_ids = list(set(np.array(inj_ids/hp.nside2npix(opt['nside']), dtype='int')))
    # np.random.seed(SEED)
    # time_ids = sorted(np.random.choice(inj_ids, size=min(len(inj_ids), 15), replace=False))
    dic_skymaps = load_obj(skymaps_path)
    time_ids = range(len(get_timeline(opt))) # To get all time steps
    dataset_list = opt['dataset_list']
    for dataset in dataset_list:
        max_dataset = dic_skymaps[dataset].max()[2:].max() # Used as max of color scale
        min_dataset = dic_skymaps[dataset].min()[2:].min() # Used as max of color scale
        for time_id in time_ids:
            # print(dic_skymaps[dataset][time_id])
            draw_skymap(opt, dic_skymaps[dataset][time_id], min_dataset, max_dataset, dataset, time_id)

# %%
# print(sig_dfs)
y = np.concatenate((dic_Xy['y_train'], dic_Xy['y_test']))
print_info_coinc(y, (y > 0), timeline, opt['nside'])

# %% Plot time histogram
# dataset_list = opt['dataset_list']
dataset_list = ["HAWC_bursts"]
for dataset in dataset_list:
    print(dataset)
    plt.figure(figsize=(6 * 1.618, 6))
    plt.xticks(fontsize=5)
    plt.yticks(fontsize=5)
    dataframe = get_data(dataset, opt['begin'], opt['end'])

    dataframe['time'] = dataframe['time'].astype('datetime64')
    dataframe['time'].groupby(dataframe['time'].dt.date).count().plot(kind='bar')
    plt.show()

# %% plot scatter
dataset_list = opt['dataset_list']
# dataset_list = ['HAWC_hotspots']
for dataset in dataset_list:
    print(dataset)
    dataframe = get_data(dataset, opt['begin'], opt['end'])
    dataframe = equatorial_to_local(dataset, dataframe)
    dataframe.plot.scatter(x='src_err68', y='alt', s=0.1)
    plt.xlabel('src\\_err68')
    # plt.xlim(0, 10)
    plt.show()


# %% Same plot for train
plt.rcParams.update({'font.size': 22})
mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))
y_train_scores = dic_pred['y_train_scores']
y_train_pred = dic_pred['y_train_pred']
y_train_scores[y_train_scores == 0] = min(y_train_scores)/10. # NB 0 values are set to 1e-7 for plotting on log scale
binning = np.logspace(np.log10(min(y_train_scores)), np.log10(max(y_train_scores)), 100)
# binning = np.linspace(min(y_train_scores), max(y_train_scores), 100)
y_train_scores_stacked = [y_train_scores[(y_train == 1) * (y_train_pred == 1)],
                          y_train_scores[(y_train == 1) * (y_train_pred == 0)],
                          y_train_scores[(y_train == 0) * (y_train_pred == 1)],
                          y_train_scores[(y_train == 0) * (y_train_pred == 0)]]
colors = ['tab:blue', 'tab:cyan', 'tab:orange', 'tab:red']
labels = ['Out; Class out', 'Out; Class in', 'In; Class out', 'In; Class in']
plt.hist(y_train_scores_stacked, stacked=True, histtype='bar', lw=2, label=labels, color=colors, bins=100)
plt.legend()
plt.xlabel('Outlier Score')
# plt.xscale('log')
plt.yscale('log')
plt.ylim(0.5)
plt.show()

# %%
def pdf_test(distance, sig68):
    """Scaled probability density function to be 1 for distance=sig68. This function is more than 3 times faster than scaled_pdf_slow with the same result.

    Args:
        distance: distance from the event.
        sig68: Angular error of the event, corresponding to 68% containment.
    Returns:
        Scaled probability density function to be 1 for distance=sig68."""
    return 1/(sig68/1.515)*np.exp(-np.power(distance, 2)/(2*np.power(sig68/1.515, 2)))

# def rayleigh_pdf_test(distance, sig68):
#     """Scaled probability density function to be 1 for distance=sig68. This function is more than 3 times faster than scaled_pdf_slow with the same result.
#
#     Args:
#         distance: distance from the event.
#         sig68: Angular error of the event, corresponding to 68% containment.
#     Returns:
#         Scaled probability density function to be 1 for distance=sig68."""
#     return distance/np.power(sig68/1.515, 2)*np.exp(-np.power(distance, 2)/(2*np.power(sig68/1.515, 2)))

# %% Event density
plt.rcParams.update({'font.size': 22})
mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))
distances = np.linspace(0, 3, 100)
src_err68 = 0.1
event_densities1 = [f_event_density(distance, src_err68) for distance in distances]
src_err68 = 1
event_densities2 = [f_event_density(distance, src_err68) for distance in distances]
src_err68 = 2
event_densities3 = [f_event_density(distance, src_err68) for distance in distances]
# event_densities2 = [scaled_pdf(distance, src_err68) for distance in distances]
# event_densities3 = [pdf_test(distance, src_err68) for distance in distances]
# event_densities3 = [scaled_pdf(distance, src_err68)*2*np.pi*distance for distance in distances]
plt.plot(distances, event_densities1, label='src\_err68 0.1°')
plt.plot(distances, event_densities2, ls='--', label='src\_err68 1°')
plt.plot(distances, event_densities3, ls='--', label='src\_err68 2°')
# plt.plot(distances, event_densities3)
plt.xlabel(r'$\text{Distance [°]}$')
plt.ylabel(r'Event Density')
plt.legend()
plt.savefig(os.path.join(opt['out_path'], 'event_density.png'), bbox_inches='tight', facecolor='white')


# %% Interp signif
plt.rcParams.update({'font.size': 22})
mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))

for dataset in opt['dataset_list']:
    cdf = DIC_INTERP_SIGNIF['cdf'][dataset]
    bin_midpoints = DIC_INTERP_SIGNIF['bin_midpoints'][dataset]
    interp = DIC_INTERP_SIGNIF['to_scaling_factor'][dataset]
    x = np.linspace(bin_midpoints[0]-0.1, bin_midpoints[-1]+0.1, 1000)
    plt.plot(x, interp(x), label=dataset.replace('_', ' '))
    plt.xlabel('Event signalness/p-value/energy/...')
    plt.ylabel('Scaling factor')
    plt.legend()

plt.savefig(os.path.join(opt['out_path'], 'scaling_factors.png'), bbox_inches='tight', facecolor='white')
