# -*- coding: utf-8 -*-
# pylint: disable=C0103
"""
.

Usage:
  data_prep

Options:
  -h --help     Show this screen.
"""
import os
import math
import warnings
from itertools import combinations
import numpy as np
import healpy as hp
from astropy import units as u
from astropy.coordinates import SkyCoord, ICRS
import pandas as pd
warnings.filterwarnings('ignore') # put default to have them back
# NB I managed to reduce event_density's time from 8 ms to 3 ms!
# I should have used classes, I think, but that is a bit too late

# %%
DATA_PATH = os.path.dirname(os.path.realpath(__file__))+'/data/'
OUTPUT_PATH = os.path.dirname(os.path.realpath(__file__))+'/output/'
JOB_PATH = os.path.dirname(os.path.realpath(__file__))+'/jobs/'

TRAIN_SPLIT = 1/3 # We split the data in two, the training data points are drawn from first part, second part is for testing
SEED = 3210  # Used to randomly choose trainning data points

def pix2ang_eq(nside, ipix):
    """Convert pixel index to dec and ra angles with dec=90 corresponding to top of hp map and dec=-90 to bottom.

    Args:
        nside: Nside of the healpy skymap. (int)
        ipix: Pixel index of healpy skymap. (int or list)
    Returns:
        dec: Declination of the pixel ipix in radians. (float or list)
        ra: Right ascension in radians. (float or list)
    """
    dec_, ra = hp.pix2ang(nside=nside, ipix=ipix)
    dec = math.pi/2. - dec_
    return dec, ra

def ang2pix_eq(nside, dec, ra):
    """Convert pixel index to dec and ra angles with dec=90 corresponding to top of hp map and dec=-90 to bottom.
    After updating pandas etc I could not apply ang2pix to pd.Series so I convert to np.array and convert back to pd.Series after.

    Args:
        dec: Declination of the pixel ipix in radians. (float or list)
        ra: Right ascension of the pixel ipix in radians. (float or list)
        nside: Nside of the healpy skymap. (int)
    Returns:
        ipix: Pixel index of healpy skymap. (int or list)
    """
    if (isinstance(dec, int) and (dec > math.pi/2. or dec < -math.pi/2.)) or (isinstance(dec, list) and ((np.array(dec) > math.pi/2.).any() or (np.array(dec) < -math.pi/2.).any())):
        ValueError("First argument should be dec in [-pi/2, pi/2]")
    ipix = hp.ang2pix(nside, np.array(math.pi/2.-dec), np.array(ra))
    if isinstance(dec, pd.Series):
        ipix = pd.Series(data=ipix, index=dec.index)
    return ipix

def jd_to_mjd(julian_date):
    """
    Convert Julian Date to Modified Julian Date.

    Args:
        julian_date: Julian Date (Float)
    Returns:
        mjd: Modified Julian Date (Float)
    """
    return julian_date - 2400000.5

def great_circle_distance_slow(dataframe, position):
    """Compute the great circle distance between (ra, dec) and the data in dataframe.

    Args:
        dataframe: Pandas dataframe with at least 'ra' and 'dec' columns.
        position: Dictionary of the position in ('ra', 'dec') for which we want the event density. It should contain 'ra_rad' and 'dec_rad' in radians.
    Returns:
        distances: np.ndarray of the distances between (ra, dec) and the data.
    """
    if not isinstance(position['ra'], (float, int)) or not isinstance(position['dec'], (float, int)):
        raise TypeError("RA and Dec should be floats.")
    if position['ra'] > 360 or position['ra'] < 0 or position['dec'] > 90 or position['dec'] < -90:
        raise ValueError("RA should be in [0, 360] and Dec in [-90, 90].")
    if dataframe.empty:
        raise ValueError("Dataframe is empty.")
    search_coord = SkyCoord(ra=position['ra'] * u.deg, dec=position['dec'] * u.deg, frame=ICRS)
    if len(dataframe['ra']) >= 10000:
        raise ValueError("{n_evts} events is too many, SkyCoord cannot be more than 10000 long.".format(n_evts=len(dataframe['ra'])))
    data_coord = SkyCoord(ra=dataframe['ra'] * u.deg, dec=dataframe['dec'] * u.deg, frame=ICRS)
    return search_coord.separation(data_coord).degree

def rayleigh_pdf(r, sig): # pylint: disable=C0103
    """Probability density function of the Rayleigh distribution.

    Args:
        r: Variable, r>0.
        sig: Sigma (scale parameter) in the Rayleigh distribution definition, ie ~39% containment.
    Returns:
        Probability density function of the Rayleigh distribution.
    """
    return r/np.power(sig, 2)*np.exp(-np.power(r, 2)/(2*np.power(sig, 2)))

def x_pdf(distance, sig68):
    """Probability density function at location X based on a 2D gaussian distribution.

    It is the Rayleigh distribution divided by 2pi*distance.

    Args:
        distance: Distance from the center of the 2D gaussian distribution.
        sig68: Sigma corresponding to the ~68% containment, ie 1.515*sigma_rayleigh.
    Returns:
        Probability density function at location X based on a 2D gaussian distribution.
    """
    return rayleigh_pdf(distance, sig68/1.515)/(2*math.pi*distance)

def scaled_pdf_slow(distance, src_err68):
    """Scaled probability density function to be 1 for distance=src_err68. It is slower than scaled_pdf but more readable.

    Args:
        distance: distance from the event.
        src_err68: Angular error of the event, corresponding to 68% containment.
    Returns:
        Scaled probability density function to be 1 for distance=src_err68."""
    return x_pdf(distance, src_err68)/x_pdf(src_err68, src_err68)

def scaled_pdf(distance, sig68):
    """Scaled probability density function to be 1 for distance=sig68. This function is more than 3 times faster than scaled_pdf_slow with the same result.

    Args:
        distance: distance from the event.
        sig68: Angular error of the event, corresponding to 68% containment.
    Returns:
        Scaled probability density function to be 1 for distance=sig68."""
    return np.exp(-np.power(distance, 2)/(2*np.power(sig68/1.515, 2)) + 1.515**2/2)

def get_timeline(opt):
    """Divide the time in timesteps.

    Args:
        opt: Option arguments of the script. (dict)
    Returns:
        timeline: List of pd.Timestamp. All timesteps should be full, ie. the last timestamp is before end - timestep.
    """
    n_steps = int((opt['end']-opt['begin'])/opt['timestep'])
    return [opt['begin'] + step*opt['timestep'] for step in range(n_steps)]

def get_random_times(begin, end, n_samples=1):
    """Draw n_samples random times between begin and end.

    Args:
        begin: Beginning of the timeline. (pd.Timestamp)
        end: End of the timeline. (pd.Timestamp)
        n_samples: Number of random times you want. (int)
    Returns:
        rdm_times: List of datetimes. (pd.datetimes)
    """
    max_timedelta = (end-begin).total_seconds()
    rdm_timedelta = pd.to_timedelta(np.random.random(n_samples)*max_timedelta, unit='seconds')
    rdm_times = begin + rdm_timedelta
    return rdm_times

def type_of_script():
    """Check if running in terminal, jupyter or ipython.

    Returns:
        type_of_script: 'terminal', 'jupyter' or 'ipython' depending on where it is running.
    """
    try:
        ipy_str = str(type(get_ipython())) # pylint: disable=E0602
        if 'zmqshell' in ipy_str:
            return 'jupyter'
        elif 'terminal' in ipy_str:
            return 'ipython'
    except:
        return 'terminal'

def all_combinations(list_, n_comb_inj='all', dataset_list=''):
    """Get combinations of the element of list_. If n_comb_inj='all' get all combinations for all n in [2, len(list_)] corresponding
    to the number of elements to combine. If n_comb_inj is set, returns only combinations for n=n_comb_inj. If n_comb_inj is a list, it
    returns a list of only one list of the indices of these datasets in the dataset_list.

    Args:
        list_: List of elements to combine. (list of int)
        n_comb_inj: Number of elements to combine. If set to 'all' the output will be all the combinations for n in [2, len(list_)]. It can also be a list of datasets. (int, str, list)
        dataset_list: List of all the datasets. It is used to get the correct index of each dataset when n_comb_inj is a list of datasets. (list)
    Returns:
        all_comb: List of combinations, i.e. list of list of indices of the datasets to combine.
    """
    all_comb = []
    if n_comb_inj == 'all':
        for n in range(2, len(list_)+1):
            for comb in list(combinations(list_, n)):
                all_comb += [list(comb)]
    elif isinstance(n_comb_inj, list):
        all_comb = [[index for index, dataset in enumerate(dataset_list) if dataset in n_comb_inj]]
    else:
        for comb in list(combinations(list_, n_comb_inj)):
            all_comb += [list(comb)]

    return list(reversed(all_comb))

def get_split_index(opt):
    """Get the index of the data point at which we split between train and test samples.
    In order to not split the data of a given time step in two, we use the ceil of the number of timesteps for training.

    Args:
        opt: Option arguments of the script. (dict)
    Returns:
        split_index: Index of the data point at which we split between train and test samples.
    """
    n_steps = len(get_timeline(opt))
    npix = hp.nside2npix(opt['nside'])
    return math.ceil(n_steps*TRAIN_SPLIT)*npix

def ids_to_plot(inj_ids, opt=None, n=15, all=False):
    """Get a list of time ids with injected signal.
    If all is True return all time ids.

    Args:
        inj_ids: List of time ids with injected signal. (list)
        n: Number of skymaps to plot. (int)
        all: If True returns all ids. (bool)
    Return:
        time_ids: List of time ids to plot. (list)
    """
    if all:
        time_ids = range(len(get_timeline(opt)))
    else:
        np.random.seed(SEED)
        time_ids = sorted(np.random.choice(inj_ids, size=min(len(inj_ids), n), replace=False))
    return time_ids

def concatenate_dic(dic_list):
    """Concatenate dictionnaries with same structure.

    Args:
        dic_list: List of dictionnaries of lists with the same keys. (dict)
    Return:
        dict: Dictionnary with the same keys but each list is a concatenation of all the dic_list. (dict)
    """
    keys = dic_list[0].keys()
    return {key: np.concatenate([dic[key] for dic in dic_list]) for key in keys}

def job_list_indices(job_id, n_jobs, len_list):
    """To split a list to be sampled among several jobs, get the first and last indices for the job <job_id>

    Args:
        job_id: Id of the current job. (int)
        n_jobs: Total number of jobs. (int)
        len_list: Size of the list to split. (int)
    Returns:
        first_index: Index of the first element of the list for this job. (int)
        last_index: Index of the last element (+1) of the list for this job. (int)
    """
    first_index = job_id*(len_list/n_jobs)
    last_index = (job_id+1)*(len_list/n_jobs)
    return int(first_index), int(last_index)
