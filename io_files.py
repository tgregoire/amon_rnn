# -*- coding: utf-8 -*-
# pylint: disable=C0103
"""
.

Usage:
  data_prep

Options:
  -h --help     Show this screen.
"""
import os
import glob
import warnings
from pprint import pprint
import pandas as pd
import joblib # pylint: disable=E0401
import pickle5 as pickle # pylint: disable=E0401
from keras.models import model_from_json # pylint: disable=E0401
from keras import models # pylint: disable=E0401
from simple_functions import OUTPUT_PATH, get_timeline
# docopt(__doc__)
warnings.filterwarnings('ignore') # put default to have them back
# NB I managed to reduce event_density's time from 8 ms to 3 ms!
# I should have used classes, I think, but that is a bit too late

N_PRED_JOBS = 10
# NB A mail from ACI said that we should use rhel7 using something like: "#PBS -l feature=rhel7" but when trying (with an interactive job) it doesn't work (maybe I should connect somewhere else, I don't know, or just contact ACI https://www.icds.psu.edu/computing-services/faq-regarding-the-transition-to-rhel7/).
PBS_FILE_BASE = """
#!/bin/bash

#PBS -A {queue}
#PBS -j oe
#PBS -l feature=rhel7
#PBS -l walltime={walltime_hrs}:00:00
#PBS -l nodes=1:ppn={ppn}{gpus}
#PBS -l mem={mem}gb
{qos}

# Get started
echo "Job started on $(hostname) at $(date)"

# Go to the correct place
cd /storage/home/tmg5746/Desktop/OutlierDetection/

# Run the job itself
{command}

# Finish up
echo "Job Ended at $(date)"
"""

# %% General
def save_obj(obj, path, verbose=False):
    """Save python object in a pkl file.

    Args:
        obj: Python object to save.
        path: Path of the file.
    """
    with open(path, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
        if verbose:
            print(path.split('/')[-1], 'file written.')

# I need to define this here because it is used in DATASETS for dur_dec_HAWC_hotspots_spline
def load_obj(path):
    """Load python object from a pkl file.

    Args:
        path: Path of the file.
    Returns:
        obj: Python object loaded.
    """
    with open(path, 'rb') as f:
        return pickle.load(f)

def short_str_dataset_list(dataset_list):
    """Convert the list of datasets into a short summary string.

    Args:
        dataset_list: List of datasets. (list)
    Returns:
        dataset_list_str: Short string summarizing the dataset list. (str)
    """
    dataset_list_str = ''
    for dataset in sorted(dataset_list):
        if dataset == 'HAWC_bursts':
            dataset_list_str += 'Hb'
        elif dataset == 'HAWC_hotspots':
            dataset_list_str += 'Hh'
        else:
            dataset_list_str += dataset[0]
    return dataset_list_str

def output_dir(opt):
    """Name directory from options.

    Args:
        opt: Option arguments of the script. (dict)
    Returns:
        filename: File name (str)
    """
    period_lst = str(opt['end']-opt['begin']).split()
    period_str = period_lst[0]+period_lst[1][0]
    timestep_str = '{}h'.format(int(opt['timestep'].total_seconds()/(3600))).replace('.', '_')
    n_src_per_month_str = '{}'.format(int(opt['n_src_per_month']))
    train_prop_percent = opt['train_prop']*100
    if train_prop_percent % 1 == 0:
        train_prop_str = '{}'.format(int(train_prop_percent))
    else:
        train_prop_str = '{}.{}'.format(int(train_prop_percent), int(train_prop_percent*10) - int(train_prop_percent)*10)

    HbBkgInj = '{}HbBkgInj_'.format(int(opt['n_Hb_bkg_inj'])) if int(opt['n_Hb_bkg_inj']) > 0 else ''
    ANTBkgInj = '{}ANTBkgInj_'.format(int(opt['n_ANT_bkg_inj'])) if int(opt['n_ANT_bkg_inj']) > 0 else ''
    suffix = ''
    if opt['file_suffix']:
        suffix = '_'+opt['file_suffix']
    sig_in_train = ''
    if opt['sig_in_train']:
        sig_in_train = '_sigInTrain'
    dataset_list_str = short_str_dataset_list(opt['dataset_list'])
    n_comb_inj = opt['n_comb_inj']
    if isinstance(n_comb_inj, list):
        n_comb_inj = short_str_dataset_list(opt['n_comb_inj'])
    no_sig_inj = ''
    if opt['n_src_per_month'] == 0:
        no_sig_inj = '_NoSig'

    return '{period}_per{step}_nside{nside}_{n_src}srcPerMonth_{n_comb_inj}combInj_{train_prop}%trainProp_{HbBkgInj}{ANTBkgInj}{dataset_list}{sig_in_train}{no_sig_inj}{suffix}'.format(
        period=period_str, step=timestep_str, nside=opt['nside'], n_src=n_src_per_month_str,
        suffix=suffix, sig_in_train=sig_in_train, no_sig_inj=no_sig_inj, n_comb_inj=n_comb_inj, train_prop=train_prop_str,
        HbBkgInj=HbBkgInj, ANTBkgInj=ANTBkgInj, dataset_list=dataset_list_str)

def existing_output_dirs(verbose=False):
    """Get suffixes of skymap file names.

    Args:
        verbose: If True print file names. (bool)
    Returns:
        data_dirs: List of file suffixes that already exist. (list of strings)
    """
    output_path = glob.glob(OUTPUT_PATH+'*d_per*')
    output_dirs = []
    for file_ in output_path:
        output_dirs += [file_.split('/')[-1]]
    output_dirs = sorted(set(output_dirs))
    if verbose:
        for file_name in output_dirs:
            print(file_name)
    return output_dirs

def load_skymaps(opt):
    """Load skymaps corresponding to dataset_list from pkl files in OUTPUT_PATH and put them in a dictionary.
    The file name is deduced from begin and end dates or using custom_name.

    Args:
        opt: Option arguments of the script. (dict)
    Returns:
        skymaps_dic: Dictionary containing the skymaps dataframes for each dataset. (dict of pd.DataFrame)
    """
    skymaps_dic = {}
    for dataset in opt['dataset_list']:
        filename = 'Skymaps_'+dataset+'.pkl'
        skymaps = pd.read_pickle(os.path.join(opt['out_path'], filename))
        skymaps_dic[dataset] = skymaps

    return skymaps_dic

def get_opt_from_args(args, verbose=False):
    """Make a dictionnary of options to run the script from the dictionnary of arguments, ie get the correct types and key names.

    Args:
        args: Dictionnary of arguments given to the script (docopt output). (dict)
    Returns:
        opt: Dictionnary of options. (dict)
    """
    opt = dict((key.replace('--', ''), value) for (key, value) in args.items()) # Remove '--' from arguments keys
    del opt['help']
    opt['dataset_list'] = opt.pop('<datasets>')
    if opt['all_datasets']:
        if opt['dataset_list']:
            raise ValueError("'--all_datasets' and '<datasets>' are given in options only one should be given.")
        opt['dataset_list'] = ['ANTARES', 'Fermi', 'HAWC_bursts', 'HAWC_hotspots', 'IceCube']
    if not opt['dataset_list']:
        opt['dataset_list'] = ['ANTARES', 'HAWC_hotspots', 'HAWC_bursts']
    if opt['write_files'] not in ['0', '1', '2']:
        raise ValueError("write_files option should be in [0, 1, 2]; got {}".format(opt['write_files']))
    opt['write_files'] = int(opt['write_files'])
    opt['n_Hb_bkg_inj'] = int(opt['n_Hb_bkg_inj'])
    opt['n_ANT_bkg_inj'] = int(opt['n_ANT_bkg_inj'])
    opt['n_src_per_month'] = int(opt['n_src_per_month'])
    if opt['n_comb_inj'] != 'all':
        comb_list_split = opt['n_comb_inj'].split(',')
        if all([comb_item in opt['dataset_list'] for comb_item in comb_list_split]):
            opt['n_comb_inj'] = comb_list_split
        else:
            opt['n_comb_inj'] = int(opt['n_comb_inj'])
            if opt['n_comb_inj'] > len(opt['dataset_list']):
                raise ValueError("n_comb_inj greater than the number of datasets in list.")
            elif opt['n_comb_inj'] <= 0:
                raise ValueError("n_comb_inj <= 0")
    opt['train_prop'] = float(opt['train_prop'])
    if opt['train_prop'] <= 0:
        raise ValueError("train_prop < 0")
    if opt['train_prop'] > 0.5:
        raise ValueError("train_prop > 0.5")
    if float(opt['timestep']) <= 0:
        raise ValueError("Timestep should be a float > 0; got {}".format(float(opt['timestep'])))
    opt['timestep'] = pd.Timedelta(float(opt['timestep']), unit='d')
    opt['begin'] = pd.Timestamp(opt['begin'])
    opt['end'] = pd.Timestamp(opt['end'])
    opt['end'] = get_timeline(opt)[-1]+opt['timestep']
    opt['nside'] = int(opt['nside'])
    opt['out_path'] = os.path.join(OUTPUT_PATH, output_dir(opt))
    opt['n_jobs'] = int(opt['n_jobs'])
    if 'job_id' in opt:
        opt['job_id'] = int(opt['job_id'])
        if opt['job_id'] >= opt['n_jobs']:
            raise ValueError("job_id is >= n_jobs.")

    for arg in ['n_src_per_month', 'nside', 'n_jobs', 'job_id', 'n_Hb_bkg_inj', 'n_ANT_bkg_inj']:
        if arg in opt and opt[arg] < 0:
            raise ValueError(arg+" < 0")

    job_types = ['job_events_df', 'job_skymaps', 'job_X', 'job_clf', 'job_predictions', 'visualize']
    if all([not opt[job_type] for job_type in job_types]):
        for job_type in job_types:
            opt[job_type] = True

    if verbose:
        pprint(opt)
    return opt

def use_written_file(write_files, path, verbose=True):
    """True if should use written file instead of computing it. (I created this function for readability.)

    Args:
        write_files: If '0' don't write don't load, '1' write if files don't exist else load, '2' always write,
                        even if files already exist. (int in 0, 1, 2)
        path: Path of the file to load.
        verbose: If True print that file already exists.
    Returns:
        use_written_file: True if file should be loaded, else False. (Boolean)
    """
    if os.path.exists(path) and write_files == 1:
        filename = path.split('/')[-1]
        if verbose:
            print(filename, 'file already exists.')
        return True
    return False

def create_dir(opt, verbose=False):
    """Create dir corresponding to options.

    Args:
        opt: Option arguments of the script. (dict)
    """
    if opt['write_files'] != 0:
        if not os.path.exists(opt['out_path']):
            os.mkdir(opt['out_path'])
            if verbose:
                print(output_dir(opt), "created")
        elif verbose:
            print(output_dir(opt), "already exists")

def write_pbs_file(job_type, opt, walltime_hrs, ppn=1, mem=20, filename_ext='', extra_opt='', parallel_args='', gpu=False):
    """Create and write pbs file with job parameters.

    Args:
        job_type: Type of job to launch. (string)
        opt: Option arguments of the script. (dict)
        walltime_hrs: Maximum time of the job. (int)
        filename_ext: Extension of the pbs file name 'job_<job_type><filename_ext>.pbs'. (string)
        extra_opt: Extra options to add to the job command. (string)
    """
    if filename_ext != '':
        filename_ext = '_'+filename_ext
    filename = 'job{filename_ext}_{job_type}.pbs'.format(job_type=job_type, filename_ext=filename_ext)
    f = open(filename, 'w')
    if gpu:
        queue = 'cyberlamp'
        gpus = ':gpus=1:shared'
        qos = '#PBS -l qos=cl_gpu'
    else:
        queue = 'open'
        gpus = ''
        qos = ''
    timestep_in_days = '{:.3f}'.format(opt['timestep'].seconds/3600/24+opt['timestep'].days)
    if job_type != 'visualize':
        job_type = 'job_'+job_type
    dataset_list = ''
    for dataset in opt['dataset_list']:
        dataset_list += ' '+dataset
    n_comb_inj = opt['n_comb_inj']
    if isinstance(n_comb_inj, list):
        n_comb_inj = ','.join(opt['n_comb_inj'])
    n_jobs = opt['n_jobs']
    if job_type == 'predictions':
        n_jobs = N_PRED_JOBS
    end = str(opt['end']).replace(' ', 'T')
    if ppn == 1:
        command = "/storage/home/tmg5746/work/miniconda3/envs/miniconda37/bin/python /storage/home/tmg5746/Desktop/OutlierDetection/PyOD_playground.py "\
                  "--write_files=2 --nside={nside} --timestep={timestep} --train_prop={train_prop} --{job_type} --n_comb_inj={n_comb_inj} "\
                  "--n_jobs={n_jobs} --file_suffix={file_suffix} --n_src_per_month={n_src_per_month} --end={end} --n_Hb_bkg_inj={n_Hb_bkg_inj} --n_ANT_bkg_inj={n_ANT_bkg_inj} {extra_opt}{datasets}"\
                  "".format(nside=opt['nside'], timestep=timestep_in_days, train_prop=opt['train_prop'], job_type=job_type,
                            n_comb_inj=n_comb_inj, n_jobs=n_jobs, file_suffix=opt['file_suffix'], n_src_per_month=opt['n_src_per_month'],
                            end=end, n_Hb_bkg_inj=opt['n_Hb_bkg_inj'], n_ANT_bkg_inj=opt['n_ANT_bkg_inj'], extra_opt=extra_opt, datasets=dataset_list)
    elif ppn > 1:
        command = "/storage/home/tmg5746/work/miniconda3/envs/miniconda37/bin/parallel --jobs={ppn} '/storage/home/tmg5746/work/miniconda3/envs/miniconda37/bin/python "\
                  "/storage/home/tmg5746/Desktop/OutlierDetection/PyOD_playground.py --write_files=2 --nside={nside} --timestep={timestep} --train_prop={train_prop} "\
                  "--{job_type} --n_comb_inj={n_comb_inj} --n_jobs={n_jobs} --file_suffix={file_suffix} --n_src_per_month={n_src_per_month} --end={end} "\
                  "--n_Hb_bkg_inj={n_Hb_bkg_inj} --n_ANT_bkg_inj={n_ANT_bkg_inj} {extra_opt}{datasets}' ::: {parallel_args}"\
                  "".format(ppn=ppn, nside=opt['nside'], timestep=timestep_in_days, train_prop=opt['train_prop'], job_type=job_type,
                            n_comb_inj=n_comb_inj, n_jobs=n_jobs, file_suffix=opt['file_suffix'], n_src_per_month=opt['n_src_per_month'],
                            end=end, n_Hb_bkg_inj=opt['n_Hb_bkg_inj'], n_ANT_bkg_inj=opt['n_ANT_bkg_inj'], extra_opt=extra_opt, datasets=dataset_list, parallel_args=parallel_args)

    file_content = PBS_FILE_BASE.format(queue=queue, walltime_hrs=walltime_hrs, ppn=ppn, gpus=gpus, mem=mem, qos=qos, command=command)
    f.write(file_content)
    f.close()

def save_clf(clf, path, clf_name, verbose=False):
    """Save clf in a pkl or json file.

    Args:
        clf: Classifier model to save.
        path: Path of the directory where to save.
        clf_name: Classifier model name.
    """
    path_clf_name = os.path.join(path, clf_name)
    # Keras objects are not picklable so we use json instead
    if 'AutoEncoder' in clf_name:
        clf_model = clf.model_
        clf_json = clf_model.to_json()
        with open(path_clf_name+'.json', "w") as json_file:
            json_file.write(clf_json)
        clf.model_ = None
        joblib.dump(clf, path_clf_name+'.pkl')
        clf.model_ = clf_model
        # NB if I want the weights of autoencoder, do like https://github.com/yzhao062/pyod/issues/88#issuecomment-615343139
        if verbose:
            print('clf file', clf_name, 'written.')

    elif 'SO-GAAL' in clf_name:
        clf_model = clf.combine_model
        clf_discriminator = clf.discriminator
        # clf_generator = clf.generator

        clf_model.save(path_clf_name+'_model')
        clf_discriminator.save(path_clf_name+'_discr')

        clf.combine_model = None
        clf.discriminator = None
        clf.generator = None

        joblib.dump(clf, path_clf_name+'.pkl')

        clf.combine_model = clf_model
        clf.discriminator = clf_discriminator
        # clf.generator = clf_generator
        # NB if I want the weights of autoencoder, do like https://github.com/yzhao062/pyod/issues/88#issuecomment-615343139
        if verbose:
            print('clf file', clf_name, 'written.')
    elif 'MO-GAAL' in clf_name:
        clf_discriminator = clf.discriminator

        clf_discriminator.save(path_clf_name+'_discr')

        clf.discriminator = None
        clf.generator = None

        joblib.dump(clf, path_clf_name+'.pkl')

        clf.discriminator = clf_discriminator
        # clf.generator = clf_generator
        # NB if I want the weights of autoencoder, do like https://github.com/yzhao062/pyod/issues/88#issuecomment-615343139
        if verbose:
            print('clf file', clf_name, 'written.')
    else:
        joblib.dump(clf, path_clf_name+'.pkl')
        if verbose:
            print('clf file', clf_name+'.pkl', 'written.')

def load_clf(path, clf_name):
    """Load python object from a pkl file.

    Args:
        path: Path of the directory where to load.
        clf_name: Classifier model name.
    Returns:
        clf: Classifier model loaded.
    """
    path_clf_name = os.path.join(path, clf_name)
    if 'AutoEncoder' in clf_name:
        clf = joblib.load(path_clf_name+'.pkl')

        json_file = open(path_clf_name+'.json', 'r')
        loaded_model_json = json_file.read()
        loaded_model_json = loaded_model_json.replace("\"ragged\": false,", " ")
        json_file.close()
        clf.model_ = model_from_json(loaded_model_json)
        return clf

    elif 'SO-GAAL' in clf_name:
        clf = joblib.load(path_clf_name+'.pkl')
        clf.combine_model = models.load_model(path_clf_name+'_model')
        clf.discriminator = models.load_model(path_clf_name+'_discr')
        return clf

    elif 'MO-GAAL' in clf_name:
        clf = joblib.load(path_clf_name+'.pkl')
        clf.discriminator = models.load_model(path_clf_name+'_discr')
        return clf
    # else:
    return joblib.load(path_clf_name+'.pkl')
