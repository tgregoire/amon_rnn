# -*- coding: utf-8 -*-
"""
Unittesting module.
"""
import io
import sys
import glob
import unittest
import math
import pandas as pd
import numpy as np
import numpy.testing as npt
from astropy.time import Time
from simple_functions import *
from io_files import *
from visualize import *
from data_prep import *  # pylint: disable=W0614, W0401
# TODO check that NN do not distinguish scrambled and real data (if not possible eventually check that distributions are the same)
# TODO from time to time test with real data instead of dummy


class TestGetData(unittest.TestCase):
    """Test get data"""

    def test_fermi_time_selection(self):
        """Test to get Fermi data with begin and end options to open only dedicated file."""
        begin = pd.Timestamp('2018-03-20T01:41:04')
        end = pd.Timestamp('2018-04-09T02:42:04')
        dataframe = get_data('Fermi', begin, end)
        self.assertEqual(len(dataframe), 3664415)

    def test_time_selection_value_error(self):
        """Test to we get an error if end is before first event or begin after last. NB no error raised if begin before first event or end after last because it might be normal."""
        self.assertRaises(ValueError, get_data, 'ANTARES', pd.Timestamp('2001-03-20T01:41:04'), pd.Timestamp('2001-04-09T02:42:04'))
        self.assertRaises(ValueError, get_data, 'ANTARES', pd.Timestamp('2022-03-20T01:41:04'), pd.Timestamp('2023-04-09T02:42:04'))

    def test_fermi_value_error(self):
        """Test we get an error if getting Fermi without begin and end."""
        begin = pd.Timestamp('2018-03-20T01:41:04')
        end = pd.Timestamp('2018-04-09T02:42:04')
        self.assertRaises(ValueError, get_data, 'Fermi')
        self.assertRaises(ValueError, get_data, 'Fermi', begin)
        self.assertRaises(ValueError, get_data, 'Fermi', None, end)


class TestEquatorialToLocal(unittest.TestCase):
    """Test convertion from equatorial to local coordinates."""

    def test_antares_convertion(self):
        """Test convertion from equatorial to local ANTARES coordinates."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[197.45, -23.3844, time]]), columns=['ra', 'dec', 'time'])
        self.assertAlmostEqual(equatorial_to_local('ANTARES', df_test)['az'][0], 146.6201912156954, 5)
        self.assertAlmostEqual(equatorial_to_local('ANTARES', df_test)['alt'][0], 16.230520741537777, 5)

    def test_icecube_convertion(self):
        """Test convertion from equatorial to local IceCube coordinates."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[197.45, -23.3844, time]]), columns=['ra', 'dec', 'time'])
        self.assertAlmostEqual(equatorial_to_local('IceCube', df_test)['az'][0], 104.94054882881836, 5)
        self.assertAlmostEqual(equatorial_to_local('IceCube', df_test)['alt'][0], 23.473786674088565, 5)

    def test_auger_convertion(self):
        """Test convertion from equatorial to local Auger coordinates."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[197.45, -23.3844, time]]), columns=['ra', 'dec', 'time'])
        self.assertAlmostEqual(equatorial_to_local('Auger', df_test)['az'][0], 120.8151926359764, 5)
        self.assertAlmostEqual(equatorial_to_local('Auger', df_test)['alt'][0], -1.9824600642917944, 5)

    def test_error_no_location(self):
        """Test we get an error if no location is given for this dataset."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[197.45, -23.3844, time]]), columns=['ra', 'dec', 'time'])
        self.assertRaises(ValueError, equatorial_to_local, 'Fermi', df_test)

    def test_dataset_name_option(self):
        """Test local coord columns are renamed with dataset name when dedicated option is used"""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[197.45, -23.3844, time]]), columns=['ra', 'dec', 'time'])
        df_local = equatorial_to_local('ANTARES', df_test, dataset_name=True)
        self.assertTrue('ANTARES-alt' in df_local.columns)
        self.assertTrue('ANTARES-az' in df_local.columns)

    def test_warning_ra_rad(self):
        """Check Warning is printed when ra is in radians."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        array = [[math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time],
                 [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time],
                 [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time],
                 [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time]]
        df_test = pd.DataFrame(np.array(array), columns=['ra', 'dec', 'time'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        equatorial_to_local('ANTARES', df_test)
        expected_output = "Warning: right ascension should be in degrees but seems to be in radians.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

    def test_warning_dec_rad(self):
        """Check Warning is printed when dec is in radians."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        array = [[100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time],
                 [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time],
                 [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time],
                 [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time]]
        df_test = pd.DataFrame(np.array(array), columns=['ra', 'dec', 'time'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        equatorial_to_local('ANTARES', df_test)
        expected_output = "Warning: declination should be in degrees but seems to be in radians.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__


class TestLocalToEquatorial(unittest.TestCase):
    """Test convertion from local to equatorial coordinates."""

    def test_antares_convertion(self):
        """Test convertion from local to equatorial ANTARES coordinates."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[146.6201912156954, 16.230520741537777, time]]), columns=['az', 'alt', 'time'])
        self.assertAlmostEqual(local_to_equatorial('ANTARES', df_test)['ra'][0], 197.45, 5)
        self.assertAlmostEqual(local_to_equatorial('ANTARES', df_test)['dec'][0], -23.3844, 5)

    def test_icecube_convertion(self):
        """Test convertion from local to equatorial IceCube coordinates."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[104.94054882881836, 23.473786674088565, time]]), columns=['az', 'alt', 'time'])
        self.assertAlmostEqual(local_to_equatorial('IceCube', df_test)['ra'][0], 197.45, 5)
        self.assertAlmostEqual(local_to_equatorial('IceCube', df_test)['dec'][0], -23.3844, 5)

    def test_auger_convertion(self):
        """Test convertion from local to equatorial Auger coordinates."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[120.8151926359764, -1.9824600642917944, time]]), columns=['az', 'alt', 'time'])
        self.assertAlmostEqual(local_to_equatorial('Auger', df_test)['ra'][0], 197.45, 5)
        self.assertAlmostEqual(local_to_equatorial('Auger', df_test)['dec'][0], -23.3844, 5)

    def test_error_no_location(self):
        """Test we get an error if no location is given for this dataset."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        df_test = pd.DataFrame(np.array([[120.8151926359764, -1.9824600642917944, time]]), columns=['az', 'alt', 'time'])
        self.assertRaises(ValueError, local_to_equatorial, 'Fermi', df_test)

    def test_warning_az_rad(self):
        """Check Warning is printed when az is in radians."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        array = [[math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time],
                 [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time],
                 [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time],
                 [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time], [math.pi, -23.3844, time]]
        df_test = pd.DataFrame(np.array(array), columns=['az', 'alt', 'time'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        local_to_equatorial('ANTARES', df_test)
        expected_output = "Warning: azimuth should be in degrees but seems to be in radians.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

    def test_warning_alt_rad(self):
        """Check Warning is printed when alt is in radians."""
        time = Time('2017-08-17T12:41:04', format='isot', scale='utc')  # GW170817 time
        array = [[100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time],
                 [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time], [100.1241, -math.pi / 2, time],
                 [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time],
                 [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time], [100.1241, math.pi / 2, time]]
        df_test = pd.DataFrame(np.array(array), columns=['az', 'alt', 'time'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        local_to_equatorial('ANTARES', df_test)
        expected_output = "Warning: altitude should be in degrees but seems to be in radians.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__


class TestPermute(unittest.TestCase):
    """Test permutation."""

    def test_label_value_error(self):
        """Test that a ValueError is raised if label is not a string or list of string."""
        dataframe = get_data("ANTARES").head()
        self.assertRaises(TypeError, permute, dataframe, 42)
        self.assertRaises(TypeError, permute, dataframe, dataframe)
        self.assertRaises(TypeError, permute, dataframe, ['ra', 42, 'dec'])
        self.assertRaises(ValueError, permute, dataframe, '42')
        self.assertRaises(ValueError, permute, dataframe, ['ra', 'dec', '42'])


class TestScramble(unittest.TestCase):
    """Test scrambling is correctly done."""

    def test_scrambling_antares(self):
        """Test scrambling changes ra and dec; time is scrambled but not pvalue."""
        ant_df = get_data('ANTARES')
        ant_df.reset_index(inplace=True)
        ant_df_scram = scramble_data('ANTARES', ant_df)
        self.assertTrue((ant_df_scram['time'] == ant_df['time']).all())
        self.assertTrue((ant_df_scram['pvalue'] != ant_df['pvalue']).any())
        self.assertTrue((ant_df_scram['dec'] != ant_df['dec']).all())
        self.assertTrue((ant_df_scram['ra'] != ant_df['ra']).all())
        ant_df.sort_values(by='id', inplace=True)
        ant_df.reset_index(inplace=True, drop=True)
        ant_df_scram.sort_values(by='id', inplace=True)
        ant_df_scram.reset_index(inplace=True, drop=True)
        self.assertTrue((ant_df_scram['time'] != ant_df['time']).any())
        self.assertTrue((ant_df_scram['pvalue'] == ant_df['pvalue']).all())
        self.assertTrue((ant_df_scram['dec'] != ant_df['dec']).all())
        self.assertTrue((ant_df_scram['ra'] != ant_df['ra']).all())

    def test_scrambling_icecube(self):
        """Test scrambling changes ra and dec; time and time_msec are scrambled but not the rest."""
        ic_df = get_data('IceCube_dummy')
        ic_df.reset_index(inplace=True)
        ic_df_scram = scramble_data('IceCube_dummy', ic_df)
        self.assertTrue((ic_df_scram[['time', 'time_msec']] == ic_df[['time', 'time_msec']]).all().all())
        self.assertTrue((ic_df_scram['sigmaR'] != ic_df['sigmaR']).any())
        self.assertTrue((ic_df_scram['energy'] != ic_df['energy']).any())
        self.assertTrue((ic_df_scram['signalness'] != ic_df['signalness']).any())
        self.assertTrue((ic_df_scram['src_error90'] != ic_df['src_error90']).any())
        self.assertTrue((ic_df_scram['src_error'] != ic_df['src_error']).any())
        self.assertTrue((ic_df_scram['dec'] != ic_df['dec']).any())
        self.assertTrue((ic_df_scram['ra'] != ic_df['ra']).all())
        ic_df.sort_values(by='id', inplace=True)
        ic_df.reset_index(inplace=True, drop=True)
        ic_df_scram.sort_values(by='id', inplace=True)
        ic_df_scram.reset_index(inplace=True, drop=True)
        self.assertTrue((ic_df_scram['time'] != ic_df['time']).any())
        self.assertTrue((ic_df_scram['time_msec'] != ic_df['time_msec']).any())
        self.assertTrue((ic_df_scram[['sigmaR', 'energy', 'signalness', 'src_error90', 'src_error']] == ic_df[['sigmaR', 'energy', 'signalness', 'src_error90', 'src_error']]).all().all())
        self.assertTrue((ic_df_scram['dec'] != ic_df['dec']).any())
        self.assertTrue((ic_df_scram['ra'] != ic_df['ra']).all())

    def test_scrambling_auger(self):
        """Test scrambling changes ra and dec; time is scrambled but not sigmaR and false_pos."""
        aug_df = get_data('Auger_dummy')
        aug_df.reset_index(inplace=True)
        aug_df_scram = scramble_data('Auger_dummy', aug_df)
        self.assertTrue((aug_df_scram['time'] == aug_df['time']).all())
        self.assertTrue((aug_df_scram['sigmaR'] != aug_df['sigmaR']).any())
        self.assertTrue((aug_df_scram['false_pos'] != aug_df['false_pos']).any())
        self.assertTrue((aug_df_scram['dec'] != aug_df['dec']).all())
        self.assertTrue((aug_df_scram['ra'] != aug_df['ra']).all())
        aug_df.sort_values(by='id', inplace=True)
        aug_df.reset_index(inplace=True, drop=True)
        aug_df_scram.sort_values(by='id', inplace=True)
        aug_df_scram.reset_index(inplace=True, drop=True)
        self.assertTrue((aug_df_scram['time'] != aug_df['time']).any())
        self.assertTrue((aug_df_scram[['sigmaR', 'false_pos']] == aug_df[['sigmaR', 'false_pos']]).all().all())
        self.assertTrue((aug_df_scram['dec'] != aug_df['dec']).all())
        self.assertTrue((aug_df_scram['ra'] != aug_df['ra']).all())

    def test_scrambling_hawc_bursts(self):
        """Test dec is scrambled and only dec."""
        hwc_df = get_data('HAWC_bursts')
        hwc_df_scram = scramble_data('HAWC_bursts', hwc_df)
        self.assertTrue((hwc_df_scram[['dec']] != hwc_df[['dec']]).any().any())
        self.assertTrue((hwc_df_scram.drop('dec', axis=1) == hwc_df.drop('dec', axis=1)).all().all())

    def test_scrambling_hawc_hotspots(self):
        """Test dec, duration are scrambled and MJD_i, MJD_f are affected."""
        hwc_df = get_data('HAWC_hotspots')
        hwc_df.sort_values(by='id', inplace=True)
        hwc_df.reset_index(inplace=True)
        hwc_df_scram = scramble_data('HAWC_hotspots', hwc_df)
        hwc_df_scram.sort_values(by='id', inplace=True)
        hwc_df_scram.reset_index(inplace=True, drop=True)
        self.assertTrue((hwc_df_scram[['dec']] != hwc_df[['dec']]).any().any())
        self.assertTrue((hwc_df_scram[['duration']] != hwc_df[['duration']]).any().any())
        self.assertTrue((hwc_df_scram[['MJD_i']] != hwc_df[['MJD_i']]).any().any())
        self.assertTrue((hwc_df_scram[['MJD_f']] != hwc_df[['MJD_f']]).any().any())
        self.assertTrue((hwc_df_scram.drop(['dec', 'duration', 'MJD_i', 'MJD_f'], axis=1) == hwc_df.drop(['dec', 'duration', 'MJD_i', 'MJD_f'], axis=1)).all().all())

    def test_scrambling_fermi(self):
        """Test nothing is scrambled for Fermi."""
        fermi_df = get_data('Fermi_dummy')  # using Fermi_dummy to go faster
        fermi_df_scram = scramble_data('Fermi_dummy', fermi_df)
        self.assertTrue((fermi_df_scram == fermi_df).all().all())

    def test_if_index_column(self):
        """Check we did not forget to drop an 'index' column while reseting indices."""
        ant_df = get_data('ANTARES')
        ic_df = get_data('IceCube_dummy')
        auger_df = get_data('Auger_dummy')
        hwc_bursts_df = get_data('HAWC_bursts')
        hwc_hotspots_df = get_data('HAWC_hotspots')
        fermi_df = get_data('Fermi_dummy')

        self.assertTrue('index' not in scramble_data('ANTARES', ant_df).columns)
        self.assertTrue('index' not in scramble_data('IceCube_dummy', ic_df).columns)
        self.assertTrue('index' not in scramble_data('Auger_dummy', auger_df).columns)
        self.assertTrue('index' not in scramble_data('HAWC_bursts', hwc_bursts_df).columns)
        self.assertTrue('index' not in scramble_data('HAWC_hotspots', hwc_hotspots_df).columns)
        self.assertTrue('index' not in scramble_data('Fermi_dummy', fermi_df).columns)
        self.assertTrue('index' not in ant_df.columns)
        self.assertTrue('index' not in ic_df.columns)
        self.assertTrue('index' not in auger_df.columns)
        self.assertTrue('index' not in hwc_bursts_df.columns)
        self.assertTrue('index' not in hwc_hotspots_df.columns)
        self.assertTrue('index' not in fermi_df.columns)


class TestTimeSelection(unittest.TestCase):
    """Test time_selection."""

    def test_time_selection(self):
        """Check timestamps inside time window are kept and not others."""
        dataset = "ANTARES"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-05T12:41:04')}
        data = np.array([pd.Timestamp('2019-12-03T12:41:04'),
                         pd.Timestamp('2019-11-04T12:41:04'),
                         pd.Timestamp('2020-12-04T12:41:04'),

                         pd.Timestamp('2019-12-05T01:41:04'),
                         pd.Timestamp('2019-12-04T12:41:05'), ])
        dataframe = pd.DataFrame(data, columns=['time'])
        self.assertListEqual(time_selection(dataset, dataframe, time_window).index.to_list(), [3, 4])

    def test_time_msec_selection(self):
        """Check timestamps inside time window are kept and not others."""
        dataset = "IceCube"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-05T12:41:04')}
        data = np.array([[pd.Timestamp('2019-12-03T12:41:04'), 858988],
                         [pd.Timestamp('2019-11-04T12:41:04'), 934076],
                         [pd.Timestamp('2020-12-04T12:41:04'), 183988],
                         [pd.Timestamp('2019-12-04T12:41:03'), 934076],
                         [pd.Timestamp('2019-12-05T12:41:04'), 135221],

                         [pd.Timestamp('2019-12-05T01:41:03'), 935221],
                         [pd.Timestamp('2019-12-04T12:41:05'), 391274], ])
        dataframe = pd.DataFrame(data, columns=['time', 'time_msec'])
        self.assertListEqual(time_selection(dataset, dataframe, time_window).index.to_list(), [5, 6])

        dataset = "Fermi_dummy"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-05T12:41:04')}
        data = np.array([[pd.Timestamp('2019-12-03T12:41:04'), 988],
                         [pd.Timestamp('2019-11-04T12:41:04'), 76],
                         [pd.Timestamp('2020-12-04T12:41:04'), 183],
                         [pd.Timestamp('2019-12-04T12:41:03'), 934],
                         [pd.Timestamp('2019-12-05T12:41:04'), 121],

                         [pd.Timestamp('2019-12-05T01:41:03'), 931],
                         [pd.Timestamp('2019-12-04T12:41:05'), 374], ])
        dataframe = pd.DataFrame(data, columns=['time', 'time_msec'])
        self.assertListEqual(time_selection(dataset, dataframe, time_window).index.to_list(), [5, 6])

    def test_hawc_hotspots(self):
        """Check time selection for hawc hotspots is correct."""
        dataset = "HAWC_hotspots"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-05T12:41:04')}
        data = np.array([[pd.Timestamp('2019-12-04T12:41:04'), 58820.50851853, 58820.52851851],
                         [pd.Timestamp('2019-12-04T12:41:04'), 58826.50851, 58826.528],

                         [pd.Timestamp('2019-12-04T12:41:04'), 58821.52851853, 58822.52851851],
                         [pd.Timestamp('2019-12-04T12:41:04'), 58822, 58828.52851851],
                         [pd.Timestamp('2019-12-04T12:41:04'), 58810.528518, 58828.52851851],
                         [pd.Timestamp('2019-12-04T12:41:04'), 58810.528518, 58822.52], ])
        dataframe = pd.DataFrame(data, columns=['time', 'MJD_i', 'MJD_f'])  # NB time column is not used
        self.assertListEqual(time_selection(dataset, dataframe, time_window).index.to_list(), [2, 3, 4, 5])

    def test_hawc_bursts(self):
        """Check time selection for hawc bursts is correct."""
        dataset = "HAWC_bursts"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-05T12:41:04')}
        data = np.array([[pd.Timestamp('2019-12-04T12:40:04'), 10],
                         [pd.Timestamp('2019-12-04T12:21:04'), 1000],
                         [pd.Timestamp('2019-12-05T13:10:04'), 1000],

                         [pd.Timestamp('2019-12-04T12:41:04'), 10],
                         [pd.Timestamp('2019-12-04T15:41:04'), 1000],
                         [pd.Timestamp('2019-12-05T12:41:04'), 100], ])
        dataframe = pd.DataFrame(data, columns=['time', 'deltaT'])
        self.assertListEqual(time_selection(dataset, dataframe, time_window).index.to_list(), [3, 4, 5])

    def test_stop_before_start(self):
        """Check a ValueError is raised if start time is after stop time."""
        dataset = "ANTARES"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-03T12:41:04')}
        data = np.array([pd.Timestamp('2019-12-03T12:41:04'), ])
        dataframe = pd.DataFrame(data, columns=['time'])
        self.assertRaises(ValueError, time_selection, dataset, dataframe, time_window)

    def test_error_wrong_type(self):
        """Check a TypeError is raised if start or stop is not a pd.Timestamp."""
        dataset = "ANTARES"
        time_window = {'start': '2019-12-02T12:41:04', 'stop': '2019-12-03T12:41:04'}
        data = np.array([[pd.Timestamp('2019-12-04T12:41:04')],
                         [pd.Timestamp('2019-12-04T12:41:04')],
                         [pd.Timestamp('2019-12-04T12:41:04')],
                         [pd.Timestamp('2019-12-04T12:41:04')],
                         [pd.Timestamp('2019-12-04T12:41:04')],
                         [pd.Timestamp('2019-12-04T12:41:04')], ])
        dataframe = pd.DataFrame(data, columns=['time'])
        self.assertRaises(TypeError, time_selection, dataset, dataframe, time_window)

    # def test_value_error_evt_time(self):
    #     """Check a ValueError is raised if stop is before first event or start is after last."""
    #     dataset = "ANTARES"
    #     data = np.array([[pd.Timestamp('2017-12-04T12:41:04')],
    #                      [pd.Timestamp('2018-12-04T12:41:04')],
    #                      [pd.Timestamp('2019-12-04T12:41:04')],
    #                      [pd.Timestamp('2020-12-04T12:41:04')], ])
    #     dataframe = pd.DataFrame(data, columns=['time'])
    #     self.assertRaises(ValueError, time_selection, dataset, dataframe, {'start': pd.Timestamp('2015-12-02T12:41:04'), 'stop': pd.Timestamp('2016-12-05T12:41:04')})
    #     self.assertRaises(ValueError, time_selection, dataset, dataframe, {'start': pd.Timestamp('2021-12-02T12:41:04'), 'stop': pd.Timestamp('2022-12-05T12:41:04')})

    def test_warning_empty(self):
        """Check timestamps inside time window are kept and not others."""
        dataset = "ANTARES"
        time_window = {'start': pd.Timestamp('2019-12-04T12:41:04'), 'stop': pd.Timestamp('2019-12-05T12:41:04')}
        data = np.array([pd.Timestamp('2019-12-03T12:41:04'),
                         pd.Timestamp('2019-11-04T12:41:04'),
                         pd.Timestamp('2020-12-04T12:41:04'),

                         pd.Timestamp('2019-12-08T01:41:04'),
                         pd.Timestamp('2019-12-03T12:41:05'), ])
        dataframe = pd.DataFrame(data, columns=['time'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        time_selection(dataset, dataframe, time_window)
        expected_output = "Warning: not a single event in the time window anywhere in the sky.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__


class TestAssignRaDecRad(unittest.TestCase):
    """Test assign_ra_dec_rad"""

    def test_warning_ra_rad(self):
        """Check warning is printed if ra is in radians."""
        data = np.array([[0, 0],
                         [math.pi, 0],
                         [math.pi, 0],
                         [math.pi, 0],
                         [0, 90],
                         [0, -90],
                         [0, 90],
                         [0, -90],
                         [math.pi, 90],
                         [math.pi, 90],
                         [math.pi, 45], ])
        dataframe = pd.DataFrame(data, columns=['ra', 'dec'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        assign_ra_dec_rad(dataframe)
        expected_output = "Warning: right ascension should be in degrees but seems to be in radians.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

    def test_warning_dec_rad(self):
        """Check warning is printed if dec is in radians."""
        data = np.array([[0, 0],
                         [360, 0],
                         [350, 0],
                         [180, 0],
                         [0, math.pi / 2],
                         [0, -math.pi / 2],
                         [0, math.pi / 2],
                         [0, -math.pi / 2],
                         [90, math.pi / 2],
                         [180, math.pi / 2],
                         [45, math.pi / 4], ])
        dataframe = pd.DataFrame(data, columns=['ra', 'dec'])

        captured_output = io.StringIO()
        sys.stdout = captured_output
        assign_ra_dec_rad(dataframe)
        expected_output = "Warning: declination should be in degrees but seems to be in radians.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__


class TestGreatCircleDistance(unittest.TestCase):
    """Test great circle distance."""

    def test_distance(self):  # pylint: disable=R0201
        """Check great circle distance is correcly computed."""
        data = np.array([[0, 0],
                         [360, 0],
                         [350, 0],
                         [180, 0],
                         [0, 90],
                         [0, -90],
                         [90, 90],
                         [180, 90],
                         [45, 45], ])
        dataframe = pd.DataFrame(data, columns=['ra', 'dec'])
        dataframe = assign_ra_dec_rad(dataframe)
        position = {'ra_rad': 0, 'dec_rad': 0}
        expected_dist = np.array([0., 0., 10., 180., 90., 90., 90., 90., 60.])
        npt.assert_allclose(great_circle_distance(dataframe, position), expected_dist, atol=1e-5)
        position = {'ra_rad': math.pi, 'dec_rad': -math.pi / 2.}
        expected_dist = np.array([90., 90., 90., 90., 180., 0., 180., 180., 135.])
        npt.assert_allclose(great_circle_distance(dataframe, position), expected_dist, atol=1e-5)

    def test_distance_comparison(self):  # pylint: disable=R0201
        """Compare great_circle_distance with the slower function great_circle_distance_slow."""
        data = np.array([[0, 0],
                         [360, 0],
                         [350, 0],
                         [180, 0],
                         [0, 90],
                         [0, -90],
                         [90, 90],
                         [180, 90],
                         [45, 45], ])
        dataframe = pd.DataFrame(data, columns=['ra', 'dec'])
        dataframe = assign_ra_dec_rad(dataframe)
        position = {'ra_rad': 0, 'dec_rad': 0}
        position_slow = {'ra': 0, 'dec': 0}
        npt.assert_allclose(great_circle_distance(dataframe, position), great_circle_distance_slow(dataframe, position_slow), atol=1e-5)
        position = {'ra_rad': math.pi, 'dec_rad': -math.pi / 2.}
        position_slow = {'ra': 180, 'dec': -90}
        npt.assert_allclose(great_circle_distance(dataframe, position), great_circle_distance_slow(dataframe, position_slow), atol=1e-5)

    def test_value_error_empty(self):  # pylint: disable=R0201
        """Check ValueError is raised if dataframe is empty."""
        dataframe = pd.DataFrame()
        position = {'ra_rad': 0, 'dec_rad': 0}
        self.assertRaises(ValueError, great_circle_distance, dataframe, position)


class TestScaledPDF(unittest.TestCase):
    """Test scaled_pdf."""

    def test_scaled_pdf_comparison(self):  # pylint: disable=R0201
        """Compare scaled_pdf with the slower function scaled_pdf_slow."""
        data = np.array([[0.01, 1],
                         [0.5, 1],
                         [0.9, 1],
                         [1, 1],
                         [1.1, 1],
                         [40, 42], ])
        dataframe = pd.DataFrame(data, columns=['distance', 'src_error'])
        npt.assert_allclose(scaled_pdf(dataframe['distance'], dataframe['src_error']), scaled_pdf_slow(dataframe['distance'], dataframe['src_error']), atol=1e-5)


class TestFEventDensity(unittest.TestCase):
    """Test event density function."""

    def test_density(self):
        """Check event density is correcly computed."""
        data = np.array([[0.01, 1],
                         [0.5, 1],
                         [0.9, 1],
                         [1, 1],
                         [1.1, 1],
                         [40, 42], ])
        dataframe = pd.DataFrame(data, columns=['distance', 'src_error'])
        self.assertAlmostEqual(f_event_density(dataframe['distance'], dataframe['src_error']), 4.121432668386671, 5)

        data = np.array([[0.2, 0.1],
                         [1, 0.1],
                         [40, 0.1], ])
        dataframe = pd.DataFrame(data, columns=['distance', 'src_error'])
        self.assertAlmostEqual(f_event_density(dataframe['distance'], dataframe['src_error']), 0.15374660281610586, 5)

    # NB that was usefull for previous event density definition
    # def test_density_continuity(self):
    #     """Test there is no discontinuity between distances lower and larger than src_error."""
    #     dataframe1 = pd.DataFrame(np.array([[0.999, 1]]), columns=['distance', 'src_error'])
    #     dataframe2 = pd.DataFrame(np.array([[1, 1]]), columns=['distance', 'src_error'])
    #     self.assertAlmostEqual(f_event_density(dataframe1['distance'], dataframe1['src_error']), f_event_density(dataframe2['distance'], dataframe2['src_error']), 3)

    def test_value_error(self):
        """Test a ValueError is raised if distance is strictly negative or src_error is negative."""
        dataframe = pd.DataFrame([[1, 0]], columns=['distance', 'src_error'])
        self.assertRaises(ValueError, f_event_density, dataframe['distance'], dataframe['src_error'])
        dataframe = pd.DataFrame([[1, -1]], columns=['distance', 'src_error'])
        self.assertRaises(ValueError, f_event_density, dataframe['distance'], dataframe['src_error'])
        dataframe = pd.DataFrame([[-1, 1]], columns=['distance', 'src_error'])
        self.assertRaises(ValueError, f_event_density, dataframe['distance'], dataframe['src_error'])


class TestGetTimeline(unittest.TestCase):
    """Test get_timeline."""

    def test_get_timeline(self):
        """Test get_timeline gives expected result."""
        opt = {'begin': pd.Timestamp('2018-10-02T02:42:04'),
               'end': pd.Timestamp('2018-10-04T03:18:27'),
               'timestep': pd.Timedelta(1, unit='d'),
              }
        expected_timeline = [pd.Timestamp('2018-10-02T02:42:04'), pd.Timestamp('2018-10-03T02:42:04')]
        self.assertListEqual(get_timeline(opt), expected_timeline)

        opt = {'begin': pd.Timestamp('2018-10-02T23:42:04'),
               'end': pd.Timestamp('2018-10-03T00:40:04'),
               'timestep': pd.Timedelta(1000, unit='s'),
              }
        expected_timeline = [pd.Timestamp('2018-10-02T23:42:04'), pd.Timestamp('2018-10-02T23:58:44'), pd.Timestamp('2018-10-03T00:15:24')]
        self.assertListEqual(get_timeline(opt), expected_timeline)


class TestEventDensity(unittest.TestCase):
    """Test event density."""

    def test_event_density(self):
        """Test event density output expected value."""
        dataset = 'ANTARES'
        data = {'ra_rad': [0],
                'dec_rad': [0],
                'src_err68': [1.1],
                'scaling_factor': [1.],
               }
        dataframe = pd.DataFrame(data)
        position = {'ra_rad': 0, 'dec_rad': 0}
        self.assertAlmostEqual(event_density(dataset, dataframe, position), 1.377272727272727, 5)
        position = {'ra_rad': data['src_err68'][0] * 2, 'dec_rad': 0}
        self.assertAlmostEqual(event_density(dataset, dataframe, position), 0, 5)

        data = {'ra_rad': [0],
                'dec_rad': [0],
                'src_err68': [1.1],
                'scaling_factor': [1.5],
               }
        dataframe = pd.DataFrame(data)
        position = {'ra_rad': 0, 'dec_rad': 0}
        self.assertAlmostEqual(event_density(dataset, dataframe, position), 2.0659090909, 5)

        dataset = 'HAWC_bursts'
        data = {'ra_rad': [0],
                'dec_rad': [0],
                'src_err68': [0.8],
                'scaling_factor': [1.],
               }
        dataframe = pd.DataFrame(data)
        position = {'ra_rad': 0, 'dec_rad': np.radians(80)}
        self.assertAlmostEqual(event_density(dataset, dataframe, position), 0, 5)
        dataset = 'HAWC_hotspots'
        position = {'ra_rad': 0, 'dec_rad': np.radians(-36)}
        self.assertAlmostEqual(event_density(dataset, dataframe, position), 0, 5)

        dataset = 'Fermi'
        data = {'ra_rad': [0],
                'dec_rad': [0],
                'src_err68': [0.8],
                'scaling_factor': [1.],
               }
        dataframe = pd.DataFrame(data)
        position = {'ra_rad': data['src_err68'][0] * 3 + 0.01, 'dec_rad': 0}  # should be null if distance larger than 3 src err68
        self.assertAlmostEqual(event_density(dataset, dataframe, position), 0, 5)
        position1 = {'ra_rad': data['src_err68'][0] * 3 + 0.01, 'dec_rad': 0}  # should be null if distance larger than 3 src err68
        position2 = {'ra_rad': data['src_err68'][0] * 3 - 0.01, 'dec_rad': 0}  # should be null if distance larger than 3 src err68
        self.assertAlmostEqual(event_density(dataset, dataframe, position1), event_density(dataset, dataframe, position2), 5)

    def test_value_error_keys(self):
        """Check ValueError is raised if position is missing 'ra_rad' or 'dec_rad' keys"""
        dataset = 'ANTARES'
        data = {'ra_rad': [0],
                'dec_rad': [0],
                'src_err68': [1.1],
               }
        dataframe = pd.DataFrame(data)
        position = {'ra': 0, 'dec': 0}
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)
        position = {'ra': 0, 'dec_rad': 0}
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)
        position = {'ra_rad': 0, 'dec': 0}
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)
        position = {}
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)

    def test_value_error_columns(self):
        """Check ValueError is raised if dataframe is missing 'ra_rad' or 'dec_rad' columns"""
        dataset = 'ANTARES'
        position = {'ra_rad': 0, 'dec_rad': 0}
        data = {'ra': [0],
                'dec': [0],
                'src_err68': [1.1],
               }
        dataframe = pd.DataFrame(data)
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)
        data = {'ra': [0],
                'dec_rad': [0],
                'src_err68': [1.1],
               }
        dataframe = pd.DataFrame(data)
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)
        data = {'ra_rad': [0],
                'dec': [0],
                'src_err68': [1.1],
               }
        dataframe = pd.DataFrame(data)
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)
        data = {'src_err68': [1.1],
               }
        dataframe = pd.DataFrame(data)
        self.assertRaises(ValueError, event_density, dataset, dataframe, position)


class TestPix2angEq(unittest.TestCase):
    """Test pix2ang_eq function."""

    def test_pix2ang_eq(self):
        """Check dec=90deg for first pix and -90 for last."""
        nside = 8
        resolution = np.degrees(hp.nside2resol(nside))  # pylint: disable=E1111
        dec, _ = np.degrees(pix2ang_eq(nside, 0))  # pylint: disable=E1111
        self.assertTrue(np.abs(dec - 90) < resolution)
        dec, _ = np.degrees(pix2ang_eq(nside, hp.nside2npix(nside) - 1))  # pylint: disable=E1111
        self.assertTrue(np.abs(dec - (-90)) < resolution)


class TestAng2pixEq(unittest.TestCase):
    """Test ang2pix_eq function."""

    def test_comparison_ang2pix_to_pix2ang(self):
        """Check convertion from ang to pix to ang is coherent and vice versa."""
        nside = 8
        resolution = hp.nside2resol(nside)

        dec0 = np.radians([0, 89, 42, -48])  # pylint: disable=E1111
        ra0 = np.radians([0, 180, 359, 18])  # pylint: disable=E1111
        for i, _ in enumerate(dec0):
            dec1, ra1 = pix2ang_eq(nside, ang2pix_eq(nside, dec0[i], ra0[i]))
            self.assertTrue(np.abs(dec1 - dec0[i]) < resolution)
            self.assertTrue(np.abs(ra1 - ra0[i]) * math.cos(dec1) < resolution)

        ipix0 = list(range(hp.nside2npix(nside)))
        np.random.shuffle(ipix0)
        dec, ra = pix2ang_eq(nside, ipix0)
        ipix1 = ang2pix_eq(nside, dec, ra)
        self.assertListEqual(ipix0, list(ipix1))

    def test_value_error_dec(self):
        """Check that ra instead of dec or dec in degrees raises a ValueError."""
        nside = 8
        self.assertRaises(ValueError, ang2pix_eq, nside, 45, math.pi)
        self.assertRaises(ValueError, ang2pix_eq, nside, math.pi, math.pi / 4.)


class TestGetSkymapsOfDensities(unittest.TestCase):
    """Test get_skymaps_of_densities."""

    def test_get_skymaps_of_densities(self):
        """Inject events at given position, get skymaps of densities and check events are injected where they should"""
        dataset = 'ANTARES'
        opt = {'begin': pd.Timestamp('2019-12-01T00:00:00'),
               'end': pd.Timestamp('2019-12-09T00:00:00'),
               'timestep': pd.Timedelta(1, unit='d'),
               'n_jobs': 1,
               'job_id': 0,
               'nside': 8,
              }
        data = {'time': [pd.Timestamp('2019-12-01T13:41:04'),
                         pd.Timestamp('2019-12-02T13:41:04'),
                         pd.Timestamp('2019-12-03T13:41:04'),
                         pd.Timestamp('2019-12-04T13:41:04'),
                         pd.Timestamp('2019-12-05T13:41:04'),
                         pd.Timestamp('2019-12-06T13:41:04'),
                         pd.Timestamp('2019-12-07T13:41:04'),
                         pd.Timestamp('2019-12-08T13:41:04'), ],
                'dec_rad': np.radians([0, 0, 0, 45, 45, 45, -45, 89]),
                'ra_rad': np.radians([0, 180, 359, 0, 90, 180, 90, 90]),
                'src_err68': [1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1],
                'scaling_factor': [1., 1., 1., 1., 1., 1., 1., 1.],
               }
        dataframe = pd.DataFrame(data)
        skymaps = get_skymaps_of_densities(opt, dataset, dataframe)

        resolution = hp.nside2resol(opt['nside'])
        for skymap in skymaps.columns[2:12]:
            ipix = skymaps[skymap].idxmax()
            dec, ra = pix2ang_eq(opt['nside'], ipix)
            self.assertTrue(dec - data['dec_rad'][skymap] < resolution)
            self.assertTrue((ra - data['ra_rad'][skymap]) * math.cos(dec) < resolution)


class TestSplitToTrainTest(unittest.TestCase):
    """Test split_to_train_test"""

    def test_split_to_train_test(self):
        """Check that split_to_train_test output what is expected."""
        opt = {'train_prop': 1/3,
               'begin': pd.Timestamp('2019-12-01T00:00:00'),
               'end': pd.Timestamp('2019-12-04T00:00:00'),
               'timestep': pd.Timedelta(1, unit='d'),
               'nside': 1,
              }
        x_data = [[0.65025126, 0.51415094, 1., 0.86283366],
                  [0.10234506, 0.46698113, 0.06695469, 0.66377326],
                  [0.08157454, 0.47641509, 0., 0.13675501],
                  [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                  [0., 1., 0.78976885, 0.6888604],
                  [0.50636516, 0.56603774, 0.20981077, 1.],
                  [0.08174204, 0.5, 0.21018263, 0.31120966],
                  [1., 0.67924528, 0.78940626, 0.],
                  [0.02881072, 0., 0.57953727, 0.86330956],
                  [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                  [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                  [0.10234506, 0.46698113, 0.06695469, 0.66377326],
                  [0.08157454, 0.47641509, 0., 0.13675501],
                  [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                  [0., 1., 0.78976885, 0.6888604],
                  [0.50636516, 0.56603774, 0.20981077, 1.],
                  [0.08174204, 0.5, 0.21018263, 0.31120966],
                  [1., 0.67924528, 0.78940626, 0.],
                  [0.02881072, 0., 0.57953727, 0.86330956],
                  [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                  [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                  [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                  [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                  [0.08157454, 0.47641509, 0., 0.13675501],
                  [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                  [0., 1., 0.78976885, 0.6888604],
                  [0.50636516, 0.56603774, 0.20981077, 1.],
                  [0.08174204, 0.5, 0.21018263, 0.31120966],
                  [1., 0.67924528, 0.78940626, 0.],
                  [0.02881072, 0., 0.57953727, 0.86330956],
                  [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                  [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                  [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                  [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                  [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                  [0.0358459, 0.07075472, 0.64651169, 0.33623525], ]
        y_list = [1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0,
                  1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0,
                  1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0]

        x_train, x_test = split_to_train_test(opt, x_data)
        y_train, y_test = split_to_train_test(opt, y_list)
        x_train2 = [[0.81675042, 0.02830189, 0.42037022, 0.13721684],
                    [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                    [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                    [1., 0.67924528, 0.78940626, 0.],
                    [0.10234506, 0.46698113, 0.06695469, 0.66377326],
                    [0., 1., 0.78976885, 0.6888604],
                    [0.02881072, 0., 0.57953727, 0.86330956],
                    [0.50636516, 0.56603774, 0.20981077, 1.],
                    [0.65025126, 0.51415094, 1., 0.86283366],
                    [0.10234506, 0.46698113, 0.06695469, 0.66377326],
                    [0.08157454, 0.47641509, 0., 0.13675501],
                    [0.08174204, 0.5, 0.21018263, 0.31120966],]
        x_test2 = [[0.08157454, 0.47641509, 0., 0.13675501],
                   [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                   [0., 1., 0.78976885, 0.6888604],
                   [0.50636516, 0.56603774, 0.20981077, 1.],
                   [0.08174204, 0.5, 0.21018263, 0.31120966],
                   [1., 0.67924528, 0.78940626, 0.],
                   [0.02881072, 0., 0.57953727, 0.86330956],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.08157454, 0.47641509, 0., 0.13675501],
                   [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                   [0., 1., 0.78976885, 0.6888604],
                   [0.50636516, 0.56603774, 0.20981077, 1.],
                   [0.08174204, 0.5, 0.21018263, 0.31120966],
                   [1., 0.67924528, 0.78940626, 0.],
                   [0.02881072, 0., 0.57953727, 0.86330956],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.0358459, 0.07075472, 0.64651169, 0.33623525]]
        self.assertTrue((np.array(x_train) == np.array(x_train2)).all())
        self.assertListEqual(list(y_train), [1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0])
        self.assertTrue((np.array(x_test) == np.array(x_test2)).all())
        self.assertListEqual(list(y_test), [1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0])

        opt = {'train_prop': 0.3,
               'begin': pd.Timestamp('2019-12-01T00:00:00'),
               'end': pd.Timestamp('2019-12-04T00:00:00'),
               'timestep': pd.Timedelta(1, unit='d'),
               'nside': 1,
              }
        x_train, x_test = split_to_train_test(opt, x_data)
        y_train, y_test = split_to_train_test(opt, y_list)
        x_train2 = [[0.02881072, 0., 0.57953727, 0.86330956],
                    [0.08174204, 0.5, 0.21018263, 0.31120966],
                    [0., 1., 0.78976885, 0.6888604],
                    [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                    [0.50636516, 0.56603774, 0.20981077, 1.],
                    [0.10234506, 0.46698113, 0.06695469, 0.66377326],
                    [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                    [0.08157454, 0.47641509, 0., 0.13675501],
                    [0.10234506, 0.46698113, 0.06695469, 0.66377326],
                    [0.65025126, 0.51415094, 1., 0.86283366],]
        x_test2 = [[0.08157454, 0.47641509, 0., 0.13675501],
                   [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                   [0., 1., 0.78976885, 0.6888604],
                   [0.50636516, 0.56603774, 0.20981077, 1.],
                   [0.08174204, 0.5, 0.21018263, 0.31120966],
                   [1., 0.67924528, 0.78940626, 0.],
                   [0.02881072, 0., 0.57953727, 0.86330956],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.08157454, 0.47641509, 0., 0.13675501],
                   [0.86582918, 0.9009434, 0.93287676, 0.33576955],
                   [0., 1., 0.78976885, 0.6888604],
                   [0.50636516, 0.56603774, 0.20981077, 1.],
                   [0.08174204, 0.5, 0.21018263, 0.31120966],
                   [1., 0.67924528, 0.78940626, 0.],
                   [0.02881072, 0., 0.57953727, 0.86330956],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.78927973, 0.05660377, 0.35326475, 0.66424525],
                   [0.81675042, 0.02830189, 0.42037022, 0.13721684],
                   [0.0358459, 0.07075472, 0.64651169, 0.33623525],]
        self.assertTrue((np.array(x_train) == np.array(x_train2)).all())
        self.assertListEqual(list(y_train), [0, 0, 1, 1, 1, 0, 0, 1, 0, 1])
        self.assertTrue((np.array(x_test) == np.array(x_test2)).all())
        self.assertListEqual(list(y_test), [1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0])

class TestGetRandomTimes(unittest.TestCase):
    """Test get_random_times"""
    def test_get_random_times(self):
        """Check get_random_times gives output between begin and end."""
        begin = pd.Timestamp('2019-12-01T00:00:00')
        end = pd.Timestamp('2019-12-09T00:00:00')
        rdm_times = get_random_times(begin, end, 20)
        self.assertTrue((rdm_times > begin).all())
        self.assertTrue((rdm_times < end).all())

class TestAllCombinations(unittest.TestCase):
    """Test all_combinations"""
    def test_all_combinations(self):
        """Check all_combinations gives expected output."""
        combs = [[0, 1, 2, 3],
                 [1, 2, 3], [0, 2, 3], [0, 1, 3], [0, 1, 2],
                 [2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1]]
        self.assertListEqual(all_combinations(range(4)), combs)

        combs = [[2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1]]
        self.assertListEqual(all_combinations(range(4), 2), combs)

        combs = [[1, 2, 3], [0, 2, 3], [0, 1, 3], [0, 1, 2],]
        self.assertListEqual(all_combinations(range(4), 3), combs)

        combs = [[0, 1, 2, 3]]
        self.assertListEqual(all_combinations(range(4), 4), combs)

        combs = [[0, 1, 2, 3, 4, 5, 6, 7]]
        self.assertListEqual(all_combinations(range(8), 8), combs)

class TestAllCombinationsStr(unittest.TestCase):
    """Test all_combinations_str"""
    def test_all_combinations_str(self):
        """Check all_combinations gives expected output."""
        dataset_list = ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube', 'HAWC_bursts']
        combs = ['Fermi+ANT+HAWC_h+IC+HAWC_b',
                 'ANT+HAWC_h+IC+HAWC_b', 'Fermi+HAWC_h+IC+HAWC_b', 'Fermi+ANT+IC+HAWC_b', 'Fermi+ANT+HAWC_h+HAWC_b', 'Fermi+ANT+HAWC_h+IC',
                 'HAWC_h+IC+HAWC_b', 'ANT+IC+HAWC_b', 'ANT+HAWC_h+HAWC_b', 'ANT+HAWC_h+IC', 'Fermi+IC+HAWC_b', 'Fermi+HAWC_h+HAWC_b', 'Fermi+HAWC_h+IC', 'Fermi+ANT+HAWC_b', 'Fermi+ANT+IC', 'Fermi+ANT+HAWC_h',
                 'IC+HAWC_b', 'HAWC_h+HAWC_b', 'HAWC_h+IC', 'ANT+HAWC_b', 'ANT+IC', 'ANT+HAWC_h', 'Fermi+HAWC_b', 'Fermi+IC', 'Fermi+HAWC_h', 'Fermi+ANT']
        self.assertListEqual(all_combinations_str(dataset_list, 'all'), combs)

        combs = ['IC+HAWC_b', 'HAWC_h+HAWC_b', 'HAWC_h+IC', 'ANT+HAWC_b', 'ANT+IC', 'ANT+HAWC_h', 'Fermi+HAWC_b', 'Fermi+IC', 'Fermi+HAWC_h', 'Fermi+ANT']
        self.assertListEqual(all_combinations_str(dataset_list, 2), combs)

        combs = ['HAWC_h+IC+HAWC_b', 'ANT+IC+HAWC_b', 'ANT+HAWC_h+HAWC_b', 'ANT+HAWC_h+IC', 'Fermi+IC+HAWC_b', 'Fermi+HAWC_h+HAWC_b', 'Fermi+HAWC_h+IC', 'Fermi+ANT+HAWC_b', 'Fermi+ANT+IC', 'Fermi+ANT+HAWC_h']
        self.assertListEqual(all_combinations_str(dataset_list, 3), combs)

        combs = ['ANT+HAWC_h+IC+HAWC_b', 'Fermi+HAWC_h+IC+HAWC_b', 'Fermi+ANT+IC+HAWC_b', 'Fermi+ANT+HAWC_h+HAWC_b', 'Fermi+ANT+HAWC_h+IC',]
        self.assertListEqual(all_combinations_str(dataset_list, 4), combs)

        combs = ['Fermi+ANT+HAWC_h+IC+HAWC_b',]
        self.assertListEqual(all_combinations_str(dataset_list, 5), combs)

        dataset_list = ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube']
        combs = ['ANT+HAWC_h+IC', 'Fermi+HAWC_h+IC', 'Fermi+ANT+IC', 'Fermi+ANT+HAWC_h']
        self.assertListEqual(all_combinations_str(dataset_list, 3), combs)

class TestCombsToColors(unittest.TestCase):
    """Test combs_to_colors"""
    def test_combs_to_colors(self):
        """Check combs_to_colors gives expected output."""
        dataset_list = ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube', 'HAWC_bursts']
        combs2col = {'Fermi+ANT+HAWC_h+IC+HAWC_b': 'xkcd:purple', 'ANT+HAWC_h+IC+HAWC_b': 'xkcd:green', 'Fermi+HAWC_h+IC+HAWC_b': 'xkcd:blue', 'Fermi+ANT+IC+HAWC_b': 'xkcd:pink', 'Fermi+ANT+HAWC_h+HAWC_b': 'xkcd:brown',
                     'Fermi+ANT+HAWC_h+IC': 'xkcd:light blue', 'HAWC_h+IC+HAWC_b': 'xkcd:orange', 'ANT+IC+HAWC_b': 'xkcd:yellow', 'ANT+HAWC_h+HAWC_b': 'xkcd:grey', 'ANT+HAWC_h+IC': 'xkcd:dark green',
                     'Fermi+IC+HAWC_b': 'xkcd:turquoise', 'Fermi+HAWC_h+HAWC_b': 'xkcd:lavender', 'Fermi+HAWC_h+IC': 'xkcd:dark blue', 'Fermi+ANT+HAWC_b': 'xkcd:tan', 'Fermi+ANT+IC': 'xkcd:cyan',
                     'Fermi+ANT+HAWC_h': 'xkcd:aqua', 'IC+HAWC_b': 'xkcd:forest green', 'HAWC_h+HAWC_b': 'xkcd:mauve', 'HAWC_h+IC': 'xkcd:dark purple', 'ANT+HAWC_b': 'xkcd:bright green', 'ANT+IC': 'xkcd:maroon',
                     'ANT+HAWC_h': 'xkcd:olive', 'Fermi+HAWC_b': 'xkcd:salmon', 'Fermi+IC': 'xkcd:beige', 'Fermi+HAWC_h': 'xkcd:royal blue', 'Fermi+ANT': 'xkcd:navy blue'}
        self.assertDictEqual(combs_to_colors(dataset_list, 'all'), combs2col)

        combs2col = {'IC+HAWC_b': 'xkcd:purple', 'HAWC_h+HAWC_b': 'xkcd:green', 'HAWC_h+IC': 'xkcd:blue', 'ANT+HAWC_b': 'xkcd:pink', 'ANT+IC': 'xkcd:brown', 'ANT+HAWC_h': 'xkcd:light blue',
                     'Fermi+HAWC_b': 'xkcd:orange', 'Fermi+IC': 'xkcd:yellow', 'Fermi+HAWC_h': 'xkcd:grey', 'Fermi+ANT': 'xkcd:dark green'}
        self.assertDictEqual(combs_to_colors(dataset_list, 2), combs2col)

        combs2col = {'HAWC_h+IC+HAWC_b': 'xkcd:purple', 'ANT+IC+HAWC_b': 'xkcd:green', 'ANT+HAWC_h+HAWC_b': 'xkcd:blue', 'ANT+HAWC_h+IC': 'xkcd:pink', 'Fermi+IC+HAWC_b': 'xkcd:brown',
                     'Fermi+HAWC_h+HAWC_b': 'xkcd:light blue', 'Fermi+HAWC_h+IC': 'xkcd:orange', 'Fermi+ANT+HAWC_b': 'xkcd:yellow', 'Fermi+ANT+IC': 'xkcd:grey', 'Fermi+ANT+HAWC_h': 'xkcd:dark green'}
        self.assertDictEqual(combs_to_colors(dataset_list, 3), combs2col)

        combs2col = {'ANT+HAWC_h+IC+HAWC_b': 'xkcd:purple', 'Fermi+HAWC_h+IC+HAWC_b': 'xkcd:green', 'Fermi+ANT+IC+HAWC_b': 'xkcd:blue', 'Fermi+ANT+HAWC_h+HAWC_b': 'xkcd:pink', 'Fermi+ANT+HAWC_h+IC': 'xkcd:brown'}
        self.assertDictEqual(combs_to_colors(dataset_list, 4), combs2col)

        combs2col = {'Fermi+ANT+HAWC_h+IC+HAWC_b': 'xkcd:purple',}
        self.assertDictEqual(combs_to_colors(dataset_list, 5), combs2col)

        dataset_list = ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube']
        combs2col = {'ANT+HAWC_h+IC': 'xkcd:purple', 'Fermi+HAWC_h+IC': 'xkcd:green', 'Fermi+ANT+IC': 'xkcd:blue', 'Fermi+ANT+HAWC_h': 'xkcd:pink'}
        self.assertDictEqual(combs_to_colors(dataset_list, 3), combs2col)

class TestGetRandomSourceDf(unittest.TestCase):
    """Test get_random_source_df"""
    def test_get_random_source_df(self):
        """Check get_random_source_df gives output expected"""
        begin = pd.Timestamp('2019-12-01T00:00:00')
        end = pd.Timestamp('2019-12-09T00:00:00')
        rdm_src = get_random_source_df(20, begin, end)
        self.assertTrue((rdm_src.time > begin).all())
        self.assertTrue((rdm_src.time < end).all())
        self.assertTrue((rdm_src.ra > 0).all())
        self.assertTrue((rdm_src.ra < 360).all())
        self.assertTrue((rdm_src.dec > -90).all())
        self.assertTrue((rdm_src.dec < 90).all())

class TestGetRandomSrcErr68(unittest.TestCase):
    """Test get_random_src_err68"""
    def test_get_random_src_err68(self):
        """Check random src err68 are between min and max of data."""
        begin = pd.Timestamp('2019-12-01T00:00:00')
        end = pd.Timestamp('2019-12-04T00:00:00')
        rdm_src_df = get_random_source_df(20, begin, end)
        dataset_list = ['ANTARES', 'IceCube_dummy', 'HAWC_bursts', 'HAWC_hotspots', 'Fermi_dummy']
        rdm_src_df = add_all_local_coords(dataset_list, rdm_src_df)

        for dataset in dataset_list:
            dataframe = get_data(dataset, begin, end)
            dataframe = assign_src_err68(dataset, dataframe)
            min_src_err68 = dataframe.src_err68.min()
            max_src_err68 = dataframe.src_err68.max()
            for evt in range(len(rdm_src_df)):
                rdm_src_err68 = get_random_src_err68(dataset, rdm_src_df.loc[evt])
                self.assertTrue(rdm_src_err68 >= min_src_err68)
                self.assertTrue(rdm_src_err68 <= max_src_err68)

class TestGetEmptySigDfs(unittest.TestCase):
    """Test get_empty_sig_dfs"""
    def test_get_empty_sig_dfs(self):
        """Check get_empty_sig_dfs columns correspond to dataset columns"""
        dataset_list = ['ANTARES', 'IceCube_dummy', 'HAWC_bursts', 'HAWC_hotspots', 'Fermi_dummy']
        begin = pd.Timestamp('2019-12-01T00:00:00')
        end = pd.Timestamp('2019-12-04T00:00:00')
        sig_dfs = get_empty_sig_dfs(dataset_list)
        for dataset in dataset_list:
            dataframe = get_data(dataset, begin, end)
            dataframe = assign_src_err68(dataset, dataframe)
            dataframe = remove_useless_columns(dataset, dataframe)
            dataframe_cols = list(dataframe.columns)
            if 'id' in dataframe_cols:
                dataframe_cols.remove('id')
            self.assertListEqual(sorted(dataframe_cols), sorted(list(sig_dfs[dataset].columns)))

class TestAddAllLocalCoords(unittest.TestCase):
    """Test add_all_local_coords"""
    def test_add_all_local_coords(self):
        """Check column names added"""
        dataset_list = ['ANTARES', 'IceCube_dummy', 'HAWC_bursts', 'HAWC_hotspots']
        begin = pd.Timestamp('2019-12-01T00:00:00')
        end = pd.Timestamp('2019-12-04T00:00:00')
        rdm_src_df = get_random_source_df(20, begin, end)
        rdm_src_df = add_all_local_coords(dataset_list, rdm_src_df)
        for dataset in dataset_list:
            for coord in ['alt', 'az']:
                self.assertTrue(dataset+'-'+coord in rdm_src_df.columns)

class TestGetY(unittest.TestCase):
    """Test get_y"""
    def test_get_y(self):
        """Check get_y outputs as expected"""
        opt = {'begin': pd.Timestamp('2019-12-01T00:00:00'),
               'end': pd.Timestamp('2019-12-04T00:00:00'),
               'nside': 1,
               'timestep': pd.Timedelta(1, unit='d'),
              }

        data = {'time': [pd.Timestamp('2019-12-01T00:00:01'), pd.Timestamp('2019-12-02T00:00:01'), pd.Timestamp('2019-12-03T00:00:01')],
                'ra': [0, 75, 340],
                'dec': [89, 0, -30],}
        dataframe = pd.DataFrame(data)
        y_check = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
        self.assertListEqual(list(get_y(opt, dataframe)), y_check)

        y_check = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.assertListEqual(list(get_y(opt, None)), y_check)

    def test_two_coinc_in_one_pix(self):
        """Check a Warning is printed and the y values are set to 1 when more than one signal in the same pixel."""
        opt = {'begin': pd.Timestamp('2019-12-01T00:00:00'),
               'end': pd.Timestamp('2019-12-04T00:00:00'),
               'nside': 1,
               'timestep': pd.Timedelta(1, unit='d'),
              }
        data = {'time': [pd.Timestamp('2019-12-02T00:00:01'), pd.Timestamp('2019-12-02T00:00:01'), pd.Timestamp('2019-12-02T00:00:01'), pd.Timestamp('2019-12-02T00:00:01')],
                'ra': [75, 75, 75, 75],
                'dec': [0, 0, 0, 0],}
        dataframe = pd.DataFrame(data)

        captured_output = io.StringIO()
        sys.stdout = captured_output
        y_list = list(get_y(opt, dataframe))
        expected_output = "Warning: two coincidences are on the same pixel at the same datetime.\n"\
                          "Be careful, when two coincidences are in adjacent pixels this warning won't show.\n"\
                          "num coinc in pix   begin time            time index   ra       dec      \n"\
                          "4                  12-02-2019 00:00:00   1            90.0     0.0      \n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

        y_check = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.assertListEqual(y_list, y_check)

class TestSaveObj(unittest.TestCase):
    """Test save_obj and load_obj at the same time."""
    def test_save_obj(self):
        """Save and load something. Check verbose=True print as expected."""
        test = [1, 2]
        path = os.path.join(OUTPUT_PATH, 'test.pkl')

        captured_output = io.StringIO()
        sys.stdout = captured_output
        save_obj(test, path, verbose=True)
        expected_output = "test.pkl file written.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

        test2 = load_obj(path)
        self.assertListEqual(test, test2)

class TestOutputDir(unittest.TestCase):
    """Test output_dir"""
    def test_output_dir(self):
        """Check output corresponds to what is expected."""
        opt = {'write_files': 2, # 0 don't write don't load, 1 write if they don't exist else load, 2 always write, even if already exist
               'file_suffix': 'suffixTest',
               'n_Hb_bkg_inj': 0,
               'n_ANT_bkg_inj': 0,
               'n_src_per_month': 15,
               'n_comb_inj': 2, # int or 'all'
               'sig_in_train': True,
               'train_prop': 0.5,
               'begin': pd.Timestamp('2019-12-01 00:00:00'),
               'end': pd.Timestamp('2020-02-29 00:00:00'),
               'timestep': pd.Timedelta(30, unit='d'),
               'nside': 4,
               'clf_name': 'KNN',
               'job_events_df': True,
               'job_skymaps': True,
               'job_X': True,
               'job_clf': True,
               'job_predictions': True,
               'visualize': True,
               'n_jobs': 1,
               'job_id': 0,
               'dataset_list': ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube', 'HAWC_bursts'],
              }
        outdir = '90d_per720h_nside4_15srcPerMonth_2combInj_50%trainProp_AFHbHhI_sigInTrain_suffixTest'
        self.assertEqual(output_dir(opt), outdir)

        opt = {'write_files': 2, # 0 don't write don't load, 1 write if they don't exist else load, 2 always write, even if already exist
               'file_suffix': 'suffixTest',
               'n_Hb_bkg_inj': 0,
               'n_ANT_bkg_inj': 0,
               'n_src_per_month': 0,
               'n_comb_inj': ['Fermi', 'HAWC_hotspots', 'IceCube'], # int or 'all'
               'sig_in_train': True,
               'train_prop': 0.005,
               'begin': pd.Timestamp('2019-12-01 00:00:00'),
               'end': pd.Timestamp('2020-02-29 00:00:00'),
               'timestep': pd.Timedelta(30, unit='d'),
               'nside': 4,
               'clf_name': 'KNN',
               'job_events_df': True,
               'job_skymaps': True,
               'job_X': True,
               'job_clf': True,
               'job_predictions': True,
               'visualize': True,
               'n_jobs': 1,
               'job_id': 0,
               'dataset_list': ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube', 'HAWC_bursts'],
              }
        outdir = '90d_per720h_nside4_0srcPerMonth_FHhIcombInj_0.5%trainProp_AFHbHhI_sigInTrain_NoSig_suffixTest'
        self.assertEqual(output_dir(opt), outdir)


class TestExistingDataDirs(unittest.TestCase):
    """Test existing_output_dirs"""
    def test_existing_output_dirs(self):
        """Check output is a list of strings with expected structure and printed output is the same."""
        captured_output = io.StringIO()
        sys.stdout = captured_output
        directories = existing_output_dirs(verbose=True)
        directories_print = captured_output.getvalue().split('\n')[:-1]
        self.assertListEqual(directories_print, directories)
        sys.stdout = sys.__stdout__

        self.assertTrue(all(['d_per' in directory for directory in directories]))
        self.assertTrue(all(['h_nside' in directory for directory in directories]))
        self.assertTrue(all(['srcPerMonth_' in directory for directory in directories]))
        self.assertTrue(all(['combInj_' in directory for directory in directories]))
        self.assertTrue(all(['%trainProp_' in directory for directory in directories]))

class TestGetOptFromArgs(unittest.TestCase):
    """Test get_opt_from_args"""
    def test_get_opt_from_args(self):
        """Check output opt are as expected."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '30',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '2',
                '<datasets>': []}
        opt = {'all_datasets': True,
               'n_Hb_bkg_inj': 0,
               'n_ANT_bkg_inj': 0,
               'begin': pd.Timestamp('2019-12-01 00:00:00'),
               'clf_name': 'KNN',
               'dataset_list': ['ANTARES',
                                'Fermi',
                                'HAWC_bursts',
                                'HAWC_hotspots',
                                'IceCube'],
               'end': pd.Timestamp('2020-02-29 00:00:00'),
               'file_suffix': '',
               'job_X': False,
               'job_clf': False,
               'job_events_df': False,
               'job_id': 0,
               'job_predictions': False,
               'job_skymaps': False,
               'n_comb_inj': 3,
               'n_jobs': 1,
               'n_src_per_month': 30,
               'nside': 8,
               'out_path': os.path.join(OUTPUT_PATH, '90d_per720h_nside8_30srcPerMonth_3combInj_1%trainProp_AFHbHhI'),
               'sig_in_train': False,
               'timestep': pd.Timedelta('30 days 00:00:00'),
               'train_prop': 0.01,
               'visualize': True,
               'write_files': 2}
        self.assertDictEqual(get_opt_from_args(args), opt)

        args = {'--all_datasets': False,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '30',
                '--train_prop': '0.01',
                '--visualize': False,
                '--write_files': '2',
                '<datasets>': []}
        opt = {'all_datasets': False,
               'n_Hb_bkg_inj': 0,
               'n_ANT_bkg_inj': 0,
               'begin': pd.Timestamp('2019-12-01 00:00:00'),
               'clf_name': 'KNN',
               'dataset_list': ['ANTARES',
                                'HAWC_hotspots',
                                'HAWC_bursts'],
               'end': pd.Timestamp('2020-02-29 00:00:00'),
               'file_suffix': '',
               'job_X': True,
               'job_clf': True,
               'job_events_df': True,
               'job_id': 0,
               'job_predictions': True,
               'job_skymaps': True,
               'n_comb_inj': 3,
               'n_jobs': 1,
               'n_src_per_month': 30,
               'nside': 8,
               'out_path': os.path.join(OUTPUT_PATH, '90d_per720h_nside8_30srcPerMonth_3combInj_1%trainProp_AHbHh'),
               'sig_in_train': False,
               'timestep': pd.Timedelta('30 days 00:00:00'),
               'train_prop': 0.01,
               'visualize': True,
               'write_files': 2}
        self.assertDictEqual(get_opt_from_args(args), opt)

    def test_error_datasets(self):
        """Check ValueError is raised if --all_datasets is set at the same time as <datasets> options."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '30',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '2',
                '<datasets>': ['ANTARES', 'IceCube']}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_write_files(self):
        """Check ValueError is raised if --write_files has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '30',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '4',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '30',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '-1',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_job_id(self):
        """Check ValueError is raised if --job_id has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '-1',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '4',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '4',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_timestep(self):
        """Check ValueError is raised if --timestep has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '1',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '0',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '4',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '0',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_n_comb_inj(self):
        """Check ValueError is raised if --n_comb_inj has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '8',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '-1',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_n_jobs(self):
        """Check ValueError is raised if --n_jobs has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '2',
                '--n_jobs': '-1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_n_src_per_month(self):
        """Check ValueError is raised if --n_scr_per_month has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '2',
                '--n_jobs': '1',
                '--n_src_per_month': '-1',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_nside(self):
        """Check ValueError is raised if --nside has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '2',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '-8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_error_train_prop(self):
        """Check ValueError is raised if --train_prop has wrong value."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '2',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '0.',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '2',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '1.1',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '2',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '1',
                '--train_prop': '-0.1',
                '--visualize': True,
                '--write_files': '0',
                '<datasets>': []}
        self.assertRaises(ValueError, get_opt_from_args, args)

    def test_print(self):
        """Check verbose=True prints as expected."""
        args = {'--all_datasets': True,
                '--n_Hb_bkg_inj': '0',
                '--n_ANT_bkg_inj': '0',
                '--begin': '2019-12-01T00:00:00',
                '--clf_name': 'KNN',
                '--end': '2020-02-29T00:00:00',
                '--file_suffix': '',
                '--help': False,
                '--job_X': False,
                '--job_clf': False,
                '--job_events_df': False,
                '--job_id': '0',
                '--job_predictions': False,
                '--job_skymaps': False,
                '--n_comb_inj': '3',
                '--n_jobs': '1',
                '--n_src_per_month': '30',
                '--nside': '8',
                '--sig_in_train': False,
                '--timestep': '30',
                '--train_prop': '0.01',
                '--visualize': True,
                '--write_files': '2',
                '<datasets>': []}

        captured_output = io.StringIO()
        sys.stdout = captured_output
        get_opt_from_args(args, verbose=True)
        expected_output = """{'all_datasets': True,
 'begin': Timestamp('2019-12-01 00:00:00'),
 'clf_name': 'KNN',
 'dataset_list': ['ANTARES',
                  'Fermi',
                  'HAWC_bursts',
                  'HAWC_hotspots',
                  'IceCube'],
 'end': Timestamp('2020-02-29 00:00:00'),
 'file_suffix': '',
 'job_X': False,
 'job_clf': False,
 'job_events_df': False,
 'job_id': 0,
 'job_predictions': False,
 'job_skymaps': False,
 'n_ANT_bkg_inj': 0,
 'n_Hb_bkg_inj': 0,
 'n_comb_inj': 3,
 'n_jobs': 1,
 'n_src_per_month': 30,
 'nside': 8,
 'out_path': '/home/timothee/code/OutlierDetection/output/90d_per720h_nside8_30srcPerMonth_3combInj_1%trainProp_AFHbHhI',
 'sig_in_train': False,
 'timestep': Timedelta('30 days 00:00:00'),
 'train_prop': 0.01,
 'visualize': True,
 'write_files': 2}
"""
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

class TestGetSigInjBeginTime(unittest.TestCase):
    """Test get_sig_inj_begin_time"""
    def test_get_sig_inj_begin_time(self):
        """Check output is as expected."""
        opt = {'begin': pd.Timestamp('2019-12-01 00:00:00'),
               'end': pd.Timestamp('2019-12-21 00:00:00'),
               'sig_in_train': False,
               'timestep': pd.Timedelta('2 days 00:00:00'),
               'train_prop': 0.1,}
        self.assertEqual(get_sig_inj_begin_time(opt), pd.Timestamp('2019-12-09 00:00:00'))

class TestGetInjectionContamination(unittest.TestCase):
    """Test get_injection_contamination."""
    def test_get_injection_contam(self):
        """Check output is as expected."""
        opt = {'nside': 8,
               'timestep': pd.Timedelta('1 days 00:00:00'),
              }
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        self.assertEqual(get_injection_contamination(opt, 768, begin, end, verbose=False), 0.05)

    def test_value_error(self):
        """Check ValueError is raised if n_src is too high (more than 50% of datapoints) or too low (negative)."""
        opt = {'nside': 8,
               'timestep': pd.Timedelta('1 days 00:00:00'),}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        self.assertRaises(ValueError, get_injection_contamination, opt, 8000, begin, end, False)

        opt = {'nside': 8,
               'timestep': pd.Timedelta('1 days 00:00:00'),}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        self.assertRaises(ValueError, get_injection_contamination, opt, -768, begin, end, False)

    def test_verbose_print(self):
        """Check print output as expected."""
        opt = {'nside': 8,
               'timestep': pd.Timedelta('1 days 00:00:00'),}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')

        captured_output = io.StringIO()
        sys.stdout = captured_output
        get_injection_contamination(opt, 768, begin, end, verbose=True)
        expected_output = "768 sources to inject. Contamination: 5.0%\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

class TestGetCombList(unittest.TestCase):
    """Test get_comb_list."""
    def test_get_comb_list(self):
        """Check output is as expected."""
        opt = {'dataset_list': ['ANTARES',
                                'HAWC_hotspots',
                                'HAWC_bursts'],
               'n_comb_inj': 2,
               'n_src_per_month': 30,}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        # NB We want as many combinations of each type so the number of combinations does not exactly correspond to n_src_per_month
        list_comb = [[1, 2], [0, 2], [0, 1], [1, 2], [0, 2], [0, 1], [1, 2], [0, 2], [0, 1],
                     [1, 2], [0, 2], [0, 1], [1, 2], [0, 2], [0, 1], [1, 2], [0, 2], [0, 1]]
        self.assertListEqual(get_comb_list(opt, begin, end), list_comb)
        self.assertTrue(len(get_comb_list(opt, begin, end)) > 20-3 and len(get_comb_list(opt, begin, end)) <= 20)

        opt = {'dataset_list': ['ANTARES',
                                'HAWC_hotspots',
                                'HAWC_bursts',
                                'IceCube'],
               'n_comb_inj': 2,
               'n_src_per_month': 60,}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        # NB We want as many combinations of each type so the number of combinations does not exactly correspond to n_src_per_month
        list_comb = [[2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1], [2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1],
                     [2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1], [2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1],
                     [2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1], [2, 3], [1, 3], [1, 2], [0, 3], [0, 2], [0, 1]]
        self.assertListEqual(get_comb_list(opt, begin, end), list_comb)
        self.assertTrue(len(get_comb_list(opt, begin, end)) > 40-6 and len(get_comb_list(opt, begin, end)) <= 40)

        opt = {'dataset_list': ['ANTARES',
                                'HAWC_bursts',
                                'IceCube'],
               'n_comb_inj': 'all',
               'n_src_per_month': 60,}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        # NB We want as many combinations of each type so the number of combinations does not exactly correspond to n_src_per_month
        list_comb = [[0, 1, 2], [1, 2], [0, 2], [0, 1], [0, 1, 2], [1, 2], [0, 2], [0, 1],
                     [0, 1, 2], [1, 2], [0, 2], [0, 1], [0, 1, 2], [1, 2], [0, 2], [0, 1],
                     [0, 1, 2], [1, 2], [0, 2], [0, 1], [0, 1, 2], [1, 2], [0, 2], [0, 1],
                     [0, 1, 2], [1, 2], [0, 2], [0, 1], [0, 1, 2], [1, 2], [0, 2], [0, 1],
                     [0, 1, 2], [1, 2], [0, 2], [0, 1], [0, 1, 2], [1, 2], [0, 2], [0, 1]]
        self.assertListEqual(get_comb_list(opt, begin, end), list_comb)
        self.assertTrue(len(get_comb_list(opt, begin, end)) > 40-4 and len(get_comb_list(opt, begin, end)) <= 40)

    def test_value_error(self):
        """Check ValueError is raised if n_comb_inj > len(dataset_list)"""
        opt = {'dataset_list': ['ANTARES',
                                'HAWC_hotspots',
                                'HAWC_bursts'],
               'n_comb_inj': 5,
               'n_src_per_month': 30,}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')
        self.assertRaises(ValueError, get_comb_list, opt, begin, end)

    def test_warning(self):
        """Check that a warning is printed if n_src_per_month is too small compared to time window."""
        opt = {'dataset_list': ['ANTARES',
                                'HAWC_hotspots',
                                'HAWC_bursts'],
               'n_comb_inj': 2,
               'n_src_per_month': 1,}
        begin = pd.Timestamp('2019-12-01 00:00:00')
        end = pd.Timestamp('2019-12-21 00:00:00')

        captured_output = io.StringIO()
        sys.stdout = captured_output
        get_comb_list(opt, begin, end)
        expected_output = "Warning: Injecting minimum number of sources: 3\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

class TestGetNToInj(unittest.TestCase):
    """Test get_n_to_inj"""
    def test_get_n_to_inj(self):
        """Check output is strictly positive."""
        for dataset in ['Fermi', 'ANTARES', 'HAWC_hotspots', 'IceCube', 'HAWC_bursts']:
            self.assertTrue(get_n_to_inj(dataset) > 0)

class TestTypeOfScript(unittest.TestCase):
    """Test type_of_script."""
    def test_type_of_script(self):
        """Check type_of_script ouputs 'terminal' as expected if it is run from a terminal (and not in jupyter)."""
        self.assertEqual(type_of_script(), 'terminal') # If test_data_prep is run in a terminal is should output 'terminal'

class TestGetTwoDimToVisualize(unittest.TestCase):
    """Test get_two_dim_to_visualize."""
    def test_get_two_dim_to_visualize(self):
        """Check output is as expected."""
        x_train = np.array([['-1', '-2', '-3'], ['-4', '-5', '-6'], ['-7', '-8', '-9']])
        x_test = np.array([[-1, -2, -3], [-4, -5, -6], [-7, -8, -9], [-10, -11, -12]])

        x_train01, x_test01 = (get_two_dim_to_visualize(x_train, x_test, 0, 1))
        self.assertTrue((x_train01 == np.array([['-1', '-2'], ['-4', '-5'], ['-7', '-8']])).all())
        self.assertTrue((x_test01 == np.array([[-1, -2], [-4, -5], [-7, -8], [-10, -11]])).all())

        x_train10, x_test10 = (get_two_dim_to_visualize(x_train, x_test, 1, 0))
        self.assertTrue((x_train10 == np.array([['-2', '-1'], ['-5', '-4'], ['-8', '-7']])).all())
        self.assertTrue((x_test10 == np.array([[-2, -1], [-5, -4], [-8, -7], [-11, -10]])).all())

        x_train12, x_test12 = (get_two_dim_to_visualize(x_train, x_test, 1, 2))
        self.assertTrue((x_train12 == np.array([['-2', '-3'], ['-5', '-6'], ['-8', '-9']])).all())
        self.assertTrue((x_test12 == np.array([[-2, -3], [-5, -6], [-8, -9], [-11, -12]])).all())

        x_trainnn, x_testnn = (get_two_dim_to_visualize(x_train, x_test))
        self.assertTrue((x_trainnn == np.array([['-1', '0.0'], ['-4', '0.0'], ['-7', '0.0']])).all())
        self.assertTrue((x_testnn == np.array([[-1, 0], [-4, 0], [-7, 0], [-10, 0]])).all())

        x_train = np.array([['-1', '-2', '-3', '-4'], ['-4', '-5', '-6', '-7'], ['-7', '-8', '-9', '-10']])
        x_test = np.array([[-1, -2, -3, -4], [-4, -5, -6, -7], [-7, -8, -9, -10], [-10, -11, -12, -13]])

        x_train01, x_test01 = (get_two_dim_to_visualize(x_train, x_test, 0, 1))
        self.assertTrue((x_train01 == np.array([['-1', '-2'], ['-4', '-5'], ['-7', '-8']])).all())
        self.assertTrue((x_test01 == np.array([[-1, -2], [-4, -5], [-7, -8], [-10, -11]])).all())

        x_trainnn, x_testnn = (get_two_dim_to_visualize(x_train, x_test))
        self.assertTrue((x_trainnn == np.array([['-1', '-2'], ['-4', '-5'], ['-7', '-8']])).all())
        self.assertTrue((x_testnn == np.array([[-1, -2], [-4, -5], [-7, -8], [-10, -11]])).all())

        x_train = np.array([['-1', '-2', '-3', '-4', '-5'], ['-4', '-5', '-6', '-7', '-8'], ['-7', '-8', '-9', '-10', '-11']])
        x_test = np.array([[-1, -2, -3, -4, -5], [-4, -5, -6, -7, -8], [-7, -8, -9, -10, -11], [-10, -11, -12, -13, -14]])

        x_train01, x_test01 = (get_two_dim_to_visualize(x_train, x_test, 0, 1))
        self.assertTrue((x_train01 == np.array([['-1', '-2'], ['-4', '-5'], ['-7', '-8']])).all())
        self.assertTrue((x_test01 == np.array([[-1, -2], [-4, -5], [-7, -8], [-10, -11]])).all())

    def test_n_features(self):
        """Check that a ValueError is raised if the number of features of X_train is the same than X_test."""
        x_train = np.array([['-1', '-2', '-3'], ['-4', '-5', '-6'], ['-7', '-8', '-9']])
        x_test = np.array([[-1, -2, -3, -4, -5], [-4, -5, -6, -7, -8], [-7, -8, -9, -10, -11], [-10, -11, -12, -13, -14]])
        self.assertRaises(ValueError, get_two_dim_to_visualize, x_train, x_test, 0, 1)

    def test_features_none(self):
        """Check that a ValueError is raised if the number of features is larger than 4 and feature_x and/or feature_y is not set."""
        x_train = np.array([['-1', '-2', '-3', '-4', '-5'], ['-4', '-5', '-6', '-7', '-8'], ['-7', '-8', '-9', '-10', '-11']])
        x_test = np.array([[-1, -2, -3, -4, -5], [-4, -5, -6, -7, -8], [-7, -8, -9, -10, -11], [-10, -11, -12, -13, -14]])
        self.assertRaises(ValueError, get_two_dim_to_visualize, x_train, x_test)

    def test_feature_xy(self):
        """Check ValueError is raised if feature_x and/or feature_y is too high, negative or equal."""
        x_train = np.array([['-1', '-2', '-3', '-4', '-5'], ['-4', '-5', '-6', '-7', '-8'], ['-7', '-8', '-9', '-10', '-11']])
        x_test = np.array([[-1, -2, -3, -4, -5], [-4, -5, -6, -7, -8], [-7, -8, -9, -10, -11], [-10, -11, -12, -13, -14]])
        self.assertRaises(ValueError, get_two_dim_to_visualize, x_train, x_test, 8, 9)
        self.assertRaises(ValueError, get_two_dim_to_visualize, x_train, x_test, 8, 4)
        self.assertRaises(ValueError, get_two_dim_to_visualize, x_train, x_test, -1, 2)
        self.assertRaises(ValueError, get_two_dim_to_visualize, x_train, x_test, 2, 2)

class TestPrintInfoCoinc(unittest.TestCase):
    """Test print_info_coinc but already tested in TestGetY so we just test the case that was not tested then."""
    def test_warning(self):
        """Check a warning is printed if y contains only 0 and 1 and show_num_coinc is True."""
        opt = {'begin': pd.Timestamp('2019-12-01T00:00:00'),
               'end': pd.Timestamp('2019-12-04T00:00:00'),
               'nside': 1,
               'timestep': pd.Timedelta(1, unit='d'),
              }
        y_array = np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
        timeline = get_timeline(opt)

        captured_output = io.StringIO()
        sys.stdout = captured_output
        print_info_coinc(y_array, (y_array > 1), timeline, opt['nside'], show_num_coinc=True)
        expected_output = "Warning: y contains only 0 and 1, it may not contain the number of coincidences.\n"\
                          "num coinc in pix   begin time            time index   ra       dec      \n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

class TestUseWrittenFile(unittest.TestCase):
    """Test use_written_file."""
    def test_use_written_file(self):
        """Check use_written_file gives output as expected and print as expected if verbose."""
        path = glob.glob(os.path.join(OUTPUT_PATH, '90*/*.pkl'))[0]
        self.assertTrue(not use_written_file(0, path, verbose=True))
        self.assertTrue(not use_written_file(2, path, verbose=True))

        captured_output = io.StringIO()
        sys.stdout = captured_output
        self.assertTrue(use_written_file(1, path, verbose=True))
        expected_output = path.split('/')[-1]+" file already exists.\n"
        self.assertEqual(captured_output.getvalue(), expected_output)
        sys.stdout = sys.__stdout__

class TestJobListIndices(unittest.TestCase):
    """Test job_list_indices."""
    def test_job_list_indices(self):
        """Check output of job_list_indices is as expected."""
        self.assertTrue(job_list_indices(0, N_PRED_JOBS, 100)[0] == 0)
        self.assertTrue(job_list_indices(0, N_PRED_JOBS, 100)[1] == 10)
        self.assertTrue(job_list_indices(3, N_PRED_JOBS, 100)[0] == 30)
        self.assertTrue(job_list_indices(3, N_PRED_JOBS, 100)[1] == 40)

class TestConcatenateDic(unittest.TestCase):
    """Test concatenate_dic."""
    def test_concatenate_dic(self):
        """Check concatenate_dic concatenate correctly."""
        dict1 = {'lala': [1, 1, 1, 1],
                 'lulu': [2, 2, 2, 2],
                 'lolo': [3, 3, 3, 3],
                }
        dict2 = {'lala': [10, 10, 10, 10],
                 'lulu': [20, 20, 20, 20],
                 'lolo': [30, 30, 30, 30],
                }
        dictf = {'lala': [1, 1, 1, 1, 10, 10, 10, 10],
                 'lulu': [2, 2, 2, 2, 20, 20, 20, 20],
                 'lolo': [3, 3, 3, 3, 30, 30, 30, 30],
                }
        self.assertListEqual(list(concatenate_dic([dict1, dict2]).keys()), list(dictf.keys()))
        self.assertListEqual(list(concatenate_dic([dict1, dict2])['lala']), dictf['lala'])
        self.assertListEqual(list(concatenate_dic([dict1, dict2])['lulu']), dictf['lulu'])
        self.assertListEqual(list(concatenate_dic([dict1, dict2])['lolo']), dictf['lolo'])

class TestMaskFermi(unittest.TestCase):
    """Test mask_fermi."""
    def test_mask_fermi(self):
        """Check output of test_mask_fermi is as expected."""
        expected_mask = [True, False, False, True, True, True, True, False, False, False, True, True, False, False, True, False, False, False, True,
                         True, False, False, True, False, False, False, True, False, False, False, True, True, False, False, True, False, False, False,
                         True, True, True, True, True, False, False, True, True, False]
        n_side = 2
        b_dist_deg = 10
        self.assertListEqual(mask_Fermi(2, 10), expected_mask)

class TestGetContamination(unittest.TestCase):
    """Test get_contamination"""
    def test_get_contamination(self):
        """Check contamination is as expected."""
        opt = {'begin': pd.Timestamp('2019-12-01 00:00:00'),
               'end': pd.Timestamp('2020-01-29 00:00:00'),
               'timestep': pd.Timedelta(10, unit='d'),
               'nside': 128,
               'sig_in_train': 0,
              }
        self.assertEqual(get_contamination(opt), 2.204047309027778e-06)
