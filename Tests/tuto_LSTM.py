from random import randint
from numpy import array
from numpy import argmax
from pandas import DataFrame
from pandas import concat
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import TimeDistributed
from keras.layers import RepeatVector

# generate a sequence of random numbers in [0, 99]

def generate_sequence(length=25, shift=False):
    if shift:
        sequence = [randint(50, 60) for _ in range(length)]
    else:
        sequence = [randint(45, 55) for _ in range(length)]
    return sequence

# one hot encode sequence


def one_hot_encode(sequence, n_unique=100):
    encoding = list()
    for value in sequence:
        vector = [0 for _ in range(n_unique)]
        vector[value] = 1
        encoding.append(vector)
    return array(encoding)

# decode a one hot encoded string


def one_hot_decode(encoded_seq):
    return [argmax(vector) for vector in encoded_seq]

# convert encoded sequence to supervised learning


def to_supervised(sequence_X, sequence_y, n_in, n_out):
    # create lag copies of the sequence
    df = DataFrame(sequence_X)
    df = concat([df.shift(n_in - i - 1) for i in range(n_in)], axis=1)
    # drop rows with missing values
    df.dropna(inplace=True)
    # specify columns for input and output pairs
    values = df.values
    width = sequence_X.shape[1]
    X = values.reshape(len(values), n_in, width)

    df = DataFrame(sequence_y)
    df = concat([df.shift(n_in - i - 1) for i in range(n_in)], axis=1)
    # drop rows with missing values
    df.dropna(inplace=True)
    # specify columns for input and output pairs
    values = df.values
    width = sequence_y.shape[1]
    y = values[:, 0:(n_out * width)].reshape(len(values), n_out, width)
    return X, y

# prepare data for the LSTM


def get_data(n_in, n_out, training=True):
    # generate random sequence
    if training:
        sequence = generate_sequence(shift=False)
    else:
        sequence = generate_sequence(shift=False)
    # one hot encode
    encoded_X = one_hot_encode(sequence)
    encoded_y = encoded_X
    # encoded_y = one_hot_encode([randint(0,1) for _ in range(25)])
    # convert to X,y pairs
    X, y = to_supervised(encoded_X, encoded_y, n_in, n_out)
    return X, y


# define LSTM
n_in = 5
n_out = 2
encoded_length = 100
batch_size = 21
model = Sequential()
model.add(LSTM(150, batch_input_shape=(batch_size, n_in, encoded_length), stateful=True))
model.add(RepeatVector(n_out))
model.add(LSTM(150, return_sequences=True, stateful=True))
model.add(TimeDistributed(Dense(encoded_length, activation='softmax')))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# train LSTM
for epoch in range(1000):
    # generate new random sequence
    X, y = get_data(n_in, n_out)
    # fit model for one epoch on this sequence
    model.fit(X, y, epochs=1, batch_size=batch_size, verbose=2, shuffle=False)
    model.reset_states()
# evaluate LSTM
X, y = get_data(n_in, n_out, training=False)
yhat = model.predict(X, batch_size=batch_size, verbose=0)
# decode all pairs
for i in range(len(X)):
    print('Expected:', one_hot_decode(y[i]), 'Predicted', one_hot_decode(yhat[i]))
