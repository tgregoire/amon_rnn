# -*- coding: utf-8 -*-
# pylint: disable=C0103,W0621
"""
.

Usage:
  data_prep

Options:
  -h --help     Show this screen.
"""
import math
import importlib
import numpy as np
from matplotlib import pyplot as plt
from keras.models import Sequential # pylint: disable=E0401
from keras.layers import Dense # pylint: disable=E0401
from keras.layers import LSTM # pylint: disable=E0401
from sklearn.metrics import mean_squared_error # pylint: disable=E0401
# from sklearn.preprocessing import MinMaxScaler # pylint: disable=E0401
from sklearn.preprocessing import RobustScaler # pylint: disable=E0401
from data_prep import * # pylint: disable=W0614,W0401
import data_prep as dp # NB this is used to reload modules more easily

# TODO selon https://www.youtube.com/watch?v=-uyXE7dY5H0 14min: to tune your NN you usually just need to check the learning rate, initialisation, clip norm of gradient, lower learning rate at the end (bon il y a aussi le batch size)
# TODO au moment venu, il faudra discuter avec Simon de la normalization des input Standardization vs Normalization
# TODO il faudra traiter la question des périodes avec buggy detector (et comprendre si ça arrive souvent etc)
# TODO to check NN cannot distinguish scrambled and real distributions: train NN on scrambled data (only one exp) then look if there is anomaly using real data with very large time windows
# TODO the time selection of Hawc events can be problematic for the transition between time steps, as they are not independent
# NB "It is usually better to use a standardized data (transformed to Gaussian, mean 0 and variance 1) for autoencoders." https://towardsdatascience.com/extreme-rare-event-classification-using-autoencoders-in-keras-a565b386f098

# %%
dataset = "HAWC_hotspots"
timestep = pd.Timedelta(1, unit='d')
one_step = False # set to True to only use one timestep
nside = 1 # distance between two pixels is 90/(nside)
pixel = 2
test_signal = 1.8
training = True

begin = DATASETS[dataset]["begin_test"]
end = DATASETS[dataset]["end_test"]
if one_step:
    end = begin + timestep

# %%
dataframe = get_data(dataset)
df_ref = scramble_data(dataset, dataframe)

df = {'ref': df_ref, 'eval': dataframe}
skymaps = {}
for data in ['ref', 'eval']:
    if training and data == 'eval':
        skymaps['eval'] = skymaps['ref']
        break
    df[data] = assign_ra_dec_rad(df[data])
    skymaps[data] = get_skymaps_of_densities(dataset, df[data], begin, end, timestep, nside)
    # TODO use Standardization instead of normalization (and use sklearn)
    # skymaps[data] = normalize(skymaps[data])
samples_input = skymaps_to_samples(skymaps['ref'], skymaps['eval'], pixel)


# %%
# TODO save a few skymaps (png and other format)
# TODO plutôt que de chercher pour chaque pixel l'event density je pourrais, pour chaque evnt faire un healpy disc que j'ajoute à la map, ça pourrait être bien plus rapide
for skymap in skymaps['eval'].columns[2:]:
    draw_skymap(skymaps['eval'][skymap])

# %%
print(np.shape(samples_input))
print(samples_input)
# %%
importlib.reload(dp) # pylint: disable=E1101
train_input, test_input, test_sig_input = split_train_test(samples_input, test_prop=0.5, test_signal=test_signal)

# TODO we will need to save the data preparation objects to apply the same as training on real data: https://machinelearningmastery.com/how-to-save-and-load-models-and-data-preparation-in-scikit-learn-for-later-use/
# normalize the dataset
# TODO finalement on va fit un scaler sur input et un autre sur target (parce que pas même shape mais pareil à l'intérieur)
scaler_input = RobustScaler().fit(train_input) # like MinMaxScaler but use quantiles (25, 75) instead, does not depend on a few events
train_input = scaler_input.transform(train_input)
test_input = scaler_input.transform(test_input)
test_sig_input = scaler_input.transform(test_sig_input)

# %%
train_target = train_input[:, -N_STEPS_EVAL:]
test_target = test_input[:, -N_STEPS_EVAL:]
test_sig_target = test_sig_input[:, -N_STEPS_EVAL:]

scaler_target = RobustScaler().fit(train_target) # like MinMaxScaler but use quantiles (25, 75) instead, does not depend on a few events
train_target = scaler_target.transform(train_target)
test_target = scaler_target.transform(test_target)
test_sig_target = scaler_target.transform(test_sig_target)

# reshape input to be [samples, time steps, features]
train_input = np.reshape(train_input, (train_input.shape[0], train_input.shape[1], 1))
test_input = np.reshape(test_input, (test_input.shape[0], test_input.shape[1], 1))
test_sig_input = np.reshape(test_sig_input, (test_sig_input.shape[0], test_sig_input.shape[1], 1))
train_target = np.reshape(train_target, (train_target.shape[0], train_target.shape[1], 1))
test_target = np.reshape(test_target, (test_target.shape[0], test_target.shape[1], 1))
test_sig_target = np.reshape(test_sig_target, (test_sig_target.shape[0], test_sig_target.shape[1], 1))

# %%
# TODO I am not sure if I want stateful or just timesteps see tuto
batch_size = 1
model = Sequential()
model.add(LSTM(4, batch_input_shape=(batch_size, N_STEPS_TOTAL, 1), stateful=True))
model.add(Dense(1)) # output layer
model.compile(loss='mean_squared_error', optimizer='adam')
for _ in range(2):
    model.fit(train_input, train_target, epochs=1, batch_size=batch_size, verbose=2, shuffle=False)
    model.reset_states()
# make predictions
train_predict = model.predict(train_input, batch_size=batch_size)
model.reset_states()
test_predict = model.predict(test_input, batch_size=batch_size)
model.reset_states()
test_sig_predict = model.predict(test_sig_input, batch_size=batch_size)

# %%
print(model.summary())
# %%
print(train_target)
train_predict = scaler_target.inverse_transform(train_predict)
train_target = scaler_target.inverse_transform(train_target)
test_predict = scaler_target.inverse_transform(test_predict)
test_target = scaler_target.inverse_transform(test_target)
test_sig_predict = scaler_target.inverse_transform(test_sig_predict)
test_sig_target = scaler_target.inverse_transform(test_sig_target)
# %%
# calculate root mean squared error
print(np.shape(train_target[:, 0]))
print(np.shape(train_predict))
train_score = math.sqrt(mean_squared_error(train_target[:, 0], train_predict))
print('Train Score: %.2f RMSE' % (train_score))
test_score = math.sqrt(mean_squared_error(test_target[:, 0], test_predict))
print('Test Score: %.2f RMSE' % (test_score))
test_sig_score = math.sqrt(mean_squared_error(test_sig_target[:, 0], test_sig_predict))
print('Test Sig Score: %.2f RMSE' % (test_sig_score))

# %%
plt.rcParams.update({'font.size': 22})
mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))

train_timesteps = range(len(train_target[:, 0, 0]))
plt.plot(train_timesteps, train_target[:, 0, 0], lw=2, color='orange')
plt.plot(train_timesteps, train_predict[:, 0], lw=2, color='green')
plt.plot(train_timesteps, train_target[:, 0, 0]-train_predict[:, 0], lw=2, color='red')
plt.show()

# %%
plt.rcParams.update({'font.size': 22})
mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))

test_timesteps = range(len(test_target[:, 0, 0]))
plt.plot(test_timesteps, test_sig_target[:, 0], lw=3, color='black')
plt.plot(test_timesteps, test_sig_predict[:, 0], lw=2, color='orange')
plt.plot(test_timesteps, test_target[:, 0, 0], lw=3, color='black')
plt.plot(test_timesteps, test_predict[:, 0], lw=2, color='green')
plt.show()

# %%
plt.rcParams.update({'font.size': 22})
mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))

plt.plot(range(len(test_sig_target[:, 0, 0])), test_sig_target[:, 0, 0]-test_sig_predict[:, 0], lw=2, color='orange')
plt.plot(range(len(test_target[:, 0, 0])), test_target[:, 0, 0]-test_predict[:, 0], lw=2, color='red')
plt.show()
