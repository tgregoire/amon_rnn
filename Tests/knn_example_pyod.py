# -*- coding: utf-8 -*-
# pylint: disable=C0103,E0401
"""Example of using ... for outlier detection
"""
# Author: Yue Zhao <zhaoy@cmu.edu>
# License: BSD 2 clause
import numpy as np
from matplotlib import pyplot as plt
from pyod.models.knn import KNN
from pyod.models.mo_gaal import MO_GAAL
# from pyod.models.so_gaal import SO_GAAL
from pyod.models.iforest import IForest
from pyod.models.auto_encoder import AutoEncoder
from pyod.utils.data import generate_data, generate_data_clusters
from pyod.utils.data import evaluate_print
from pyod.utils.example import visualize
from data_prep import * # pylint: disable=W0614,W0401


# TODO Get real data
# TODO simulate coincidences
# TODO test several algorithms
# TODO get outliers inliers
# NB si pour le training on donne tous les pixels proches (et donc similaires) KNN va considéré qu'il y a pleins de nearest neighbors,
# ...il faudrait un pixel par zone par timestep

# %%
contamination_threshold = 1e-3  # proportion of false positive we accept
test_contamination = 0.04  # proportion of outliers injected in test data
n_features = 1  # number of dimensions # TODO test with more dimensions
n_train = 20000  # number of training points
n_test = 10000  # number of testing points

# Generate sample data
X_train, X_test, y_train, y_test = generate_data_train_inlier(n_train, n_test, n_features, contamination_threshold, test_contamination)

# %%
X_train
# %%
clf_name = 'KNN'
clf = KNN(contamination=contamination_threshold)
clf.fit(X_train)

# clf_name = 'MO-GAAL'
# clf = MO_GAAL(k=4, lr_d=0.1, lr_g=0.01, stop_epochs=100, contamination=contamination_threshold)
# clf.fit(X_train)
#
# clf_name = 'IForest'
# clf = IForest(contamination=contamination_threshold)
# clf.fit(X_train)
#
# clf_name = 'AutoEncoder'
# clf = AutoEncoder(contamination=contamination_threshold)
# clf.fit(X_train)

# %%
# get the prediction labels and outlier scores of the training data
y_train_pred = clf.labels_  # binary labels (0: inliers, 1: outliers)
y_train_scores = clf.decision_scores_  # raw outlier scores

# get the prediction on the test data
y_test_pred = clf.predict(X_test)  # outlier labels (0 or 1)
y_test_scores = clf.decision_function(X_test)  # outlier scores

# %%
# evaluate and print the results
# print("\nOn Training Data:")
# evaluate_print(clf_name, y_train, y_train_scores)
print("\nOn Test Data:")
evaluate_print(clf_name, y_test, y_test_scores)

# %%
# visualize the results
# visualize(clf_name, X_train, y_train, X_test, y_test, y_train_pred,
#           y_test_pred, show_figure=True, save_figure=True)


# %%
plt.rcParams.update({'font.size': 22})
# mpl_tex_rc(sans=True)
plt.figure(figsize=(6 * 1.618, 6))
binning = np.logspace(np.log10(min(y_test_scores)), np.log10(max(y_test_scores)), 100)
# binning = np.linspace(min(y_test_scores), max(y_test_scores), 100)
y_test_scores_stacked = [y_test_scores[(y_test == 1) * (y_test_pred == 1)],
                         y_test_scores[(y_test == 1) * (y_test_pred == 0)],
                         y_test_scores[(y_test == 0) * (y_test_pred == 1)],
                         y_test_scores[(y_test == 0) * (y_test_pred == 0)]]
colors = ['tab:blue', 'tab:cyan', 'tab:orange', 'tab:red']
labels = ['Out; Class out', 'Out; Class in', 'In; Class out', 'In; Class in']
plt.hist(y_test_scores_stacked, stacked=True, histtype='bar', bins=binning, density=True, lw=2, label=labels, color=colors)
plt.legend()
plt.xscale('log')
plt.yscale('log')
plt.show()

# %%
y_test_scores_stacked
