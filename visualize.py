# -*- coding: utf-8 -*-
# pylint: disable=C0103
"""
.

Usage:
  data_prep

Options:
  -h --help     Show this screen.
"""
import os
# import glob
import math
import warnings
# import pickle
from itertools import combinations
# from itertools import compress
import numpy as np
import healpy as hp
from scipy import interpolate
from matplotlib import pyplot as plt
import matplotlib as mpl
from distutils.spawn import find_executable
# from astropy import units as u
# from astropy.coordinates import SkyCoord, EarthLocation, AltAz, ICRS
# from astropy.time import Time
# import pandas as pd
# from pandas.tseries.offsets import MonthEnd
from sklearn.utils import column_or_1d, check_consistent_length # pylint: disable=E0401
from sklearn.metrics import roc_auc_score, roc_curve, auc # pylint: disable=E0401
from pyod.utils.utility import precision_n_scores # pylint: disable=E0401
from pyod.utils import data as pyod # pylint: disable=E0401
# from docopt import docopt # pylint: disable=E0401
from simple_functions import pix2ang_eq, get_timeline, all_combinations, type_of_script, get_split_index, SEED
# docopt(__doc__)
warnings.filterwarnings('ignore') # put default to have them back
# NB I managed to reduce event_density's time from 8 ms to 3 ms!
# I should have used classes, I think, but that is a bit too late

SHORTEN_DATASET_NAMES = {'ANTARES': 'ANT', 'IceCube': 'IC', 'HAWC_hotspots': 'HAWC_h', 'HAWC_bursts': 'HAWC_b'}

def all_combinations_str(dataset_list, n_comb_inj='all'):
    """Get combinations of the element of list_ in the form of strings <dataset1>+<dataset2>+... .
    If n_comb_inj='all' get all combinations for all n in [2, len(list_)] corresponding to the number of elements to combine.
    If n_comb_inj is set, returns only combinations for n=n_comb_inj.

    Args:
        dataset_list: List of datasets to combine. (list of str)
        n_comb_inj: Number of elements to combine. If set to 'all' it will get all the combinations for n in [2, len(dataset_list)]. (int)
    Returns:
        datasets_combs: List of strings of combinations. (list of str)
    """
    combs = all_combinations(range(len(dataset_list)), n_comb_inj, dataset_list)
    datasets_combs = []
    for comb in combs:
        datasets = ''
        for dataset in comb:
            dataset_str = dataset_list[dataset]
            if dataset_str in SHORTEN_DATASET_NAMES.keys():
                dataset_str = SHORTEN_DATASET_NAMES[dataset_str]
            datasets += dataset_str+'+'
        datasets = datasets[:-1]
        datasets_combs += [datasets]
    return datasets_combs

def combs_to_colors(dataset_list, n_comb_inj='all'):
    """Create dictionnary with combination of datasets as keys and colors at values.

    Args:
        dataset_list: List of datasets to combine. (list of str)
        n_comb_inj: Number of elements to combine. If set to 'all' it will get all the combinations for n in [2, len(dataset_list)]. (int)
    Returns:
        combs_to_colors: Dictionnary with a color for each combination. (dict)
    """
    datasets_combs = all_combinations_str(dataset_list, n_comb_inj)
    color_list = list(reversed(list(mpl._color_data.XKCD_COLORS.keys())))
    # Remove colors that are too similar or similar to the background of the plot
    color_list.remove('xkcd:red')
    # color_list.remove('xkcd:orange')
    color_list.remove('xkcd:light green')
    color_list.remove('xkcd:magenta')
    color_list.remove('xkcd:teal')
    color_list.remove('xkcd:sky blue')
    color_list.remove('xkcd:lime green')
    color_list.remove('xkcd:light purple')
    color_list.remove('xkcd:violet')
    return {combination: color for combination, color in zip(datasets_combs, color_list)}


def draw_skymap(opt, skymap, minimum=None, maximum=None, title='', skymap_id='', dic_inj_comb=None):
    """Draw the skymap in Oranges with a marker for the injected signal position if dic_inj_comb is given.
    Save it if opt['write_files'].

    Args:
        opt: Option arguments of the script. (dict)
        skymap: An healpy skymap. (list)
        maximum: Maximum of the color axis. (float)
        title: Title of the plot. (str)
        skymap_id: Time id of the skymap. It is written as subtitle. (str/int)
        dic_inj_comb: Dictionnary containing the pixel index and combined datasets of the injected signal. (dict)
    """
    plt.rcParams.update({'font.size': 18})
    title_subtitle = title+'\nTime id: '+str(skymap_id)
    # bw_cmap = plt.cm.get_cmap('Oranges', 8) # pylint: disable=E1101
    bw_cmap = plt.cm.get_cmap('seismic', 8) # pylint: disable=E1101
    hp.mollview(skymap, title=latex_escape(title_subtitle), min=minimum, max=maximum, cbar=True, notext=True, hold=False, cmap=bw_cmap)
    hp.graticule()
    plt.text(2.0, 0., r"$0^\circ$", ha="left", va="center")
    plt.text(1.9, 0.45, r"$30^\circ$", ha="left", va="center")
    plt.text(1.4, 0.8, r"$60^\circ$", ha="left", va="center")
    plt.text(1.9, -0.45, r"$-30^\circ$", ha="left", va="center")
    plt.text(1.4, -0.8, r"$-60^\circ$", ha="left", va="center")
    plt.text(2.0, -0.15, r"$180^\circ$", ha="center", va="center")
    plt.text(1.333, -0.15, r"$240^\circ$", ha="center", va="center")
    plt.text(.666, -0.15, r"$300^\circ$", ha="center", va="center")
    plt.text(0.0, -0.15, r"$0^\circ$", ha="center", va="center")
    plt.text(-.666, -0.15, r"$60^\circ$", ha="center", va="center")
    plt.text(-1.333, -0.15, r"$120^\circ$", ha="center", va="center")
    plt.text(-2.0, -0.15, r"$180^\circ$", ha="center", va="center")

    if dic_inj_comb and dic_inj_comb['comb_pixels']:
        datasets2color = combs_to_colors(opt['dataset_list'], opt['n_comb_inj'])
        color_events = [datasets2color[datasets] for datasets in dic_inj_comb['comb_datasets']]

        dec, ra = np.rad2deg(hp.pix2ang(opt['nside'], list(dic_inj_comb['comb_pixels']))) # pylint: disable=E1111
        dec = 90. - dec
        hp.projscatter(ra, dec, lonlat=True, coord='G', marker=u'$\u25EF$', s=200, color=color_events) # Don't manage to change marker size or use normal 'o' marker not filled

        markers = [mpl.lines.Line2D([], [], color=color, marker=u'$\u25EF$', linestyle='None', label=latex_escape(datasets)) for datasets, color in datasets2color.items()]
        plt.legend(loc='lower right', bbox_to_anchor=(0.15, 0.75), fontsize=10, handles=markers)

    plt.draw()
    if opt['write_files'] > 0:
        title = title.replace(' ', '_')
        filename = 'skymap'+title+str(skymap_id)+'.png'
        if title not in opt['dataset_list'] and 'Pixels' not in title:
            filename = opt['clf_name']+'_'+filename
        filepath = os.path.join(opt['out_path'], filename)
        plt.savefig(filepath, bbox_inches='tight', facecolor='white')
    if type_of_script() != 'jupyter':
        plt.close()

def get_dic_inj_comb(dic_Xy, is_test, tid_start, tid_end):
    """Get dictionnary of injected combinaision of signal of a given skymap used for plotting.

    Args:
        dic_Xy: Dictionnary X and y. (dict)
        is_test: True if we want the injected signal in the test sample
        tid_start: id of the first pixel of the map we want.
        tid_end: id of the last pixel of the map we want.
    Return:
        dic_inj_comb: Dictionnary of injected combination of signal used for plotting skymaps.
    """
    Xy_key = 'y_test' if is_test else 'y_train'
    outliers_tid = dic_Xy[Xy_key][tid_start:tid_end]
    dic_inj_comb = {'comb_pixels': [i for i, x in enumerate(outliers_tid) if x]}

    y_before = dic_Xy[Xy_key][:tid_start]
    if is_test:
        n_inj_before = len(dic_Xy['y_train'][dic_Xy['y_train'] == 1]) + len(y_before[y_before == 1])
    else:
        n_inj_before = len(y_before[y_before == 1])
    dic_inj_comb['comb_datasets'] = dic_Xy['comb_datasets'][n_inj_before:int(n_inj_before+sum(outliers_tid))]
    return dic_inj_comb

def outlier_skymaps(opt, dic_Xy, dic_pred, key='y_test_score', title='', time_ids=None, threshold=None):
    """Plot skymaps of the y_data which can be outlier scores, predictions or truth.

    Args:
        opt: Option arguments of the script. (dict)
        dic_Xy: Dictionnary X and y. (dict)
        dic_pred: Dictionnary of predictions and scores. (dict)
        key: Key of dic_pred to plot. (str)
        title: Title of the figure. (str)
        time_ids: Time ids of the skymaps we want to show. If None plot all skymaps. (list)
        threshold: Threshold of the outlier score. Color scale is centered around it if not None. (float)
    """
    is_test = 'test' in key
    if not is_test and not opt['sig_in_train']:
        print("Warning: No signal in train.")

    if is_test:
        y_data = dic_pred[key]
    else: # if train, build map with zeros for unused pixels and score for the others
        y_data = np.zeros(get_split_index(opt))
        rng = np.random.default_rng(SEED)
        n_train_points = len(dic_pred[key])
        indices_y = rng.choice(range(get_split_index(opt)), n_train_points, replace=False)
        y_data[indices_y] = dic_pred[key]

    npix = hp.nside2npix(opt['nside'])
    max_score = y_data.max() # Used as max of color scale
    if max_score < 1 and max_score > 0.9:
        max_score = 1
    if not threshold:
        min_score = y_data.min() # Used as min of color scale
    else:
        min_score = max_score - 2.*(max_score - threshold) # Used as min of color scale so that threshold is at the center of the scale

    first_time_id = int(get_split_index(opt)/npix) if is_test else 0
    if time_ids is None:
        time_ids = range(len(get_timeline(opt)))

    for tid in time_ids:
        if tid < first_time_id:
            continue
        tid_start = (tid-first_time_id)*npix
        tid_end = (tid-first_time_id+1)*npix
        if tid_end > len(y_data):
            break
        data_tid = y_data[tid_start:tid_end]

        dic_inj_comb = get_dic_inj_comb(dic_Xy, is_test, tid_start, tid_end)
        draw_skymap(opt, data_tid, min_score, max_score, title, tid, dic_inj_comb)

def mpl_tex_rc(sans=False):
    """Get beautifull plots."""
    use_latex = True
    if not find_executable('dvipng'):
        use_latex = False
    if sans:
        plt.rc('text', usetex=use_latex)
        plt.rc('font', family='sans-serif')
        plt.rc('font', **{'sans-serif': 'Computer Modern Sans Serif'})
        mpl.rcParams['text.latex.preamble'] = [
            r'\usepackage{amsmath}',
            r'\usepackage{sansmath}',
            r'\SetSymbolFont{operators}   {sans}{OT1}{cmss} {m}{n}'
            r'\SetSymbolFont{letters}     {sans}{OML}{cmbrm}{m}{it}'
            r'\SetSymbolFont{symbols}     {sans}{OMS}{cmbrs}{m}{n}'
            r'\SetSymbolFont{largesymbols}{sans}{OMX}{iwona}{m}{n}'
            r'\sansmath'
        ]
    else:
        plt.rc('text', usetex=use_latex)
        plt.rc('font', family='serif')
        plt.rc('font', serif='Computer Modern Roman')
        mpl.rcParams['text.latex.preamble'] = [
            r'\usepackage{amsmath}',
        ]

def latex_escape(string):
    """Escape special Latex characters

    Args:
        string: A string.
    Returns:
        string: Same string with latex special characters preceded by \
    """
    return (string
            .replace('&', '\\&')
            .replace('$', '\\$')
            .replace('%', '\\%')
            .replace('#', '\\#')
            .replace('_', '\\_')
            .replace('{', '\\{')
            .replace('}', '\\}')
           )

def get_two_dim_to_visualize(X_train, X_test, feature_x=None, feature_y=None):
    """Add or remove dimensions to X_train and X_test to get 2 features only for visualization.

    Args:
        X_train: The training samples. (numpy array of shape (n_samples, n_features))
        X_test: The testing samples. (numpy array of shape (n_samples, n_features))
        feature_x: When there is more than 2 features, this is the index of feature to plot in x axis. (int smaller than n_features)
        feature_y: When there is more than 2 features, this is the index of feature to plot in y axis. (int smaller than n_features)
    Returns:
        X_train_2D: Two dimensions training samples to plot. (numpy array of shape (n_samples, 2))
        X_test_2D: Two dimensions testing samples to plot. (numpy array of shape (n_samples, 2))
    """
    if X_train.shape[1] != X_test.shape[1]:
        raise ValueError("X_train and X_test must have the same number of features.")
    if X_train.shape[1] > 4 and (feature_x is None or feature_y is None):
        raise ValueError("n_features > 4, feature_x and feature_y should not be None.")
    if (feature_x is not None and X_train.shape[1] <= feature_x) or (feature_y is not None and X_train.shape[1] <= feature_y):
        raise ValueError("Feature index is larger than number of features. {feature_x} or {feature_y} >= {n_features}"
                         .format(feature_x=feature_x, feature_y=feature_y, n_features=X_train.shape[1]))
    if (feature_x is not None and feature_x < 0) or (feature_y is not None and feature_y < 0):
        raise ValueError("feature_x and/or feature_y is larger than number of features.")
    if feature_x is not None and feature_y is not None and feature_x == feature_y:
        raise ValueError("feature_x should not be equal to feature_y.")

    if X_train.shape[1] == 3 and feature_x is None and feature_y is None:
        X_train_2D = np.array([X_train.T[0], np.zeros(len(X_train))]).T
        X_test_2D = np.array([X_test.T[0], np.zeros(len(X_test))]).T
    elif X_train.shape[1] == 4 and feature_x is None and feature_y is None:
        X_train_2D = np.array(X_train[:, [0, 1]])
        X_test_2D = np.array(X_test[:, [0, 1]])
    elif feature_x is not None and feature_y is not None:
        X_train_2D = np.array(X_train[:, [feature_x, feature_y]])
        X_test_2D = np.array(X_test[:, [feature_x, feature_y]])
    return X_train_2D, X_test_2D

def visualize_2D_results(opt, X_train, y_train, y_train_pred, X_test, y_test,
                         y_test_pred, mean_Z, labels=None, show_figure=True, save_figure=False):
    """Utility function for visualizing the results of 2D samples.

    Args:
        opt: Option arguments of the script. (dict)
        X_train: The training samples. (numpy array of shape (n_samples, 2))
        y_train: The ground truth of training samples. (list or array of shape (n_samples,))
        X_test: The test samples. (numpy array of shape (n_samples, 2))
        y_test: The ground truth of test samples. (list or array of shape (n_samples,))
        y_train_pred: The predicted binary labels of the training samples. (numpy array of shape (n_samples, n_features))
        y_test_pred: The predicted binary labels of the test samples. (numpy array of shape (n_samples, n_features))
        show_figure: If set to True, show the figure. (bool)
        save_figure: If set to True, save the figure to the local. (bool)
    """

    def _add_sub_plot(X_inliers, X_outliers, sub_plot_title,
                      inlier_color='blue', outlier_color='orange'):
        """Method to add subplot of inliers and outliers.

        Args:
            X_inliers: Outliers. (numpy array of shape (n_samples, n_features))
            X_outliers: Inliers. (numpy array of shape (n_samples, n_features))
            sub_plot_title: Subplot title. (str)
            inlier_color: The color of inliers. (str)
            outlier_color: The color of outliers. (str)
        """
        # plt.axis("equal")
        bins = np.linspace(-0.02, 1.02, 6)
        xx, yy = np.meshgrid(bins, bins)
        plt.contourf(xx, yy, mean_Z, levels=np.linspace(mean_Z.min(), mean_Z.max(), 10), cmap=plt.get_cmap('Greys_r'))
        plt.scatter(X_inliers[:, 0], X_inliers[:, 1], label='inliers',
                    color=inlier_color, s=None, alpha=0.02)
        plt.scatter(X_outliers[:, 0], X_outliers[:, 1],
                    label='outliers', color=outlier_color, s=4, marker='^')
        plt.title(latex_escape(sub_plot_title), fontsize=15)
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        if labels is not None:
            plt.xlabel(latex_escape(labels[0]), fontsize=10)
            plt.ylabel(latex_escape(labels[1]), fontsize=10)
        plt.legend(prop={'size': 10})

    # check input data shapes are consistent
    X_train, y_train, X_test, y_test, y_train_pred, y_test_pred = \
        pyod.check_consistent_shape(X_train, y_train, X_test, y_test, y_train_pred,
                                    y_test_pred)

    if X_train.shape[1] != 2:
        raise ValueError("Input data has to be 2-d for visualization. The "
                         "input data has {shape}.".format(shape=X_train.shape))

    X_train_outliers, X_train_inliers = pyod.get_outliers_inliers(X_train, y_train)
    X_train_outliers_pred, X_train_inliers_pred = pyod.get_outliers_inliers(
        X_train, y_train_pred)

    X_test_outliers, X_test_inliers = pyod.get_outliers_inliers(X_test, y_test)
    X_test_outliers_pred, X_test_inliers_pred = pyod.get_outliers_inliers(
        X_test, y_test_pred)

    # plot ground truth vs. predicted results
    fig = plt.figure(figsize=(12, 10))
    plt.suptitle(latex_escape("Demo of {clf_name} Detector".format(clf_name=opt['clf_name'])),
                 fontsize=15)

    fig.add_subplot(221)
    _add_sub_plot(X_train_inliers, X_train_outliers, 'Train Set Ground Truth',
                  inlier_color='blue', outlier_color='orange')

    fig.add_subplot(222)
    _add_sub_plot(X_train_inliers_pred, X_train_outliers_pred,
                  'Train Set Prediction', inlier_color='blue',
                  outlier_color='orange')

    fig.add_subplot(223)
    _add_sub_plot(X_test_inliers, X_test_outliers, 'Test Set Ground Truth',
                  inlier_color='green', outlier_color='red')

    fig.add_subplot(224)
    _add_sub_plot(X_test_inliers_pred, X_test_outliers_pred,
                  'Test Set Prediction', inlier_color='green',
                  outlier_color='red')

    if save_figure:
        plt.savefig(os.path.join(opt['out_path'], opt['clf_name']+'_'+labels[0]+'_'+labels[1]+'.png'), bbox_inches='tight', facecolor='white')#, dpi=300)

    if show_figure:
        plt.show()
    if type_of_script() != 'jupyter':
        plt.close()

def visualize_all_results(opt, clf, X_train, y_train, y_train_pred,
                          X_test, y_test, y_test_pred):
    """Utility function for visualizing the results.
    It will display several 2D plots of the different features.

    Args:
        opt: Option arguments of the script. (dict)
        X_train: The training samples. (numpy array of shape (n_samples, n_features))
        y_train: The ground truth of training samples. (list or array of shape (n_samples,))
        X_test: The test samples. (numpy array of shape (n_samples, n_features))
        y_test: The ground truth of test samples. (list or array of shape (n_samples,))
        y_train_pred: The predicted binary labels of the training samples. (numpy array of shape (n_samples, n_features))
        y_test_pred: The predicted binary labels of the test samples. (numpy array of shape (n_samples, n_features))
    """
    bins = np.linspace(-0.02, 1.02, 6) # time to run is prop to n_bins^(n_datasets+2)
    axes = [bins]*len(X_train[0, :])
    grid_axes = np.meshgrid(indexing='ij', *axes)
    Z = clf.decision_function(np.array([axis.ravel() for axis in grid_axes]).T) * -1
    if 'GAAL' in opt['clf_name']: # NB Correct for a bug in pyod for MO-GAAL, SO-GAAL where 0 is outlier and 1 is inlier.
        Z = 1-Z
    Z = Z.reshape(grid_axes[0].shape)
    show_figure = (opt['write_files'] == 0)
    save_figure = (not opt['write_files'] == 0)

    if X_train.shape[1]-2 >= 2:
        for i, j in combinations(range(X_train.shape[1]-2), 2):
            labels = [opt['dataset_list'][j], opt['dataset_list'][i]]
            X_train_2D, X_test_2D = get_two_dim_to_visualize(X_train, X_test, j, i)

            axes_to_mean = list(range(Z.ndim))
            axes_to_mean.remove(i)
            axes_to_mean.remove(j)
            mean_Z = np.mean(Z, axis=tuple(axes_to_mean))

            visualize_2D_results(opt, X_train_2D, y_train, y_train_pred, X_test_2D, y_test, y_test_pred,
                                 mean_Z, labels, show_figure=show_figure, save_figure=save_figure)
    elif X_train.shape[1]-2 == 1:
        labels = [opt['dataset_list'][0], "None"]
        X_train_2D, X_test_2D = get_two_dim_to_visualize(X_train, X_test)
        mean_Z = np.mean(Z, axis=tuple([1, 2]))
        visualize_2D_results(opt, X_train_2D, y_train, y_train_pred,
                             X_test_2D, y_test, y_test_pred, mean_Z, labels,
                             show_figure=show_figure, save_figure=save_figure)

    labels = ["sin(Altitude)", "Azimuth"]
    X_train_2D, X_test_2D = get_two_dim_to_visualize(X_train, X_test, X_train.shape[1]-1, X_train.shape[1]-2)
    axes_to_mean = list(range(Z.ndim))[:-2]
    mean_Z = np.mean(Z, axis=tuple(axes_to_mean))
    visualize_2D_results(opt, X_train_2D, y_train, y_train_pred,
                         X_test_2D, y_test, y_test_pred, mean_Z, labels,
                         show_figure=show_figure, save_figure=save_figure)
    return

def print_info_coinc(y, condition, timeline, nside, show_num_coinc=False):
    """Print a table with each row corresponding to a data point with more than one coincidence,
    the columns are the number of coincidences, the time and position (ra, dec) in degrees.

    Args:
        y: Number of coincidence for each data point. (1D np.array)
        condition: Condition of what elements to show, typically condition=(y > 0). (np.array of bool with same length than y)
        timeline: List of pd.Timestamp.
        nside: nside of the healpy skymap. (int)
        show_num_coinc: True will show the number of sources in each selected datapoint. However, it works only if y contains the number of sources and not just 0 and 1.
    """
    if sum((y > 1)) == 0 and show_num_coinc:
        print("Warning: y contains only 0 and 1, it may not contain the number of coincidences.")
    data_time_pix = np.array([[time, pix] for _, time in enumerate(timeline) for pix in range(hp.nside2npix(nside))]) # TODO make a function from this and a test
    row_format = '{:<22}{:<13}{:<9}{:<9}'
    column_names = ['begin time', 'time index', 'ra', 'dec']
    if show_num_coinc:
        row_format = '{:<19}' + row_format
        column_names = ['num coinc in pix'] + column_names
    print(row_format.format(*column_names))
    for i in range(sum(condition)):
        time = data_time_pix[condition][i, 0]
        time_index = timeline.index(time)
        pix = data_time_pix[condition][i, 1]
        dec, ra = np.degrees(pix2ang_eq(nside, pix)) # pylint: disable=E1111
        info = [time.strftime('%m-%d-%Y %H:%M:%S'), time_index, '{:.5}'.format(ra), '{:.5}'.format(dec)]
        if show_num_coinc:
            num_coinc = y[condition][i]
            info = [num_coinc] + info
        print(row_format.format(*info))

def evaluate_print(opt, dic_Xy, dic_pred, sample='test', n_pred_pos=None):
    """Utility function for evaluating and printing the results for examples.
    Default metrics include ROC (Receiver Operating Characteristic) and Precision @ n

    Args:
        opt: Option arguments of the script. (dict)
        dic_Xy: Dictionnary X and y. (dict)
        dic_pred: Dictionnary of predictions and scores. (dict)
        sample: 'train' or 'test' depending on the sample we want. (str)
        n_pred_pos: Number of positive predicted. (int)
    """
    y = column_or_1d(dic_Xy['y_'+sample])
    y_pred_score = column_or_1d(dic_pred['y_'+sample+'_scores'])
    check_consistent_length(y, y_pred_score)

    if np.count_nonzero(y) > 0:
        print('{clf_name} ROC:{roc}, true pos/all pos:{prn}'.format(
            clf_name=opt['clf_name'],
            roc=np.round(roc_auc_score(y, y_pred_score), decimals=4),
            prn=np.round(precision_n_scores(y, y_pred_score, n_pred_pos), decimals=4)))
    else:
        print('{clf_name} No true outliers, true pos/all pos:{prn}'.format(
            clf_name=opt['clf_name'],
            prn=np.round(precision_n_scores(y, y_pred_score, n_pred_pos), decimals=4)))


    # fpr: prop of badly classified neg: false pos/neg
    # tpr: prop of well classified pos: true pos/pos
    fpr, tpr, _ = roc_curve(y, y_pred_score)
    roc_auc = auc(fpr, tpr)

    plt.rcParams.update({'font.size': 18})
    plt.figure(figsize=(6 * 1.618, 6))
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic curve')
    plt.legend(loc="lower right")
    if opt['write_files'] > 0:
        plt.savefig(os.path.join(opt['out_path'], opt['clf_name']+'_ROC_'+sample+'.png'), bbox_inches='tight', facecolor='white')#, dpi=300)
    else:
        plt.show()
        # print(threshold)
    if type_of_script() != 'jupyter':
        plt.close()

def plot_dur_dec_HAWC_hotspots_spline(spline, dec=None, dur=None):
    """Plot spline of the duration versus declination of the HAWC_hotspots. Eventually plot the data too.

    Args:
        spline: Spline representation obtained from interpolate.splrep.
        dec: Declination data (x axis) to plot.
        dur: Duration data (y axis) to plot.
    """
    plt.figure()
    dec_new = np.linspace(-26, 64, 1000)
    dur_new = interpolate.splev(dec_new, spline, der=0)
    if dec and dur:
        plt.plot(dec, dur, 'o', dec_new, dur_new, markersize=1)
        plt.legend(['Data Points', 'Cubic Spline'])
    else:
        plt.plot(dec_new, dur_new, markersize=1)
        plt.legend(['Cubic Spline'])
    plt.xlabel('Declination')
    plt.ylabel('Duration')
    plt.show()

def distrib_outlier_score(opt, y_, y_scores, y_pred, filename, data_type='test'):
    """
    Plot the outlier score distribution.
    Args:
        opt: Option arguments of the script. (dict)
        y_: y_test or y_train from dic_Xy. (array)
        y_scores: y_test_scores or y_train_scores from dic_pred. (array)
        y_pred:  y_test_pred or y_train_pred from dic_pred. (array)
        filename: Name of the png file. (str)
        data_type: 'test', 'test_nosig' or 'train' depending on the distribution we plot.
    """
    plt.rcParams.update({'font.size': 18})
    mpl_tex_rc(sans=True)
    plt.figure(figsize=(6 * 1.618, 6))

    if data_type == 'train':
        y_scores[y_scores == 0] = min(y_scores)/10. # NB 0 values are set to 1e-7 for plotting on log scale
    else:
        y_scores[y_scores == 0] = 1e-7#min(y_scores)/10. # NB 0 values are set to 1e-7 for plotting on log scale

    if data_type == 'nosig' or data_type == 'test_nosig':
        y_scores_stacked = [np.array([]),
                            y_scores[y_pred == 1],
                            np.array([]),
                            y_scores[y_pred == 0]]
    else:
        y_scores_stacked = [y_scores[(y_ == 1) * (y_pred == 1)],
                            y_scores[(y_ == 0) * (y_pred == 1)],
                            y_scores[(y_ == 1) * (y_pred == 0)],
                            y_scores[(y_ == 0) * (y_pred == 0)]]

    colors = ['tab:blue', 'tab:orange', 'tab:cyan', 'tab:red']
    labels = ['True pos; Class pos', 'True neg; Class pos', 'True pos; Class neg', 'True neg; Class neg']
    plt.hist(y_scores_stacked, stacked=True, histtype='bar', lw=2, label=labels, color=colors, bins=200)# bins=binning)#
    # plt.hist(y_scores_stacked, stacked=False, histtype='stepfilled', lw=2, label=labels, color=colors, alpha=0.5, bins=100)
    plt.legend()
    plt.title(latex_escape(opt['clf_name']))
    plt.xlabel('Outlier Score')
    plt.yscale('log')
    ymin, ymax = plt.gca().get_ylim()
    plt.ylim(ymin/2.)
    never_save = opt['write_files'] == 0
    if not never_save:
        plt.savefig(os.path.join(opt['out_path'], filename), bbox_inches='tight', facecolor='white')
        print('Outlier score distribution saved.')
    if type_of_script() != 'jupyter':
        plt.close()
